package com.redorange.stitch.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.room.Room;

import com.redorange.stitch.R;
import com.redorange.stitch.activities.AllMessagesActivity;
import com.redorange.stitch.activities.LoginActivity;
import com.redorange.stitch.activities.RequestsRecievedActivity;
import com.redorange.stitch.activities.TraineeCoursesActivity;
import com.redorange.stitch.activities.TrainerCourses;
import com.redorange.stitch.activities.TrainerOverviewActivity;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.modal.CurrentUserModal;

import java.io.File;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class SidebarUtils  {

    public static void showSideBar(Activity context, List<CurrentUserModal> tempStored, String token) {
        AlertDialog.Builder dialogBuilder;
        AlertDialog alertDialog;
        dialogBuilder = new AlertDialog.Builder(context);
        View layoutView = context.getLayoutInflater().inflate(R.layout.activity_trainers_profile, null);
        CircleImageView circleImageView = layoutView.findViewById(R.id.imageViewUserImage);
        LinearLayout buttonLogout = layoutView.findViewById(R.id.linearViewLogout);
        LinearLayout requestToJoin = layoutView.findViewById(R.id.send_message);
        TextView textViewUserName = layoutView.findViewById(R.id.textViewUserName);

        ImageView imageViewCross = layoutView.findViewById(R.id.imageViewCross);
        LinearLayout btnProfile = layoutView.findViewById(R.id.myProfile);
        LinearLayout myCourses = layoutView.findViewById(R.id.myCourses);
        LinearLayout myMessages = layoutView.findViewById(R.id.myMessages);
        LinearLayout changeLanguage = layoutView.findViewById(R.id.changeLanguage);
        dialogBuilder.setView(layoutView);


        changeLanguage.setOnClickListener(v -> {
            if (context.getResources().getConfiguration().locale.getLanguage().matches("bn")) {
                Locale myLocale = new Locale("en");
                Resources res = context.getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);

            } else {
                Locale myLocale = new Locale("bn");
                Resources res = context.getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
            }

            if (tempStored.get(0).getRole().matches("Trainee")) {
                Intent intent = new Intent(context, TraineeCoursesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);

            } else {
                Intent intent = new Intent(context, TrainerCourses.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);

            }
//                isEnglish = !isEnglish;
        });

        if (tempStored.get(0).getPic_local() != null) {
            File imgFile = new File(tempStored.get(0).getPic_local());

            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                circleImageView.setImageBitmap(myBitmap);
            }
        }
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
        myCourses.setOnClickListener(view -> {
            Intent mainCourses = new Intent(context, TraineeCoursesActivity.class);
            context.startActivity(mainCourses);
            context.finish();
            alertDialog.dismiss();
        });

        imageViewCross.setOnClickListener(v -> alertDialog.dismiss());
        textViewUserName.setText("" + tempStored.get(0).getName());
        TextView textViewRole = layoutView.findViewById(R.id.textViewRole);
        textViewRole.setText("" + tempStored.get(0).getRole());

        btnProfile.setOnClickListener(v -> {
            myCourses.setBackgroundColor(Color.parseColor("#e20161"));
            Intent intent = new Intent(context, TrainerOverviewActivity.class);
            context.startActivity(intent);
            alertDialog.dismiss();
        });
        requestToJoin.setOnClickListener(v -> {
            Intent intent2 = new Intent(context, RequestsRecievedActivity.class);
            intent2.putExtra("token", "" + token);
            context.startActivity(intent2);
        });


        buttonLogout.setOnClickListener(view -> {
            AppDatabase database = Room.databaseBuilder(context, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();

            database.getCurrentUserDAO().deleteAllCurrent();
            database.getCoursesDAO().deleteAllCurrent();
            database.getOrgsDAO().deleteAllOrgs();
            database.getContentCourseDAO().deleteAllCourseContent();
            database.getProgressWDao().deleteAllProgressManagerWithExamModel();
            database.getProgresNDao().deleteAllProgress();
            database.getSubmissionsDao().deleteAllSubmissions();

            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        });


        myMessages.setOnClickListener(v -> {
            Intent intent = new Intent(context, AllMessagesActivity.class);
            context.startActivity(intent);
            alertDialog.dismiss();
        });
    }
}
