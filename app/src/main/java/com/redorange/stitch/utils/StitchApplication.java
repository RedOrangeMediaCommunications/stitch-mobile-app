package com.redorange.stitch.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import io.realm.Realm;

public class StitchApplication extends Application {
    private static final String WIFI_STATE_CHANGE_ACTION = "android.net.wifi.WIFI_STATE_CHANGED";

    private static Context context;

    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        StitchApplication.context = getApplicationContext();
//        registerForNetworkChangeEvents(this);
    }

    public static Context getAppContext() {
        return StitchApplication.context;
    }

    public static String getResourceString(int resourceId){
        Context appContext = getAppContext();
        if(appContext == null) return null;
        Resources resources = appContext.getResources();
        return resources.getString(resourceId);
    }

//    public static void registerForNetworkChangeEvents(final Context context) {
//        NetworkStateChangeReceiver networkStateChangeReceiver = new NetworkStateChangeReceiver();
//        context.registerReceiver(networkStateChangeReceiver, new IntentFilter(CONNECTIVITY_ACTION));
//        context.registerReceiver(networkStateChangeReceiver, new IntentFilter(WIFI_STATE_CHANGE_ACTION));
//    }
}
