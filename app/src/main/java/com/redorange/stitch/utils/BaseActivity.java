package com.redorange.stitch.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.redorange.stitch.activities.TraineeCoursesActivity;

import java.io.File;

public class BaseActivity extends AppCompatActivity {
    //base of all activity class
    public static DialogUtil dialogUtil;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize dialog utils class for getting dialog util object
        dialogUtil = new DialogUtil(this);
    }

    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(String msg){
        Toast.makeText(StitchApplication.getAppContext(), msg, Toast.LENGTH_SHORT);
    }

    public static void showWait(String msg, Context context){
        dialogUtil = new DialogUtil(context);
        dialogUtil.showProgressDialog(msg);
    }

    public static void hideWait(){
        if(dialogUtil == null) return;
        dialogUtil.dismissProgress();
    }

    public static void showError(Object err){
        hideWait();
        if(err instanceof Exception){
            Exception exception = (Exception) err;
            showToast(StitchApplication.getAppContext(), exception.getMessage());
        }
        else if(err instanceof String){
            showToast(StitchApplication.getAppContext(), err.toString());
        }
        else{
            showToast(StitchApplication.getAppContext(),"An error occurred");
        }
    }

    //method for network available or not checking
    public static boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager)StitchApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    //method for fragment calling

    protected void initFragment(Fragment fragment, String id, int resId) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(resId, fragment, id)
                .addToBackStack(null)
                .commit();
    }

    //method for fragment replace
    protected void replaceFragment(Fragment fragment, String id, int resId) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(resId, fragment, id)
                .addToBackStack(id)
                .commit();
    }

    //method for going to homepage
    public void gotoHomePage() {

        Intent intent;
        intent = new Intent(this, TraineeCoursesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);

    }

    public void adjustFontScale(Configuration configuration) {

        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);

    }

    public boolean internetIsConnected() {
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }

    public String bytesIntoHumanReadable(long bytes) {
        long kilobyte = 1024;
        long megabyte = kilobyte * 1024;
        long gigabyte = megabyte * 1024;
        long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";

        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KB";

        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MB";

        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GB";

        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TB";

        } else {
            return bytes + " Bytes";
        }
    }

    protected String DownloadImage(String url) {
        String path = "";
        if (url != null) {
            String filename = url.substring(url.lastIndexOf("/") + 1);
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);
            path = file.getPath();
            if (file.exists()) {

            } else {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
                        .setTitle(filename)
                        .setDescription("Downloading")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                        .setDestinationUri(Uri.fromFile(file))
                        .setAllowedOverMetered(true)
                        .setAllowedOverRoaming(true);
                DownloadManager downloadManager = (DownloadManager) getApplicationContext().getSystemService(DOWNLOAD_SERVICE);
                long referenceID = downloadManager.enqueue(request);
            }
        }
        return path;
    }

}

