package com.redorange.stitch.utils;


import android.content.Context;
import android.net.Uri;

import androidx.fragment.app.Fragment;

import java.util.List;

public class Singleton {
    private static Singleton ourInstance = new Singleton();
    private Context context;
    private Fragment whichFragmentItIs;

    private String avater, senderName, senderImg;
    private int examId;

    private Singleton() {
    }

    public static Singleton getInstance() {
        return ourInstance;
    }

    public Context getContext() {
        if (context == null) {
            throw new RuntimeException("Context is null in Singleton singleton. Check if setContext() is called properly.");
        }
        return context;
    }

    public void setContext(Context context) {
        this.context = context.getApplicationContext();
    }



    public void setWhichFragmentItIs(Fragment whichFragmentItIs) {
        this.whichFragmentItIs = whichFragmentItIs;
    }

    public Fragment getWhichFragmentItIs() {
        return whichFragmentItIs;
    }

    public static Singleton getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(Singleton ourInstance) {
        Singleton.ourInstance = ourInstance;
    }

    public String getAvater() {
        return avater;
    }

    public void setAvater(String avater) {
        this.avater = avater;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public String getSenderImg() {
        return senderImg;
    }

    public void setSenderImg(String senderImg) {
        this.senderImg = senderImg;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}

