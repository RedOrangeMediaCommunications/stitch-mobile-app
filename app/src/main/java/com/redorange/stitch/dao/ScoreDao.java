package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.Query;

import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.ScoreModal;

import java.util.List;

@Dao
public interface ScoreDao {

    @Insert
    public void insert(ScoreModal... scoreModals);

    @Query("SELECT * FROM tbl_score WHERE id= :id And chapterId= :chapterId")
    public List<ScoreModal> getScore(String id, String chapterId);
}
