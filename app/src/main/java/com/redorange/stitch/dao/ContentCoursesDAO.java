package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.typeconverter.contentChapterConverter;

import java.util.List;

@Dao
public interface ContentCoursesDAO {
    @Insert
    public void insert(CourseContentModel... currentUserModals);

    @Update
    public void update(CourseContentModel... booksLocals);

    @Delete
    public void delete(CourseContentModel booksLocal);

    @Query("SELECT * FROM courses_content")
    public List<CourseContentModel> getCourseContentAll();

    @Query("SELECT * FROM courses_content WHERE id = :id")
    public CourseContentModel getGetCourseContentbyID(String id);

    @TypeConverters(contentChapterConverter.class)
    @Query("UPDATE courses_content SET Chapters =:Chapters  WHERE id = :id")
    void updateChapters(String id, List<ContentChapters> Chapters);


    @Query("DELETE FROM courses_content")
    void deleteAllCourseContent();
}