package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.modal.ProgressManagerWithExamModel;

import java.util.List;

@Dao
public interface ProgressWithExamDAO {
    @Insert
    public void insert(ProgressManagerWithExamModel... currentUserModals);

    @Update
    public void update(ProgressManagerWithExamModel... booksLocals);

    @Delete
    public void delete(ProgressManagerWithExamModel booksLocal);

    @Query("SELECT * FROM progress_manager_with")
    public List<ProgressManagerWithExamModel> getOrgsAll();

    @Query("SELECT * FROM progress_manager_with WHERE chapterId = :chapterId")
    public ProgressManagerWithExamModel getProgressbyChapterID(String chapterId);

    @Query("DELETE FROM progress_manager_with")
    void deleteAllProgressManagerWithExamModel();
}