package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;

import java.util.List;

@Dao
public interface CoursesDAO {
    @Insert
    public void insert(CoursesModal... currentUserModals);

    @Update
    public void update(CoursesModal... booksLocals);

    @Delete
    public void delete(CoursesModal booksLocal);

    @Query("SELECT * FROM courses_all")
    public List<CoursesModal> getCoursesAll();

    @Query("SELECT * FROM courses_all WHERE id = :id")
    public CoursesModal getGetCoursesbyID(String id);

    @Query("DELETE FROM courses_all")
    void deleteAllCurrent();
}