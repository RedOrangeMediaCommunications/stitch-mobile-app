package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.OrgsModal;

import java.util.List;

@Dao
public interface OrgsDAO {
    @Insert
    public void insert(OrgsModal... currentUserModals);

    @Update
    public void update(OrgsModal... booksLocals);

    @Delete
    public void delete(OrgsModal booksLocal);

    @Query("SELECT * FROM orgs")
    public List<OrgsModal> getOrgsAll();

    @Query("SELECT * FROM orgs WHERE id = :id")
    public OrgsModal getGetOrgsyID(String id);

    @Query("DELETE FROM orgs")
    void deleteAllOrgs();
}