package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.entities.SubmissionsM;
import com.redorange.stitch.modal.CourseContentModel;

import java.util.List;

@Dao
public interface SubmissionsDAO {
    @Insert
    public void insert(SubmissionsM... currentUserModals);

    @Update
    public void update(SubmissionsM... booksLocals);

    @Delete
    public void delete(SubmissionsM booksLocal);

    @Query("SELECT * FROM submissions")
    public List<SubmissionsM> getCourseContentAll();

    @Query("SELECT * FROM submissions WHERE examId = :id")
    public SubmissionsM getGetSubmissionsbyExamID(String id);


    

    @Query("DELETE FROM submissions")
    void deleteAllSubmissions();
}