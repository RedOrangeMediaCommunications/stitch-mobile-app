package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.modal.EvaluationModal;

import java.util.List;

@Dao
public interface EvaluationDao {
    @Insert
    public void insert(EvaluationModal... evaluationModal);

    @Update
    public void update(EvaluationModal... evaluationModal);

    @Delete
    public void delete(EvaluationModal... evaluationModal);

    @Query("SELECT * FROM tbl_evaluation WHERE id = :id")
    List<EvaluationModal> getEvaluationModal(String id);

    @Query("DELETE FROM tbl_evaluation")
    void deleteAllCurrent();

}
