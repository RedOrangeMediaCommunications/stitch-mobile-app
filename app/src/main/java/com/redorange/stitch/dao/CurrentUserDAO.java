package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.redorange.stitch.modal.CurrentUserModal;

import java.util.List;

@Dao
public interface CurrentUserDAO {
    @Insert
    public void insert(CurrentUserModal... currentUserModals);

    @Query("UPDATE current_user SET name =:name, bio =:bio, address = :address  WHERE id = :id")
    void updateUser(String id, String name, String bio, String address);
//    public void update(CurrentUserModal... currentUserModals);

    @Delete
    public void delete(CurrentUserModal booksLocal);

    @Query("SELECT * FROM current_user")
    public List<CurrentUserModal> getCurrentUser();

    @Query("SELECT * FROM current_user WHERE name = :name")
    public CurrentUserModal getBooksByName(String name);

    @Query("UPDATE current_user SET pic_local =:pic  WHERE id = :id")
    void updateUserAvater(String id, String pic);

    @Query("DELETE FROM current_user")
    void deleteAllCurrent();
}