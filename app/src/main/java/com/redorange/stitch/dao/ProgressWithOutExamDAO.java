package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.modal.ProgressManagerNoExamModel;

import java.util.List;

@Dao
public interface ProgressWithOutExamDAO {
    @Insert
    public void insert(ProgressManagerNoExamModel... currentUserModals);

    @Update
    public void update(ProgressManagerNoExamModel... booksLocals);

    @Delete
    public void delete(ProgressManagerNoExamModel booksLocal);

    @Query("SELECT * FROM progress_manager_no_exam")
    public List<ProgressManagerNoExamModel> getProgressNoExam();

    @Query("SELECT * FROM progress_manager_no_exam WHERE (chapterId = :chapterId AND fileId=:fileId)")
    public ProgressManagerNoExamModel getByChapterFileID (String chapterId,String fileId);


    @Query("SELECT * FROM progress_manager_no_exam WHERE chapterId = :chapterId ")
    public  List<ProgressManagerNoExamModel> getByChapter(String chapterId);

    @Query("DELETE FROM progress_manager_no_exam")
    void deleteAllProgress();
}