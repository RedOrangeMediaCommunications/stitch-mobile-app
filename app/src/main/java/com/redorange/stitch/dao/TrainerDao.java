package com.redorange.stitch.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redorange.stitch.entities.Trainers;

import java.util.List;

@Dao
public interface TrainerDao {
    @Insert
    public void insert(Trainers... trainerModels);

    @Update
    public void update(Trainers... trainerModels);

    @Delete
    public void delete(Trainers trainerModel);

    @Query("SELECT * FROM tbl_trainers")
    public List<Trainers> getTrainers();

    @Query("SELECT * FROM tbl_trainers WHERE id = :courseId")
    public Trainers getTrainers(String courseId);

    @Query("DELETE FROM tbl_trainers")
    void deleteAllCurrent();
}
