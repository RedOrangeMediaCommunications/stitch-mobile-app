package com.redorange.stitch.apis;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.ContentCoursesDAO;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.EvaluationDao;
import com.redorange.stitch.dao.OrgsDAO;
import com.redorange.stitch.dao.ProgressWithExamDAO;
import com.redorange.stitch.dao.ProgressWithOutExamDAO;
import com.redorange.stitch.dao.ScoreDao;
import com.redorange.stitch.dao.SubmissionsDAO;
import com.redorange.stitch.entities.SubmissionsM;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.EvaluationModal;
import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.modal.ProgressManagerNoExamModel;
import com.redorange.stitch.modal.ProgressManagerWithExamModel;
import com.redorange.stitch.modal.ScoreModal;


@Database(entities = {SubmissionsM.class,ProgressManagerWithExamModel.class,
        ProgressManagerNoExamModel.class,CurrentUserModal.class, CoursesModal.class, CourseContentModel.class,
        OrgsModal.class, ScoreModal.class, EvaluationModal.class}, version = DatabaseConstants.DATABASE_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CurrentUserDAO getCurrentUserDAO();
    public abstract CoursesDAO getCoursesDAO();
    public abstract OrgsDAO getOrgsDAO();
    public abstract ContentCoursesDAO getContentCourseDAO();
    public abstract ProgressWithExamDAO getProgressWDao();
    public abstract ProgressWithOutExamDAO getProgresNDao();
    public abstract SubmissionsDAO getSubmissionsDao();
    public abstract ScoreDao getScoreDao();
    public abstract EvaluationDao getEvaluationDao();
}