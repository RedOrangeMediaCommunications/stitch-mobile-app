package com.redorange.stitch.apis;

import com.redorange.stitch.constant.APIConstants;
import com.redorange.stitch.entities.CourseContentM;
import com.redorange.stitch.entities.CourseJoinRequestM;
import com.redorange.stitch.entities.CoursesByIdModel;
import com.redorange.stitch.entities.EvalutionModel;
import com.redorange.stitch.entities.Questions;
import com.redorange.stitch.entities.course.CoursesM;
import com.redorange.stitch.entities.LoginInfoM;
import com.redorange.stitch.entities.MessageGet;
import com.redorange.stitch.entities.OrgsM;
import com.redorange.stitch.entities.RegisterInfoM;
import com.redorange.stitch.entities.RequestSentByTraineeM;
import com.redorange.stitch.entities.SubmissionsReply;
import com.redorange.stitch.entities.TrainerStats;
import com.redorange.stitch.entities.thread.Info;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface StitchApi {

    @FormUrlEncoded
    @POST(APIConstants.AUTH)
    Call<LoginInfoM> getLogin(
            @Field("phone") String phone,
            @Field("password") String password);

    @FormUrlEncoded

    @POST(APIConstants.REGISTER)
    Call<RegisterInfoM> getRegister(
            @Field("name") String name,
            @Field("password") String password,
            @Field("bio") String bio,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("address") String address,
            @Field("blocked") String blocked,
            @Field("role") String role);

    @FormUrlEncoded
    @POST(APIConstants.PROCESS)
    Call<LoginInfoM> getOtp(@Field("otp") String otp);

    @GET(APIConstants.COURSES)
    Call<List<CoursesM>> getCourses(@Header("Authorization") String Authorization);

    @GET(APIConstants.ORGS)
    Call<List<OrgsM>> getOrgs(@Header("Authorization") String Authorization);

    @FormUrlEncoded
    @POST(APIConstants.REQUESTS)
    Call<CourseJoinRequestM> postRequestJoin(
            @Header("Authorization") String Authorization,
            @Field("type") String type,
            @Field("courseId") String courseId,
            @Field("content") String content);

    @GET(APIConstants.REQUEST_USER)
    Call<RequestSentByTraineeM> getRequestJoin(@Header("Authorization") String Authorization);


//    @GET("courses/{id}/mobile")
//    Call<CourseContentM> getCourseContent(@Header("Authorization") String Authorization, @Path("id") String Course_id);


//    @GET("users/{id}")
//    Call<LoginInfoM> getCurrentUser(@Header("Authorization") String Authorization, @Path("id") String User_id);


    @GET(APIConstants.COURSES_USER)
    Call<List<CourseContentM>> getCurrentContent(@Header("Authorization") String Authorization);

    @DELETE(APIConstants.REQUESTS_BY_ID)
    Call<ResponseBody> getDELETErequest(
            @Header("Authorization") String Authorization,
            @Path("id") String request_id);

    @FormUrlEncoded
    @POST(APIConstants.COMMENTS)
    Call<ResponseBody> postComments(
            @Header("Authorization") String Authorization,
            @Field("parentId") String parentId,
            @Field("content") String content,
            @Field("entityId") String entityId,
            @Field("entityType") String entityType);

    @FormUrlEncoded
    @POST(APIConstants.REACTIONS)
    Call<ResponseBody> postReaction(
            @Header("Authorization") String Authorization,
            @Field("type") String type,
            @Field("entityId") String entityId,
            @Field("entityType") String entityType);

    @FormUrlEncoded
    @PUT(APIConstants.USERS_BY_ID)
    Call<ResponseBody> getUpdateUserText(
            @Header("Authorization") String Authorizationon,
            @Path("id") String User_id,
            @Field("name") String name,
            @Field("bio") String bio,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("address") String address);

    @Multipart
    @PUT(APIConstants.USERS_BY_ID)
    Call<ResponseBody> getUpdateUserPic(
            @Header("Authorization") String Authorizationon,
            @Path("id") String User_id,
            @Part("pic") RequestBody file);

    @Multipart
    @PUT(APIConstants.USERS_BY_ID)
    Call<ResponseBody> getUpdateUserCV(
            @Header("Authorization") String Authorizationon,
            @Path("id") String User_id,
            @Part("cv") MultipartBody.Part file);

    @FormUrlEncoded
    @POST(APIConstants.AUTH_FORGOT)
    Call<ResponseBody> getForgotPassword(@Field("phone") String phone);

    @FormUrlEncoded
    @POST(APIConstants.REQUESTS_BY_PROCESS)
    Call<ResponseBody> getResetPassword(
            @Field("otp") String otp,
            @Field("password") String password);

    @FormUrlEncoded
    @POST(APIConstants.SUBMISSIONS)
    Call<SubmissionsReply> postSubmissions(
            @Header("Authorization") String Authorization,
            @Field("examId") int examId,
            @Field("answers") String answers);

    @FormUrlEncoded
    @POST(APIConstants.COURSES_PATH_BY_RATE)
    Call<ResponseBody> postCourseRating(
            @Header("Authorization") String Authorization,
            @Path("Path") String User_id,
            @Field("rating") float rating);

    @FormUrlEncoded
    @POST(APIConstants.RATINGS)
    Call<ResponseBody> postTrainerRating(
            @Header("Authorization") String Authorization,
            @Field("rating") float rating,
            @Field("entityId") String entityId,
            @Field("entityType") String entityType);

    @FormUrlEncoded
    @POST(APIConstants.THREADS_USER)
    Call<List<MessageGet>> getMessages(
            @Header("Authorization") String Authorization,
            @Field("userId") String userId);

    @FormUrlEncoded
    @POST(APIConstants.MESSAGES)
    Call<ResponseBody> postMessageWithout(
            @Header("Authorization") String Authorization,
            @Field("recipientId") String recipientId,
            @Field("content") String content);

    @FormUrlEncoded
    @POST(APIConstants.MESSAGES)
    Call<ResponseBody> postMessageWithThread(
            @Header("Authorization") String Authorization,
            @Field("recipientId") String recipientId,
            @Field("content") String content,
            @Field("threadId") String threadId);

//    @FormUrlEncoded
    @POST(APIConstants.MESSAGES)
    Call<ResponseBody> postThreads(
            @Header("Authorization") String Authorization,
            @Body Info info);

    @GET(APIConstants.SINGLE_THREAD)
    Call<MessageGet> getSingleThread(
            @Header("Authorization") String Authorization,
            @Path("id") int id
    );


    @FormUrlEncoded
    @POST(APIConstants.REQUESTS)
    Call<ResponseBody> postRequestCertificate(
            @Header("Authorization") String Authorization,
            @Field("type") String type,
            @Field("courseId") String courseId,
            @Field("orgId") String orgId,
            @Field("content") String content);


    @FormUrlEncoded
    @POST(APIConstants.USERS_TRAINER_STATS)
    Call<TrainerStats> getTrainerStats(
            @Header("Authorization") String Authorization,
            @Path("Path") String User_id);

    @GET(APIConstants.COURSES_BY_ID)
    Call<CoursesByIdModel> getExamId(
            @Header("Authorization") String Authorization,
            @Path("id") String id);

    @GET(APIConstants.EXAMS_READ_ONE)
    Call<EvalutionModel> getEvaluationQuestion(
            @Header("Authorization") String Authorization,
            @Path("id") int id);

}
