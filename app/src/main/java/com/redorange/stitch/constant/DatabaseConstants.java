package com.redorange.stitch.constant;

public class DatabaseConstants {
    // Database Version
    public static final int DATABASE_VERSION = 1;
    // Database Name
    public static final String DATABASE_NAME = "mydb";
}
