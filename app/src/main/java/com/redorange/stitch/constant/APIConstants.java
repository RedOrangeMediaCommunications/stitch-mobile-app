package com.redorange.stitch.constant;

public class APIConstants {
    public static final String AUTH = "auth/login";
    public static final String REGISTER = "auth/register";
    public static final String PROCESS = "requests/process";
    public static final String COURSES = "courses";
    public static final String ORGS = "orgs";
    public static final String REQUESTS= "requests";
    public static final String REQUEST_USER = "requests/user";
    public static final String COURSES_USER = "courses/user";
    public static final String COURSES_BY_ID = "courses/{id}";
    public static final String REQUESTS_BY_ID = "requests/{id}";
    public static final String COMMENTS = "comments";
    public static final String REACTIONS = "reactions";
    public static final String USERS_BY_ID = "users/{id}";
    public static final String AUTH_FORGOT = "auth/forgot";
    public static final String REQUESTS_BY_PROCESS = "requests/process";
    public static final String SUBMISSIONS = "submissions";
    public static final String COURSES_PATH_BY_RATE = "/courses/{Path}/rate";
    public static final String RATINGS = "/ratings";
    public static final String THREADS_USER = "/threads/user";
    public static final String MESSAGES = "messages";
    public static final String POST_THREAD= "threads";
    public static final String SINGLE_THREAD = "threads/{id}";
    public static final String USERS_TRAINER_STATS= "users/{Path}/trainerStats";
    public static final String EXAMS_READ_ONE = "exams/{id}";

}
