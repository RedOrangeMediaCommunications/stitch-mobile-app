package com.redorange.stitch.constant;

public class ApplicationConstants {
    public static final int APP_LOAD_TIME = 3000;
    public static final int PERMISSION_REQUEST_CODE = 1;
    public static final int REQUEST_CODE_VIDEO_FULLSCREEN = 5000;
    public static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
    public static final int PICK_PHOTO_FOR_AVATAR = 0;
}
