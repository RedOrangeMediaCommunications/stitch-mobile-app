package com.redorange.stitch.activities;

import android.content.Intent;
import android.os.Bundle;

import android.widget.EditText;
import android.widget.Toast;

import androidx.room.Room;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;

import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class ResetPasswordActivity extends BaseActivity {
    StitchApi api;
//    String token;
    public AppDatabase database;
    List<CurrentUserModal> tempStored;
    CurrentUserDAO currentUSerDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_reset_password);

        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
        EditText editTextPhoneNumber=findViewById(R.id.editTextPhoneNumber);
        EditText editTextOTPText=findViewById(R.id.editTextOTPText);
        EditText editTextConfirmPass=findViewById(R.id.editTextConfirmPass);
        EditText editTextConfirmPass2=findViewById(R.id.editTextConfirmPass2);

        database = Room.databaseBuilder(ResetPasswordActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();
//        token = "bearer " + tempStored.get(0).getToken();

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.imageViewSidebar).setOnClickListener(v -> showSideBar(ResetPasswordActivity.this, tempStored, "token"));

        findViewById(R.id.textViewSendOtp).setOnClickListener(v -> {
            if(editTextPhoneNumber.getText().length()>0){
                Call<ResponseBody> call1=api.getForgotPassword(""+editTextPhoneNumber.getText().toString());
                call1.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getApplicationContext(),"OTP SENT",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),""+response.message().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),""+t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

        findViewById(R.id.textViewConfirm).setOnClickListener(v -> {
            if(editTextOTPText.getText().length()>0){
                if(editTextConfirmPass.getText().toString().matches(editTextConfirmPass2.getText().toString())){
                    Call<ResponseBody> call1=api.getResetPassword(editTextOTPText.getText().toString(),editTextConfirmPass.getText().toString());
                    call1.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(ResetPasswordActivity.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();

                                Intent intent=new Intent(ResetPasswordActivity.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }else{
                                Toast.makeText(ResetPasswordActivity.this, ""+response.message().toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(ResetPasswordActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }else{
                    Toast.makeText(ResetPasswordActivity.this, "Password Did Not Match", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.textViewLogin).setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        findViewById(R.id.textViewRegister).setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
    }
}