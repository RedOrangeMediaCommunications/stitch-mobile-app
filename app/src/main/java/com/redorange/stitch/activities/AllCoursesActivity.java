package com.redorange.stitch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterTrainer;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.BaseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class AllCoursesActivity extends BaseActivity implements CustomAdapterTrainer.ItemClickListener {
    public String TAG = AllCoursesActivity.class.getSimpleName();
    List<CurrentUserModal> tempStored;
    List<Trainers> trainersList = new ArrayList<>();
    CustomAdapterTrainer adapterTrainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_all_courses);

        Intent intent = getIntent();
        String courseId = intent.getExtras().getString("courseId");
        String token = intent.getExtras().getString("token");


        AppDatabase database = Room.databaseBuilder(AllCoursesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        CurrentUserDAO currentUSerDao = database.getCurrentUserDAO();
        CoursesDAO coursesDAO = database.getCoursesDAO();

        CoursesModal courseStored = coursesDAO.getGetCoursesbyID(courseId);
        Log.d(TAG, "onCreate: " + courseStored.getDescription());
        tempStored = currentUSerDao.getCurrentUser();

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());

        LinearLayout joinButton = findViewById(R.id.joinButton);
        joinButton.setOnClickListener(v -> {
            Intent intent1 = new Intent(AllCoursesActivity.this, JoinCourseRequestActivity.class);
            intent1.putExtra("courseId", "" + courseId);
            intent1.putExtra("token", "" + token);
            startActivity(intent1);
        });
        TextView title = findViewById(R.id.textViewCourseTitle);
        title.setText("" + courseStored.getTitle());

        TextView description = findViewById(R.id.textViewDiscription);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(courseStored.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            description.setText(Html.fromHtml(courseStored.getDescription()));
        }

        TextView textViewObjective = findViewById(R.id.textViewObjective);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textViewObjective.setText(Html.fromHtml(courseStored.getObjective(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            textViewObjective.setText(Html.fromHtml(courseStored.getObjective()));
        }

        RecyclerView recyclerViewTrainer = findViewById(R.id.recyclerVIewTrainer);
        recyclerViewTrainer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTrainer.setNestedScrollingEnabled(false);

        for (Trainers b : courseStored.getTrainers()) {
            trainersList.add(b);
        }

        adapterTrainer = new CustomAdapterTrainer(this, trainersList,token);
        adapterTrainer.setClickListener(this);
        recyclerViewTrainer.setAdapter(adapterTrainer);

        ImageView imageViewBanner = findViewById(R.id.imageViewBanner);
        if (courseStored.getBannerLocal() != null) {
            File imgFile = new File(courseStored.getBannerLocal());

            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageViewBanner.setImageBitmap(myBitmap);
            }
        }

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(AllCoursesActivity.this, tempStored, token));
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}