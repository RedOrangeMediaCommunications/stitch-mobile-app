package com.redorange.stitch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.jsibbold.zoomage.ZoomageView;
import com.redorange.stitch.R;
import com.redorange.stitch.utils.BaseActivity;

import java.io.File;

public class ImageFullscreenActivity extends BaseActivity {
    ZoomageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_image_fullscreen2);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Intent intent = getIntent();
        String path = intent.getExtras().getString("IMAGEURL");

        imageView = findViewById(R.id.imageView31);
        String filename = URLUtil.guessFileName(path, null, null);
        String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/.stitch";

        File file = new File(downloadPath, filename);
        if (file.exists()) {
            imageView.setImageURI(Uri.fromFile(file));
        }
    }
}