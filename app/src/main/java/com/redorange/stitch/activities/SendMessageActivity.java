package com.redorange.stitch.activities;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.room.Room;

import com.google.gson.Gson;
import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.thread.Info;
import com.redorange.stitch.entities.thread.ThreadContent;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = SendMessageActivity.class.getSimpleName();
    public StitchApi api;
    public AppDatabase database;
    CurrentUserDAO currentUSerDao;
    List<CurrentUserModal> tempStored;

    String courseID, trainerId, token,threadID="";

    EditText editTextMessage;
    TextView send_message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_send_message);

        Intent intent = getIntent();
        courseID = intent.getExtras().getString("COURSEID");
        trainerId = intent.getExtras().getString("TRAINERID");

        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);

        database = Room.databaseBuilder(SendMessageActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();
        token = "bearer " + currentUSerDao.getCurrentUser().get(0).getToken();
        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.imageViewSidebar).setOnClickListener(v -> showSideBar(SendMessageActivity.this, tempStored, currentUSerDao.getCurrentUser().get(0).getToken()));

        initViews();
        initListeners();
    }



    private void initViews() {
        editTextMessage = findViewById (R.id.editTextMessage);
        send_message = findViewById (R.id.send_message);
    }

    private void initListeners() {
        send_message.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.send_message:
                sendMessage(token, trainerId, editTextMessage.getText().toString(), null );
                break;
        }
    }

    private void sendMessage(String token, String trainerId, String msg, String threadID) {
        if (!isNetworkAvailable()){
            Toast.makeText(SendMessageActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        } else {
            if (msg.length() ==0){
            }
            else {
                ThreadContent threadContent = new ThreadContent(msg);

                Info info = new Info(trainerId, threadID, threadContent);

                Call<ResponseBody> callPostMessage = api.postThreads("" + token, info);
                callPostMessage.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            editTextMessage.setText("");
                            Toast.makeText(SendMessageActivity.this, "Message sent please check message tabs from message window", Toast.LENGTH_SHORT).show();
                        }else {
                            Log.e(TAG, response.message());
                            Toast.makeText(SendMessageActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }


    }
}
