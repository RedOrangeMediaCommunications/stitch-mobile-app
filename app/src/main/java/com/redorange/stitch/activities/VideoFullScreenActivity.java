package com.redorange.stitch.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.redorange.stitch.R;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.videoplayer.UniversalMediaController;
import com.redorange.stitch.videoplayer.UniversalVideoView;

public class VideoFullScreenActivity extends BaseActivity implements UniversalVideoView.VideoViewCallback {

    int currentTime;
    String Url = "";

    View mBottomLayout;
    View mVideoLayout;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    private static final String TAG = VideoFullScreenActivity.class.getSimpleName();
    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
    private static String VIDEO_URL;
    private int mSeekPosition;
    private int cachedHeight;
    private boolean isFullscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();

        getSupportActionBar().hide();
        setContentView(R.layout.activity_video_full_screen);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (null != extras) {
            mSeekPosition = extras.getInt("currenttime", 0);
            Log.d(TAG, "onCreate: " + mSeekPosition);
            VIDEO_URL = extras.getString("Url");

            mVideoLayout = findViewById(R.id.video_layout);
            mBottomLayout = findViewById(R.id.bottom_layout);
            mVideoView = findViewById(R.id.videoView);
            mMediaController = findViewById(R.id.media_controller);
            mVideoView.setMediaController(mMediaController);
            setVideoAreaSize();
            mVideoView.setVideoViewCallback(this);


            if (mSeekPosition > 0) {
                Log.d(TAG, "onCreate: " + mSeekPosition);

                mVideoView.seekTo(mSeekPosition);
            }

            mMediaController.setTitle("Big Buck Bunny");

            mVideoView.setVideoPath(VIDEO_URL);
        }

        mVideoView.setOnCompletionListener(mp -> Log.d(TAG, "onCompletion "));
        mVideoView.setOnPreparedListener(arg0 -> {
            mVideoView.seekTo(currentTime);
            mVideoView.start();
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
        if (mVideoView != null && mVideoView.isPlaying()) {
            mSeekPosition = mVideoView.getCurrentPosition();
            Log.d(TAG, "onPause mSeekPosition=" + mSeekPosition);
            mVideoView.pause();
        }
    }


    private void setVideoAreaSize() {
        mVideoLayout.post((Runnable) () -> {
            int width = mVideoLayout.getWidth();
            cachedHeight = (int) (mVideoLayout.getHeight());
            ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
            videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            videoLayoutParams.height = cachedHeight;
            mVideoLayout.setLayoutParams(videoLayoutParams);

            mVideoView.requestFocus();
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        Log.d(TAG, "onRestoreInstanceState Position=" + mSeekPosition);
    }


    @Override
    public void onScaleChange(boolean isFullscreen) {
//        this.isFullscreen = isFullscreen;
//        if (isFullscreen) {
//            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//            mVideoLayout.setLayoutParams(layoutParams);
//            mBottomLayout.setVisibility(View.GONE);
//
//        } else {
//            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
//            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//            layoutParams.height = this.cachedHeight;
//            mVideoLayout.setLayoutParams(layoutParams);
//            mBottomLayout.setVisibility(View.VISIBLE);
//        }

        onBackPressed();
    }


    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPause UniversalVideoView callback");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
//        Log.d(TAG, "onStart UniversalVideoView callback");

    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingStart UniversalVideoView callback");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingEnd UniversalVideoView callback");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra("currenttime", mVideoView.getCurrentPosition());
        setResult(RESULT_OK, data);
        super.finish();
    }
}