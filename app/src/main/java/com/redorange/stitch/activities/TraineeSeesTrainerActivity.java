package com.redorange.stitch.activities;

import androidx.annotation.ColorInt;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class TraineeSeesTrainerActivity extends BaseActivity {
    String courseId;
    Trainers user;
    String token;
    List<CurrentUserModal> tempStored;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_trainee_sees_trainer);

        Intent intent = getIntent();
        courseId = intent.getExtras().getString("COURSEID");
        String trainerid = intent.getExtras().getString("TRAINERID");


        AppDatabase database = Room.databaseBuilder(TraineeSeesTrainerActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        CurrentUserDAO currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();
        token = "bearer " +currentUSerDao.getCurrentUser().get(0).getToken();


        CoursesDAO coursesDAO = database.getCoursesDAO();

        CoursesModal courseStored = coursesDAO.getGetCoursesbyID(courseId);

        for (Trainers b : courseStored.getTrainers()) {
            if (b.getId().matches(trainerid)) {
                user = b;
            }

        }



        RatingBar ratingBar=findViewById(R.id.simpleRatingBar);
        ratingBar.setRating(0);
        if(null!=courseStored.getRating()){
            ratingBar.setRating(Float.parseFloat(user.getRating()));
        }
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> {
            StitchApi api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
            Call<ResponseBody> call=api.postTrainerRating(""+token,rating,""+user.getId(),"User");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        setCurrentRating(rating);

                    }else {
                        Toast.makeText(TraineeSeesTrainerActivity.this, ""+response.message().toString(), Toast.LENGTH_SHORT).show();
                    }
                }
                private void setCurrentRating(float rating) {
                    LayerDrawable drawable = (LayerDrawable) ratingBar1.getProgressDrawable();
                        setRatingStarColor(drawable.getDrawable(2), ContextCompat.getColor(TraineeSeesTrainerActivity.this, R.color.starcolor));
                        setRatingStarColor(drawable.getDrawable(1), ContextCompat.getColor(TraineeSeesTrainerActivity.this, R.color.starcolor));
                        setRatingStarColor(drawable.getDrawable(0), ContextCompat.getColor(TraineeSeesTrainerActivity.this, R.color.textColor));


                }
                private void setRatingStarColor(Drawable drawable, @ColorInt int color)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    {
                        DrawableCompat.setTint(drawable, color);
                    }
                    else
                    {
                        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(TraineeSeesTrainerActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        CircleImageView imageViewTrainer = findViewById(R.id.imageViewTrainer);

        TextView textViewTrainerName = findViewById(R.id.textViewTrainerName);
        textViewTrainerName.setText("" + user.getName());

        TextView textViewPhone = findViewById(R.id.textViewPhone);
        if (user.getPhone() == null){
            textViewPhone.setText("No Phone Number Found");
        }else {
            textViewPhone.setText("" + user.getPhone());
        }

        TextView textViewEmail = findViewById(R.id.textViewEmail);
        if (user.getEmail() == null){
            textViewEmail.setText("No Email Account Found");
        }else {
            textViewEmail.setText("" + user.getEmail());
        }

        TextView textViewSendMEssage = findViewById(R.id.textViewSendMEssage);
        textViewSendMEssage.setOnClickListener(v -> {
            Intent intentMessage = new Intent(TraineeSeesTrainerActivity.this, MessageActivity.class);
            intentMessage.putExtra("COURSEID", "" + courseId);

            intentMessage.putExtra("TRAINERID", "" + trainerid);

            startActivity(intentMessage);
        });

        TextView textViewAddress = findViewById(R.id.textViewAddress);
        if ( user.getAddress() == null ){
            textViewAddress.setText("No Address Found" );
        }else {
            textViewAddress.setText("" + user.getAddress());
        }

        TextView textViewBio = findViewById(R.id.textViewBio);
        if (user.getBio() == null){
            textViewBio.setText("No Bio Found");
        } else {
            textViewBio.setText("" + user.getBio());
        }

        if(user.getPic() == null){
            imageViewTrainer.setImageDrawable(getResources().getDrawable(R.drawable.noimage2));
        } else {
            Glide.with(getApplicationContext())
                    .load(user.getPic())
                    .into(imageViewTrainer);
        }


        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.imageViewSidebar).setOnClickListener(v->{
            showSideBar(TraineeSeesTrainerActivity.this, tempStored, token);
        });
    }
}