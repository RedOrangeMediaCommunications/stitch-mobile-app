package com.redorange.stitch.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterCertificates;
import com.redorange.stitch.adapters.CustomAdapterChapters;
import com.redorange.stitch.adapters.CustomAdapterCourses;
import com.redorange.stitch.adapters.CustomAdapterMyCourses;
import com.redorange.stitch.adapters.CustomAdapterOrganizations;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.EvaluationDao;
import com.redorange.stitch.dao.OrgsDAO;
import com.redorange.stitch.entities.Comments;
import com.redorange.stitch.entities.Content;
import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.entities.CourseContentM;
import com.redorange.stitch.entities.CoursesByIdModel;
import com.redorange.stitch.entities.course.CoursesM;
import com.redorange.stitch.entities.OrgsM;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.Singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class TraineeCoursesActivity extends BaseActivity implements
        CustomAdapterCertificates.ItemClickListener, CustomAdapterChapters.ItemClickListener,
        CustomAdapterMyCourses.ItemClickListener, CustomAdapterCourses.ItemClickListener,
        CustomAdapterOrganizations.ItemClickListener {

    public String TAG = TraineeCoursesActivity.class.getSimpleName();
    CustomAdapterMyCourses customAdapterMyCourses;
    CustomAdapterCourses customAdapterCourses;
    CustomAdapterOrganizations customAdapterOrganizations;
    CustomAdapterChapters customAdapterChapters;

    List<CoursesModal> courseList = new ArrayList<>();
    List<CourseContentModel> courseListMy = new ArrayList<>();
    List<OrgsModal> orgList = new ArrayList<>();
    List<CurrentUserModal> tempStored;
    List<CoursesModal> courseListStored = new ArrayList<>();
    List<OrgsModal> orgListStored = new ArrayList<>();

    String token;
    RecyclerView trainerCoursesRecyclerView, recyclerViewAllCourses, recyclerViewOrg, recyclerViewChapter;

    SwipeRefreshLayout refreshLayout;
    CurrentUserDAO currentUSerDao;
    EvaluationDao evaluationDao;
    Boolean isEnglish = true;
    StitchApi api;

    CoursesDAO coursesDAO;
    OrgsDAO orgsDAO;

    private ShimmerFrameLayout mShimmerViewContainer;

    AppDatabase database;
    SearchView searchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_trainee_courses);

        Singleton.getInstance().setContext(getApplicationContext());

        mShimmerViewContainer = findViewById (R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmer();

        database = Room.databaseBuilder(TraineeCoursesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        coursesDAO = database.getCoursesDAO();
        orgsDAO = database.getOrgsDAO();

        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();
        evaluationDao = database.getEvaluationDao();
        token = "bearer " + tempStored.get(0).getToken();

        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);


        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(TraineeCoursesActivity.this, tempStored, token));

        searchEditText = findViewById(R.id.editTextSearch);

        searchEditText.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                customAdapterCourses.getFilter().filter("" + searchEditText.getQuery().toString());
                customAdapterMyCourses.getFilter().filter("" + searchEditText.getQuery().toString());
                customAdapterOrganizations.getFilter().filter("" + searchEditText.getQuery().toString());
                return true;
            }
        });


        refreshLayout = findViewById(R.id.swipe_container);
        refreshLayout.setOnRefreshListener(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast_layout, findViewById(R.id.toast_layout_root));

            TextView text = layout.findViewById(R.id.text);
            text.setText("Updating Content");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 15);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();

            setupMyCourses(api, currentUSerDao);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        setupRecycler();
        SetupAllCourses(coursesDAO, orgsDAO, api);
        setupOrgList(orgsDAO, api);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("DEBUG", "onResume of HomeFragment");
        super.onResume();
        mShimmerViewContainer.startShimmer();
        //Code to refresh listview
        customAdapterMyCourses.notifyDataSetChanged();
        trainerCoursesRecyclerView.setAdapter(customAdapterMyCourses);
        customAdapterCourses.notifyDataSetChanged();
        recyclerViewAllCourses.setAdapter(customAdapterCourses);
        customAdapterOrganizations.notifyDataSetChanged();
        recyclerViewOrg.setAdapter(customAdapterOrganizations);
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of HomeFragment");
        super.onPause();
    }

    private void setupMyCourses(StitchApi api, CurrentUserDAO currentUSerDao) {
        if (internetIsConnected()) {
            Call<List<CourseContentM>> call = api.getCurrentContent(token);
            call.enqueue(new Callback<List<CourseContentM>>() {
                @Override
                public void onResponse(Call<List<CourseContentM>> call, Response<List<CourseContentM>> response) {
                    refreshLayout.setRefreshing(false);
                    if(response.isSuccessful()){
                        AppDatabase database = Room.databaseBuilder(TraineeCoursesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                                .allowMainThreadQueries()
                                .build();
                        List<CourseContentM> tempData = response.body();
                        if (null != tempData) {
                            if (tempData.size() > 0) {
                                courseListMy.clear();
                                database.getContentCourseDAO().deleteAllCourseContent();
                                for (CourseContentM course : tempData) {
                                    CourseContentM tempStored = course;
                                    courseListMy.add(new CourseContentModel("" + tempStored.getId(), "" + tempStored.getTitle(),
                                            ""+tempStored.getBanner() ,""+tempStored.getShortDesc(),""+tempStored.getDescription(),
                                            ""+tempStored.getObjective(),""+tempStored.getRating(),""+tempStored.getOrg().getId(),
                                            ""+tempStored.getOrg().getName(),""+tempStored.getOrg().getLogo() ,
                                            tempStored.getChapters()));

                                    database.getContentCourseDAO().insert(new CourseContentModel("" + tempStored.getId(), "" + tempStored.getTitle(),
                                            ""+tempStored.getBanner() ,""+tempStored.getShortDesc(),""+tempStored.getDescription(),
                                            ""+tempStored.getObjective(),""+tempStored.getRating(),""+tempStored.getOrg().getId(),
                                            ""+tempStored.getOrg().getName(),""+tempStored.getOrg().getLogo() ,
                                            tempStored.getChapters()));

                                    DownloadImage(""+tempStored.getOrg().getLogo());
                                    DownloadImage(""+tempStored.getBanner());

//                                    for (ContentChapters cc : tempStored.getChapters()) {
//                                        for (Comments comments:cc.getComments()){
//                                            if (comments.getUser().getPic() == null){
//
//                                            } else {
//                                                DownloadImage(""+comments.getUser().getPic());
//                                            }
//
//                                        }
//                                    }
                                }
                                customAdapterMyCourses.notifyDataSetChanged();
                            }
                        }
                    }
                }
                @Override
                public void onFailure(Call<List<CourseContentM>> call, Throwable t) {}
            });

        } else {
            AppDatabase database = Room.databaseBuilder(TraineeCoursesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
            courseListMy.clear();
            for (CourseContentModel coursesMyList : database.getContentCourseDAO().getCourseContentAll()) {
                courseListMy.add(coursesMyList);
                customAdapterMyCourses.notifyDataSetChanged();
            }
        }
    }



    private void SetupAllCourses(CoursesDAO coursesDAO, OrgsDAO orgsDAO, StitchApi api) {
        courseListStored = coursesDAO.getCoursesAll();
        if (courseListStored == null || courseListStored.size() <= 0) {
        } else {
            for (CoursesModal a : coursesDAO.getCoursesAll()) {
                courseList.add(a);
            }
            customAdapterCourses.notifyDataSetChanged();
        }
        Call<List<CoursesM>> call = api.getCourses("bearer " + tempStored.get(0).getToken());
        call.enqueue(new Callback<List<CoursesM>>() {
            @Override
            public void onResponse(Call<List<CoursesM>> call, Response<List<CoursesM>> response) {
                if (response.isSuccessful()) {
                    List<CoursesM> tempServer = response.body();
                    coursesDAO.deleteAllCurrent();
                    for (CoursesM a : tempServer) {
                        if (a.getBanner() != null) {
                            a.setBannerLocal(DownloadImage(a.getBanner()));
                        }
                        String orgName = a.getOrgId();

                        coursesDAO.insert(new CoursesModal("" + a.getId(), "" + orgName, ""
                                + a.getTitle(), "" + a.getBanner(), ""
                                + a.getBannerLocal(), "" + a.getShortDesc(), ""
                                + a.getDescription(), "" + a.getObjective(), ""
                                + a.getPassing_mark(), "" + a.getVisitor_count(), ""
                                + a.getIs_featured(), "" + a.getPublished(), ""
                                + a.getCreatedAt(), "" + a.getUpdatedAt(), ""
                                + a.getDeletedAt(), a.getTrainers(),a.getRating()));
                        for (Trainers t : a.getTrainers()) {
                            DownloadImage(t.getPic());
                        }
                    }

                    courseListStored = coursesDAO.getCoursesAll();
                    courseList.clear();
                    for (CoursesModal c : courseListStored) {
                        courseList.add(c);
                    }
                    customAdapterCourses.notifyDataSetChanged();

                } else {
                    courseListStored = coursesDAO.getCoursesAll();
                    if (courseListStored == null || courseListStored.size() <= 0) {

                    } else {
                        courseList.clear();
                        for (CoursesModal a : coursesDAO.getCoursesAll()) {
                            courseList.add(a);
                        }
                        customAdapterCourses.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CoursesM>> call, Throwable t) {
                courseListStored = coursesDAO.getCoursesAll();
                if (courseListStored == null || courseListStored.size() <= 0) {

                } else {
                    courseList.clear();
                    for (CoursesModal a : coursesDAO.getCoursesAll()) {
                        courseList.add(a);
                    }
                    customAdapterCourses.notifyDataSetChanged();
                }
            }
        });
    }

    private void setupOrgList(OrgsDAO orgsDAO, StitchApi api) {
        orgListStored = orgsDAO.getOrgsAll();
        for (OrgsModal d : orgListStored) {
            orgList.add(d);
        }

        customAdapterOrganizations.notifyDataSetChanged();
        setupMyCourses(api, currentUSerDao);

        Call<List<OrgsM>> call2 = api.getOrgs("bearer " + tempStored.get(0).getToken());
        call2.enqueue(new Callback<List<OrgsM>>() {
            @Override
            public void onResponse(Call<List<OrgsM>> call, Response<List<OrgsM>> response) {

                if (response.isSuccessful()) {
                    orgsDAO.deleteAllOrgs();
                    List<OrgsM> tempdata = response.body();
                    for (OrgsM b : tempdata) {

                        if (b.getLogo() != null) {
                            b.setLogo_local(DownloadImage(b.getLogo()));
                        }
                        orgsDAO.insert(new OrgsModal("" + b.getId(), ""
                                + b.getName(), "" + b.getLogo(), "" + b.getLogo_local(), ""
                                + b.getShortDesc(), "" + b.getAddress(), ""
                                + b.getCreatedAt(), "" + b.getUpdatedAt(), ""
                                + b.getDeletedAt()));
                    }
                    orgListStored.clear();
                    orgListStored = orgsDAO.getOrgsAll();
                    orgList.clear();
                    for (OrgsModal d : orgListStored) {
                        orgList.add(d);
                    }
                    customAdapterOrganizations.notifyDataSetChanged();

                } else {
                    orgListStored = orgsDAO.getOrgsAll();
                    if (orgListStored == null || orgListStored.size() <= 0) {
                    } else {
                        orgList.clear();
                        for (OrgsModal d : orgListStored) {
                            orgList.add(d);
                        }
                        customAdapterOrganizations.notifyDataSetChanged();
                    }
                }
                setupMyCourses(api, currentUSerDao);
            }

            @Override
            public void onFailure(Call<List<OrgsM>> call, Throwable t) {
                orgListStored = orgsDAO.getOrgsAll();

                if (orgListStored == null || orgListStored.size() <= 0) {
                } else {
                    orgListStored = orgsDAO.getOrgsAll();
                    orgList.clear();
                    for (OrgsModal d : orgListStored) {
                        orgList.add(d);
                    }

                    customAdapterOrganizations.notifyDataSetChanged();
                    setupMyCourses(api, currentUSerDao);
                }
            }
        });
    }


    public boolean internetIsConnected() {
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }

    private void setupRecycler() {
        trainerCoursesRecyclerView = findViewById(R.id.recyclerViewCourses);
        trainerCoursesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        trainerCoursesRecyclerView.setNestedScrollingEnabled(false);

        customAdapterMyCourses = new CustomAdapterMyCourses(this, courseListMy);
        customAdapterMyCourses.setClickListener(this);
        trainerCoursesRecyclerView.setAdapter(customAdapterMyCourses);

        // Stopping Shimmer Effect's animation after data is loaded to ListView
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);

        recyclerViewAllCourses = findViewById(R.id.recyclerViewAllCourses);
        recyclerViewAllCourses.setNestedScrollingEnabled(false);
        recyclerViewAllCourses.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        customAdapterCourses = new CustomAdapterCourses(this, courseList);
        customAdapterCourses.setClickListener(this);
        recyclerViewAllCourses.setAdapter(customAdapterCourses);

        recyclerViewOrg = findViewById(R.id.recyclerViewOrganizations);
        recyclerViewOrg.setNestedScrollingEnabled(false);
        recyclerViewOrg.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        customAdapterOrganizations = new CustomAdapterOrganizations(this, orgList);
        customAdapterOrganizations.setClickListener(this);
        recyclerViewOrg.setAdapter(customAdapterOrganizations);
    }


    private void showSideBar2(List<ContentChapters> contentChapters,String courseID) {
        AlertDialog.Builder dialogBuilder;
        AlertDialog alertDialog;
        dialogBuilder = new AlertDialog.Builder(TraineeCoursesActivity.this);
        View layoutView = getLayoutInflater().inflate(R.layout.dialog_chapters, null);

        dialogBuilder.setView(layoutView);

        ImageView imageViewCross = layoutView.findViewById(R.id.imageViewCross);
        recyclerViewChapter = layoutView.findViewById(R.id.recyclerviewChapters2);
        recyclerViewChapter.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewChapter.setNestedScrollingEnabled(false);

//        Collections.sort(commentList, new Comparator<Chapters>(){
//            public int compare(Chapters obj1, Chapters obj2) {
//
//                return obj1.getTitle().compareToIgnoreCase(obj2.getTitle()); // To compare string values
//
//            }
//        });
        Collections.sort(contentChapters, new Comparator<ContentChapters>() {
            public int compare(ContentChapters obj1, ContentChapters obj2) {
                // ## Ascending order
                return obj1.getId().compareToIgnoreCase(obj2.getId()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });
        customAdapterChapters = new CustomAdapterChapters(TraineeCoursesActivity.this, contentChapters,courseID);
        customAdapterChapters.setClickListener(TraineeCoursesActivity.this);
        recyclerViewChapter.setAdapter(customAdapterChapters);

        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        customAdapterChapters.notifyDataSetChanged();
        imageViewCross.setOnClickListener(v -> alertDialog.dismiss());

    }

    private void showCertificates() {
        Log.e(TAG,"Evaluations");
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_layout, findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("Show Certifiactes");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 15);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.linearLayoutItemView || view.getId() == R.id.textViewButtonDetails) {
            View v = trainerCoursesRecyclerView.getLayoutManager().findViewByPosition(position);
            ImageView textViewTemp = v.findViewById(R.id.imageViewMyCourseBanner);

            Intent intent2 = new Intent(TraineeCoursesActivity.this, CourseDetailsActivity.class);
            intent2.putExtra("courseId", "" + customAdapterMyCourses.getCourseID(position));
            intent2.putExtra("token", "" + token);

            TextView textViewTemp1 = v.findViewById(R.id.textViewProgressWord);
            TextView textViewTemp2 = v.findViewById(R.id.textViewChapterTally);

            intent2.putExtra("progressText1", "" + textViewTemp1.getText().toString());
            intent2.putExtra("progressText2", "" + textViewTemp2.getText().toString());
            startActivity(intent2);
        }
        else if (view.getId() == R.id.textViewJoin) {
            Boolean isFound = false;
            for (CourseContentModel coursesModal : courseListMy) {
                if (coursesModal.getId().matches(customAdapterCourses.getCourseID(position))) {
                    isFound = true;
                }
            }
            if (isFound) {
                int idPosition = customAdapterMyCourses.getPositionbyCourseID(customAdapterCourses.getCourseID(position));
                showSideBar2(customAdapterMyCourses.getChapters(idPosition), customAdapterCourses.getCourseID(position));

            } else {
                Intent intent = new Intent(TraineeCoursesActivity.this, JoinCourseRequestActivity.class);
                intent.putExtra("courseId", "" + customAdapterCourses.getCourseID(position));
                intent.putExtra("token", "" + token);
                startActivity(intent);
            }

        }
        else if (view.getId() == R.id.linearAllcore) {
            Boolean isFound = false;
            for (CourseContentModel coursesModal : courseListMy) {
                if (coursesModal.getId().matches(customAdapterCourses.getCourseID(position))) {
                    isFound = true;
                }
            }
            if (isFound) {
                int idPosition = customAdapterMyCourses.getPositionbyCourseID(customAdapterCourses.getCourseID(position));
                showSideBar2(customAdapterMyCourses.getChapters(idPosition), customAdapterCourses.getCourseID(position));

            } else {
                Intent allCoursesIntent = new Intent(TraineeCoursesActivity.this, AllCoursesActivity.class);
                allCoursesIntent.putExtra("courseId", "" + customAdapterCourses.getCourseID(position));
                allCoursesIntent.putExtra("token", "" + token);
                startActivity(allCoursesIntent);
            }
        }
        else if (view.getId() == R.id.textViewButton) {
            TextView textViewTemp = (TextView) view;
            String textT = "";
            textT = "" + textViewTemp.getText().toString();
            if (textT.matches("Evaluation")) {

                Call<CoursesByIdModel> callRequest = api.getExamId("" + token, customAdapterMyCourses.getCourseID(position));
                callRequest.enqueue(new Callback<CoursesByIdModel>() {
                    @Override
                    public void onResponse(Call<CoursesByIdModel> call, Response<CoursesByIdModel> response) {
                        if (response.isSuccessful()) {
                            Singleton.getInstance().setExamId(response.body().getExams().get(0).getId());

                            if(evaluationDao.getEvaluationModal(customAdapterMyCourses.getCourseID(position)).size()==1){
                                Toast.makeText(getApplicationContext(), "You Already Evaluated", Toast.LENGTH_SHORT).show();
                            } else {
                                Intent evaluationIntent = new Intent(TraineeCoursesActivity.this, EvaluationActivity.class);
                                evaluationIntent.putExtra("COURSEID", "" + customAdapterMyCourses.getCourseID(position));
                                evaluationIntent.putExtra("CHAPTERID", "" + response.body().getChapterId());
                                Log.d(TAG, customAdapterMyCourses.getCourseID(position));
                                startActivity(evaluationIntent);
                            }


                        } else {
                            Toast.makeText(TraineeCoursesActivity.this, "" + "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CoursesByIdModel> call, Throwable t) {
                        Toast.makeText(TraineeCoursesActivity.this, "" + "Something Went Wrong", Toast.LENGTH_LONG).show();

                    }
                });
                } else {
                showSideBar2(customAdapterMyCourses.getChapters(position), customAdapterMyCourses.getCourseID(position));
            }

        } else if (view.getId() == R.id.buttonChapter) {
            Intent i = new Intent(TraineeCoursesActivity.this, ChapterDetailsActivity.class);
            i.putExtra("COURSEID", "" + customAdapterChapters.getCourserID(position));
            i.putExtra("CHAPTERID", "" + customAdapterChapters.getChapterID(position));
            startActivity(i);
        }  else if (view.getId() == R.id.buttonTakeQuiz) {
            Intent k = new Intent(TraineeCoursesActivity.this, ExamActivity.class);

            k.putExtra("COURSEID", "" + customAdapterChapters.getCourserID(position));
            k.putExtra("CHAPTERID", "" + customAdapterChapters.getChapterID(position));

            startActivity(k);
        } else if (view.getId() == R.id.linearOrg) {
            Intent i = new Intent(TraineeCoursesActivity.this, OrganizationDetailsActivity.class);
            i.putExtra("OrgID", "" + customAdapterOrganizations.getOrgId(position));

            startActivity(i);
        } else if (view.getId() == R.id.buttonCertificate) {
            if (position == 0) {
                Content content = new Content("Please grant me a certificate!");
                Gson gson = new Gson();

                Call<ResponseBody> callRequest = api.postRequestCertificate("" + token, "trainee_certificate", "0" , "", "" + gson.toJson(content));
                callRequest.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
//                            Toast.makeText(TraineeCoursesActivity.this, "Request Sent", Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(TraineeCoursesActivity.this, "" + response.message(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                        Toast.makeText(TraineeCoursesActivity.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        }
    }
}