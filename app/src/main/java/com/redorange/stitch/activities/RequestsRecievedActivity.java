package com.redorange.stitch.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterRequests;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.RequestSentByTraineeM;
import com.redorange.stitch.entities.SentByUser;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class RequestsRecievedActivity extends BaseActivity
        implements CustomAdapterRequests.ItemClickListener {

    public String TAG = RequestsRecievedActivity.class.getSimpleName();

    CustomAdapterRequests adapter;
    List<SentByUser> sentTOUserServer = new ArrayList<>();
    StitchApi api;
    List<CurrentUserModal> tempStored;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_requests_recieved);
        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
        Intent intent = getIntent();

        token = intent.getExtras().getString("token");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerviewRequest);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);

        adapter = new CustomAdapterRequests(this, sentTOUserServer);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);


        AppDatabase database = Room.databaseBuilder(RequestsRecievedActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        CurrentUserDAO currentUSerDao = database.getCurrentUserDAO();

        tempStored = currentUSerDao.getCurrentUser();

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(RequestsRecievedActivity.this, tempStored, token));


        Call<RequestSentByTraineeM> call = api.getRequestJoin(token);
        call.enqueue(new Callback<RequestSentByTraineeM>() {
            @Override
            public void onResponse(Call<RequestSentByTraineeM> call, Response<RequestSentByTraineeM> response) {
                if (response.isSuccessful()) {
                    List<SentByUser> request = response.body().getSentToUser();
                    for (SentByUser a : request) {
                        sentTOUserServer.add(a);
                    }
                    adapter.notifyDataSetChanged();
                    Log.d(TAG, "onResponse: ");
                } else {
                    Log.d(TAG, "" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<RequestSentByTraineeM> call, Throwable t) {
                Log.d(TAG, "" + t.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

        if (view.getId() == R.id.imageViewCancel) {

            if(adapter.getStatus(position).matches("approved")){
                Toast.makeText(getApplicationContext(), "Request Already Approved", Toast.LENGTH_SHORT).show();
            }else {
                Call<ResponseBody> callCancel = api.getDELETErequest(token, "" + adapter.getRequestId(position));
                callCancel.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.custom_toast_layout,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));


                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Request Cancelled");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 15);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            onBackPressed();
                            finish();
                        } else {
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.custom_toast_layout,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));


                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Cannot Cancel Request");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 15);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.custom_toast_layout,
                                (ViewGroup) findViewById(R.id.toast_layout_root));


                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Cannot Reach Server");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.BOTTOM, 0, 15);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();
                    }
                });
            }
        }
    }
}