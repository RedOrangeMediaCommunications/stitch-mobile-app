package com.redorange.stitch.activities;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterChapters;
import com.redorange.stitch.adapters.CustomAdapterTrainer;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.ContentCoursesDAO;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class CourseDetailsActivity extends BaseActivity implements
        CustomAdapterChapters.ItemClickListener, CustomAdapterTrainer.ItemClickListener {

    public String TAG = CourseDetailsActivity.class.getSimpleName();

    List<CurrentUserModal> tempStored;
    List<ContentChapters> chaptersList = new ArrayList<>();
    List<Trainers> trainersList = new ArrayList<>();

    String courseId;

    CustomAdapterChapters customAdapterChapters;
    CustomAdapterTrainer adapterTrainer;

    RecyclerView recyclerviewChapters;
    StitchApi api;

    CoursesDAO coursesDAO;
    CurrentUserDAO currentUSerDao;
    CoursesModal courseStored;

    AppDatabase database;

    RecyclerView recyclerViewTrainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_course_details);

        Intent intent = getIntent();
        courseId = intent.getExtras().getString("courseId");
        String token = intent.getExtras().getString("token");
        String progressText1 = intent.getExtras().getString("progressText1");
        String progressText2 = intent.getExtras().getString("progressText2");
        ((TextView) findViewById(R.id.tprogressText1)).setText("" + progressText1);
        ((TextView) findViewById(R.id.tprogressText2)).setText("" + progressText2);
        ProgressBar progressBar=findViewById(R.id.progressBar3);

        database = Room.databaseBuilder(CourseDetailsActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        currentUSerDao = database.getCurrentUserDAO();
        coursesDAO = database.getCoursesDAO();
        courseStored = coursesDAO.getGetCoursesbyID(courseId);
        tempStored = currentUSerDao.getCurrentUser();

        RatingBar ratingBar=findViewById(R.id.simpleRatingBarCourse);
        ratingBar.setRating(0);
        if(null!=courseStored.getRating()){
            ratingBar.setRating(Float.parseFloat(courseStored.getRating()));
        }
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> {
            api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
            Call<ResponseBody> call=api.postCourseRating(token,courseId,rating);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NotNull Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        setCurrentRating(rating);
                    }else {
                        Toast.makeText(CourseDetailsActivity.this, ""+response.message().toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                private void setCurrentRating(float rating) {
                    LayerDrawable drawable = (LayerDrawable) ratingBar1.getProgressDrawable();
                    setRatingStarColor(drawable.getDrawable(2), ContextCompat.getColor(CourseDetailsActivity.this, R.color.starcolor));
                    setRatingStarColor(drawable.getDrawable(1), ContextCompat.getColor(CourseDetailsActivity.this, R.color.starcolor));
                    setRatingStarColor(drawable.getDrawable(0), ContextCompat.getColor(CourseDetailsActivity.this, R.color.textColor));
                }

                private void setRatingStarColor(Drawable drawable, @ColorInt int color) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    {
                        DrawableCompat.setTint(drawable, color);
                    } else {
                        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(CourseDetailsActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });


        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ContentCoursesDAO contentCoursesDAO = database.getContentCourseDAO();

        CourseContentModel chaptersFromContent = contentCoursesDAO.getGetCourseContentbyID(courseId);

        TextView title = findViewById(R.id.textViewCourseTitle);
        title.setText("" + courseStored.getTitle());

        TextView description = findViewById(R.id.textViewDiscription);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(chaptersFromContent.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            description.setText(Html.fromHtml(chaptersFromContent.getDescription()));
        }

        TextView textViewObjective = findViewById(R.id.textViewObjective);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textViewObjective.setText(Html.fromHtml(chaptersFromContent.getObjective(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            textViewObjective.setText(Html.fromHtml(chaptersFromContent.getObjective()));
        }

        findViewById(R.id.relative).setOnClickListener(v ->{
            recyclerViewTrainer = findViewById(R.id.recyclerVIewTrainer);
            if (recyclerViewTrainer.getVisibility() == View.GONE) {
                recyclerViewTrainer.setAlpha(0);
                recyclerViewTrainer.setVisibility(View.VISIBLE);

                recyclerViewTrainer.setVisibility(View.VISIBLE);
                recyclerViewTrainer.animate().alpha(1);
                findViewById(R.id.dropdownRecycler).animate().rotation(0);
            } else {
                findViewById(R.id.dropdownRecycler).animate().rotation(180);
                recyclerViewTrainer.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.linearDescription).setOnClickListener(v -> {
            TextView textViewInstructions = findViewById(R.id.textViewDiscription);
            if (textViewInstructions.getVisibility() == View.GONE) {
                textViewInstructions.setAlpha(0);
                textViewInstructions.setVisibility(View.VISIBLE);

                textViewInstructions.setVisibility(View.VISIBLE);
                textViewInstructions.animate().alpha(1);
                findViewById(R.id.dropdownDescription).animate().rotation(0);
            } else {
                findViewById(R.id.dropdownDescription).animate().rotation(180);
                textViewInstructions.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.linearObjective).setOnClickListener(v -> {
            TextView textViewInstructions = findViewById(R.id.textViewObjective);
            if (textViewInstructions.getVisibility() == View.GONE) {
                textViewInstructions.setAlpha(0);
                textViewInstructions.setVisibility(View.VISIBLE);

                textViewInstructions.setVisibility(View.VISIBLE);
                textViewInstructions.animate().alpha(1);
                findViewById(R.id.dropdownObjective).animate().rotation(0);
            } else {
                findViewById(R.id.dropdownObjective).animate().rotation(180);
                textViewInstructions.setVisibility(View.GONE);
            }

        });


        recyclerviewChapters = findViewById(R.id.recyclerviewChapters);
        recyclerviewChapters.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerviewChapters.setNestedScrollingEnabled(false);

        int TotalChapters=0;
        int TotalCompleted=0;

        if(null!=chaptersFromContent.getChapters()){
            for (ContentChapters a : chaptersFromContent.getChapters()) {
                chaptersList.add(a);

            }
            TotalChapters=chaptersFromContent.getChapters().size();
            for(ContentChapters chapters:chaptersFromContent.getChapters()){
                if(null!=database.getProgressWDao().getProgressbyChapterID(chapters.getId())){
                    TotalCompleted++;
                }
            }
        }

        progressBar.setMax(TotalChapters);
        progressBar.setProgress(TotalCompleted);

        Collections.sort(chaptersList, new Comparator<ContentChapters>() {
            public int compare(ContentChapters obj1, ContentChapters obj2) {
                // ## Ascending order
                return obj1.getId().compareToIgnoreCase(obj2.getId()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });

        customAdapterChapters = new CustomAdapterChapters(this, chaptersList,courseId);
        customAdapterChapters.setClickListener(this);
        recyclerviewChapters.setAdapter(customAdapterChapters);

        recyclerViewTrainer = findViewById(R.id.recyclerVIewTrainer);
        recyclerViewTrainer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewTrainer.setNestedScrollingEnabled(false);

        for (Trainers b : courseStored.getTrainers()) {
            trainersList.add(b);
        }

        adapterTrainer = new CustomAdapterTrainer(this, trainersList,token);
        adapterTrainer.setClickListener(this);
        recyclerViewTrainer.setAdapter(adapterTrainer);

        ImageView imageViewBanner = findViewById(R.id.imageViewBanner);

        if (courseStored.getBannerLocal() != null) {
            File imgFile = new File(courseStored.getBannerLocal());

            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageViewBanner.setImageBitmap(myBitmap);
            }
        }

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(CourseDetailsActivity.this, tempStored, token));
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.buttonChapter) {
            Intent chapterIntent = new Intent(CourseDetailsActivity.this, ChapterDetailsActivity.class);
            chapterIntent.putExtra("COURSEID", "" + courseId);
            chapterIntent.putExtra("TRAINERID", "1" );
            chapterIntent.putExtra("CHAPTERID", "" + customAdapterChapters.getChapterID(position));
            startActivity(chapterIntent);

        } else if (view.getId() == R.id.buttonTakeQuiz) {
            Intent examIntent = new Intent(CourseDetailsActivity.this, ExamActivity.class);
            examIntent.putExtra("COURSEID", "" + courseId);
            examIntent.putExtra("CHAPTERID", "" + customAdapterChapters.getChapterID(position));
            startActivity(examIntent);

        }  else if (view.getId() == R.id.textViewTrainerProfile) {
            Intent intent = new Intent(CourseDetailsActivity.this, TraineeSeesTrainerActivity.class);
            intent.putExtra("COURSEID", "" + courseId);
            intent.putExtra("TRAINERID", "" + adapterTrainer.getTrainerID(position));
            startActivity(intent);
        } else if (view.getId() == R.id.tvMessage){
            Intent intentMessage = new Intent(CourseDetailsActivity.this, SendMessageActivity.class);
            intentMessage.putExtra("COURSEID", "" + courseId);
            intentMessage.putExtra("TRAINERID", "" + adapterTrainer.getTrainerID(position));
            Log.d(TAG, adapterTrainer.getTrainerID(position) );
            startActivity(intentMessage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        customAdapterChapters.notifyDataSetChanged();
        recyclerviewChapters.setAdapter(customAdapterChapters);
    }
}

//else if (view.getId() == R.id.buttonTakeQuiz) {
//        Intent k = new Intent(CourseDetailsActivity.this, ExamActivity.class);
//        k.putExtra("COURSEID", "" + courseId);
//        k.putExtra("CHAPTERID", "" + customAdapterChapters.getChapterID(position));
//        startActivity(k);
//        }