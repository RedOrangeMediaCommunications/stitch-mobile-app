package com.redorange.stitch.activities;

import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.constant.ApplicationConstants;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.Singleton;

import java.util.List;

public class SplashActivity extends BaseActivity {
    AppDatabase database;
    CurrentUserDAO currentUSerDao;
    List<CurrentUserModal> tempStored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        getSupportActionBar().hide();

        setContentView(R.layout.activity_splash);
        Singleton.getInstance().setContext(getApplicationContext());
        database = Room.databaseBuilder(SplashActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();

        loader();
    }

    private void loader() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (tempStored.size() != 0) {
                Intent intent;
                if (tempStored.get(0).isLoggedIN()) {
                    intent = new Intent(SplashActivity.this, TraineeCoursesActivity.class);

                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                }
                startActivity(intent);
            } else {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            finish();
        }, ApplicationConstants.APP_LOAD_TIME);
    }
}