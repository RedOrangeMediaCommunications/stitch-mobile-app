package com.redorange.stitch.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.loader.content.CursorLoader;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.entities.RegisterInfoM;
import com.redorange.stitch.typeconverter.ImageFilePath;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.Singleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.constant.ApplicationConstants.PICK_PHOTO_FOR_AVATAR;

public class RegisterActivity extends BaseActivity {
    public String TAG = RegisterActivity.class.getSimpleName();
    ImageView profilePic;
    AutoCompleteTextView rolesSpinner;
    public String imagePath ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register);

        Singleton.getInstance().setContext(getApplicationContext());
        Button uploadButton = findViewById(R.id.buttonUpload);
        profilePic = findViewById(R.id.imageViewProfile);
        EditText name = findViewById(R.id.editTextOTP);
        EditText phone = findViewById(R.id.editTextPhone);
        EditText password = findViewById(R.id.editTextPassword);
        EditText confirmPassword = findViewById(R.id.editTextConfirmPassword);
        EditText email = findViewById(R.id.editTextEmail);
        EditText bio = findViewById(R.id.editTextBio);
        EditText address = findViewById(R.id.editTextAddress);
        EditText registerAs = findViewById(R.id.editTextAddress);
        TextView register = findViewById(R.id.register);
        LinearLayout buttonProfile = findViewById(R.id.buttonProfile);

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rolesSpinner = findViewById(R.id.customerTextView);
        ArrayList<String> customerList = getRolesList();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(RegisterActivity.this, android.R.layout.simple_spinner_item, customerList);
        rolesSpinner.setAdapter(adapter);

        rolesSpinner.setFocusable(false);
        rolesSpinner.setClickable(true);


        uploadButton.setOnClickListener(v -> pickImage());


        buttonProfile.setOnClickListener(v -> pickImage());
        register.setOnClickListener(v -> {

            if (password.getText().toString().matches(confirmPassword.getText().toString())) {

//                RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(Singleton.getInstance().getAvater())), file);
//                RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), desc);
                StitchApi api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
                Call<RegisterInfoM> call = api.getRegister(
                        "" + name.getText().toString(),
                        "" + password.getText().toString(),
                        "" + bio.getText().toString(),
                        "" + phone.getText().toString(),
                        "" + email.getText().toString(),
                        "" + address.getText().toString(), "false",
                        "" + "Trainee");
                call.enqueue(new Callback<RegisterInfoM>() {
                    @Override
                    public void onResponse(Call<RegisterInfoM> call, Response<RegisterInfoM> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Registered", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                            startActivity(intent);
                        } else {

                            Toast.makeText(RegisterActivity.this, response.message().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterInfoM> call, Throwable t) {

                    }
                });
            } else {
                Toast.makeText(RegisterActivity.this, "Password Did Not Match", Toast.LENGTH_SHORT).show();
            }


        });

    }

    private ArrayList<String> getRolesList() {
        ArrayList<String> roles = new ArrayList<>();
        roles.add("Trainee");
        roles.add("Trainer");
        return roles;
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                Uri uri = data.getData();
                imagePath = ImageFilePath.getPath(RegisterActivity.this, uri);
//                imagePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                Log.i(TAG, "onActivityResult: file path : " + imagePath);
                InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);

                profilePic.setImageBitmap(bmp);

                Singleton.getInstance().setAvater(imagePath);
//
//                Toast.makeText(RegisterActivity.this, "True", Toast.LENGTH_SHORT).show();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(RegisterActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
}