package com.redorange.stitch.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterAlMessages;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.entities.MessageGet;
import com.redorange.stitch.listeners.MessageThread;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.Singleton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class AllMessagesActivity extends BaseActivity implements MessageThread {
    RecyclerView recyclerView;
    List<MessageGet> messageListAll = new ArrayList<>();
    ImageView backButton;

    String token;
    List<CurrentUserModal> tempStored;
    StitchApi api;
    CustomAdapterAlMessages customAdapterAlMessages;
    AllMessagesActivity listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_all_messages);

        listener = this;

        AppDatabase database = Room.databaseBuilder(AllMessagesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        tempStored = database.getCurrentUserDAO().getCurrentUser();
        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
        token = "bearer " + tempStored.get(0).getToken();

        recyclerView = findViewById(R.id.recyclerviewAllMessages);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);

        customAdapterAlMessages = new CustomAdapterAlMessages(this, listener, messageListAll);
//        customAdapterAlMessages.setClickListener(this);
        recyclerView.setAdapter(customAdapterAlMessages);

        callMessage();

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        backButton = findViewById (R.id.backButton);
        sidebarIcon.setOnClickListener(view -> showSideBar(AllMessagesActivity.this, tempStored, token));
        backButton.setOnClickListener(view -> {onBackPressed(); });
    }

    private void callMessage() {
        Call<List<MessageGet>> callMessage=api.getMessages(""+token,""+tempStored.get(0).getId());
        callMessage.enqueue(new Callback<List<MessageGet>>() {
            @Override
            public void onResponse(Call<List<MessageGet>> call, Response<List<MessageGet>> response) {
                if(response.isSuccessful()){
                    List<MessageGet> tempServer=response.body();
                    for (MessageGet m: tempServer){
                        messageListAll.add(m);
                    }
                    customAdapterAlMessages.notifyDataSetChanged();
                }else{
                    Toast.makeText(AllMessagesActivity.this, "Something Went Wrong"+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<MessageGet>> call, Throwable t) {
                Toast.makeText(AllMessagesActivity.this, "Something Went Wrong "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(String threadId, int position, String name, String img) {
        Intent intentMessage = new Intent(AllMessagesActivity.this, MessageActivity.class);
        AppDatabase database = Room.databaseBuilder(AllMessagesActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        List<CoursesModal> temp1=database.getCoursesDAO().getCoursesAll();
        String courseId="0";
        for (CoursesModal coursesModal:temp1){
            courseId=coursesModal.getId();
        }
        intentMessage.putExtra("COURSEID", "" + courseId);

        intentMessage.putExtra("TRAINERID", "" + customAdapterAlMessages.getTrainerid(position));
        intentMessage.putExtra("threadID", threadId);

        Singleton.getInstance().setSenderImg(img);
        Singleton.getInstance().setSenderName(name);

        startActivity(intentMessage);
    }
}