package com.redorange.stitch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.WindowManager;
import android.webkit.URLUtil;

import com.github.barteksc.pdfviewer.PDFView;
import com.redorange.stitch.R;
import com.redorange.stitch.utils.BaseActivity;

import java.io.File;

public class PdfFullscreenActivity extends BaseActivity {
    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_pdf_fullscreen);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Intent intent = getIntent();
        String path = intent.getExtras().getString("PDFURL");

        pdfView = findViewById(R.id.pdfView);
        String filename = URLUtil.guessFileName(path, null, null);
        String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/.stitch";

        File file = new File(downloadPath, filename);
        if (file.exists()) {
            pdfView.fromFile(file).showMinimap(true).enableSwipe(false).enableDoubletap(true).load();
        }
    }
}