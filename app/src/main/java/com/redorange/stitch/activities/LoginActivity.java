package com.redorange.stitch.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.redorange.stitch.R;
import com.redorange.stitch.apis.APIError;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.OrgsDAO;
import com.redorange.stitch.entities.course.CoursesM;
import com.redorange.stitch.entities.LoginInfoM;
import com.redorange.stitch.entities.OrgsM;
import com.redorange.stitch.entities.Roles;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.modal.CoursesModal;

import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.Singleton;

import java.io.File;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.constant.ApplicationConstants.PERMISSION_REQUEST_CODE;

public class LoginActivity extends BaseActivity {

    AppDatabase database;
    List<CurrentUserModal> tempStored;
    String token;
    public StitchApi stitchApi;
    public  TextView register, emailT, passwordT, resetPassword;
    public EditText phoneEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        Singleton.getInstance().setContext(this);
        stitchApi = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);



        if (checkPermission()) {
            deleteExsitingFolder();
        } else {
            requestPermission();
        }

        register = findViewById(R.id.textViewRegister);
        emailT = findViewById(R.id.textViewEmail);
        passwordT = findViewById(R.id.textViewPassword);
        phoneEditText = findViewById(R.id.editTextOTP);
        passwordEditText = findViewById(R.id.etPass);

        register.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });

        resetPassword = findViewById(R.id.textViewResetPassword);
        resetPassword.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
        });

        TextView signIn = findViewById(R.id.textViewValidate);
        signIn.setOnClickListener(v -> {

            if (checkPermission()) {
                deleteExsitingFolder();
            } else {
                requestPermission();
            }

            String phoneNumber = phoneEditText.getText().toString();
            String passwordNumber = passwordEditText.getText().toString();
            if (phoneNumber.matches("") || passwordNumber.matches("")) {

                if (phoneNumber.matches("")) {
                    int colorFrom = getResources().getColor(R.color.red);
                    int colorTo = getResources().getColor(R.color.textColor);
                    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
                    colorAnimation.setDuration(500); // milliseconds
                    colorAnimation.addUpdateListener(animator -> {
                        emailT.setTextColor((int) animator.getAnimatedValue());
                        emailT.setTextSize(16);

                    });
                    colorAnimation.start();
                }

                if (passwordNumber.matches("")) {
                    int colorFrom = getResources().getColor(R.color.red);
                    int colorTo = getResources().getColor(R.color.textColor);
                    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
                    colorAnimation.setDuration(500); // milliseconds
                    colorAnimation.addUpdateListener(animator -> {

                        passwordT.setTextColor((int) animator.getAnimatedValue());
                        passwordT.setTextSize(16);
                    });
                    colorAnimation.start();
                }

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast_layout,
                        (ViewGroup) findViewById(R.id.toast_layout_root));


                TextView text = layout.findViewById(R.id.text);
                text.setText("Fields Empty");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 15);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();

            } else {
                callLoginApi(phoneNumber, passwordNumber);
            }
        });

        TextView oTP = findViewById(R.id.textViewOtp);

        oTP.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
            startActivity(intent);
        });
    }

    private void deleteExsitingFolder() {
        String downloadPathName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/.stitch/";
        String picturePathName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/";

        File downloadFolder = new File(downloadPathName);
        File pictureFolder = new File(picturePathName);

        if (downloadFolder.exists()){
            deleteAll(downloadFolder);
            deleteAll(pictureFolder);
        }

//        if(pictureFolder.exists()){
//            deleteAll(downloadFolder);
//            deleteAll(pictureFolder);
//        }
    }

    public static boolean deleteAll(File file) {
        if (file == null || !file.exists()) return false;

        boolean success = true;
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        success &= deleteAll(f);
                    }
                    if (!f.delete()) {
                        Log.w("deleteAll", "Failed to delete " + f);
                        success = false;
                    }
                }
            }

            else {
                for (File f : files) {
                    if (f.isDirectory()) {
                        success &= deleteAll(f);
                    }

                }
                if (!file.delete()) {
                    Log.w("deleteAll 1", "Failed to delete " + file);
                    success = false;
                }
            }
        } else {
            if (!file.delete()) {
                Log.w("deleteAll 2", "Failed to delete " + file);
                success = false;
            }
        }
        return success;
    }

    private void callLoginApi(String phoneNumber, String passwordNumber) {
        if (isNetworkAvailable()) {
            dialogUtil.showProgressDialog();
            Call<LoginInfoM> call = stitchApi.getLogin(phoneNumber, passwordNumber);
            call.enqueue(new Callback<LoginInfoM>() {
                @Override
                public void onResponse(Call<LoginInfoM> call, Response<LoginInfoM> response) {
                    database = Room.databaseBuilder(LoginActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                            .allowMainThreadQueries()
                            .build();

                    CurrentUserDAO currentUSerDao = database.getCurrentUserDAO();
                    List<CurrentUserModal> tempStored = currentUSerDao.getCurrentUser();

                    if (response.isSuccessful()) {
                        dialogUtil.dismissProgress();
                        if (response.body().getRoles().get(0).getName().equalsIgnoreCase("Organization Admin")
                                || response.body().getRoles().get(0).getName().equalsIgnoreCase("Trainer")) {
                            Log.d("Sani", "No Login Access");
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.custom_toast_layout, findViewById(R.id.toast_layout_root));

                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("You have no permission to login");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 15);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                        }

                        else {
                            Log.d("Sani_Check", "onResponse: " + response.isSuccessful());
                        LoginInfoM tempData = response.body();
                        currentUSerDao.deleteAllCurrent();

                        if (checkPermission()) {
                            String storedPath = DownloadImage(tempData.getPic());


                            String role = "Trainee";
                            for (Roles roleTemp : tempData.getRoles()) {
                                if (roleTemp.getName().matches("Trainer")) {
                                    role = "Trainer";
                                }
                            }

                            CurrentUserModal tempUser = new CurrentUserModal("" + tempData.getId(), "" + tempData.getName(), "" + tempData.getCv(), "" + tempData.getPic(), "" + storedPath, "" + tempData.getBio(), "" + tempData.getPhone(), "" + tempData.getEmail(), "" + tempData.getAddress(), "" + tempData.getToken(), true, "" + role);

                            currentUSerDao.insert(tempUser);
                            if( tempUser.getRole().matches("Trainee")){

                                CoursesDAO coursesDAO = database.getCoursesDAO();
                                OrgsDAO orgsDAO = database.getOrgsDAO();

                                currentUSerDao = database.getCurrentUserDAO();
                                tempStored = currentUSerDao.getCurrentUser();
                                token = "bearer " + tempStored.get(0).getToken();
                                SetupAllCourses(coursesDAO, orgsDAO, stitchApi);

                            }else {

                                CoursesDAO coursesDAO = database.getCoursesDAO();
                                OrgsDAO orgsDAO = database.getOrgsDAO();

                                currentUSerDao = database.getCurrentUserDAO();
                                tempStored = currentUSerDao.getCurrentUser();
                                token = "bearer " + tempStored.get(0).getToken();
                                SetupAllCourses(coursesDAO, orgsDAO, stitchApi);

                                Intent intent = new Intent(LoginActivity.this, TrainerCourses.class);
                                startActivity(intent);
                                finish();
                            }

                            requestPermission();
                            }
                        }

                    }

                    else {
                        dialogUtil.dismissProgress();
                        APIError message = new Gson().fromJson(response.errorBody().charStream(), APIError.class);

                        if (message.getMessage().matches("No user found")) {
                            int colorFrom = getResources().getColor(R.color.red);
                            int colorTo = getResources().getColor(R.color.textColor);
                            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
                            colorAnimation.setDuration(500); // milliseconds
                            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                                @Override
                                public void onAnimationUpdate(ValueAnimator animator) {
                                    emailT.setTextColor((int) animator.getAnimatedValue());
                                    emailT.setTextSize(16);

                                }

                            });
                            colorAnimation.start();
                        }

                        if (message.getMessage().matches("Incorrect Password")) {
                            int colorFrom = getResources().getColor(R.color.red);
                            int colorTo = getResources().getColor(R.color.textColor);
                            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
                            colorAnimation.setDuration(500); // milliseconds
                            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                                @Override
                                public void onAnimationUpdate(ValueAnimator animator) {

                                    passwordT.setTextColor((int) animator.getAnimatedValue());
                                    passwordT.setTextSize(16);
                                }

                            });
                            colorAnimation.start();
                        }
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.custom_toast_layout,
                                (ViewGroup) findViewById(R.id.toast_layout_root));


                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("" + message.getMessage());

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.BOTTOM, 0, 15);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();
                    }
                }

                @Override
                public void onFailure(Call<LoginInfoM> call, Throwable t) {
                    Log.d("Sani_Check", "onResponse: " + t.getMessage());
                    dialogUtil.dismissProgress();

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast_layout,
                            (ViewGroup) findViewById(R.id.toast_layout_root));


                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Login Failed. No Internet");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 15);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                }
            });
        } else {

            Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (result == PackageManager.PERMISSION_GRANTED) {
            deleteExsitingFolder();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

            }
        }
    }

    private void SetupAllCourses(CoursesDAO coursesDAO, OrgsDAO orgsDAO, StitchApi api) {
        AppDatabase database = Room.databaseBuilder(LoginActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        token = "bearer " + database.getCurrentUserDAO().getCurrentUser().get(0).getToken();
        Call<List<CoursesM>> call = api.getCourses(token);
        Log.d("TAG", "SetupAllCourses: "+token);
        call.enqueue(new Callback<List<CoursesM>>() {
            @Override
            public void onResponse(Call<List<CoursesM>> call, Response<List<CoursesM>> response) {
                if (response.isSuccessful()) {

                    List<CoursesM> tempServer = response.body();
                    coursesDAO.deleteAllCurrent();
                    for (CoursesM a : tempServer) {

                        if (a.getBanner() != null) {
                            a.setBannerLocal(DownloadImage(a.getBanner()));
                        }

                        String orgsName = a.getOrgId();


                        coursesDAO.insert(new CoursesModal("" + a.getId(), "" + orgsName, ""
                                + a.getTitle(), "" + a.getBanner(), ""
                                + a.getBannerLocal(), "" + a.getShortDesc(), ""
                                + a.getDescription(), "" + a.getObjective(), ""
                                + a.getPassing_mark(), "" + a.getVisitor_count(), ""
                                + a.getIs_featured(), "" + a.getPublished(), ""
                                + a.getCreatedAt(), "" + a.getUpdatedAt(), ""
                                + a.getDeletedAt(), a.getTrainers(),a.getRating()));
                        for (Trainers t : a.getTrainers()) {
                            DownloadImage(t.getPic());
                        }

                    }
                    setupOrgList(orgsDAO, api);
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<CoursesM>> call, Throwable t) {
            }
        });
    }

    private void setupOrgList(OrgsDAO orgsDAO, StitchApi api) {
        database = Room.databaseBuilder(LoginActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        token = "bearer " + database.getCurrentUserDAO().getCurrentUser().get(0).getToken();
        Call<List<OrgsM>> call2 = api.getOrgs(token);
        call2.enqueue(new Callback<List<OrgsM>>() {
            @Override
            public void onResponse(Call<List<OrgsM>> call, Response<List<OrgsM>> response) {
                if (response.isSuccessful()) {
                    orgsDAO.deleteAllOrgs();
                    List<OrgsM> tempData = response.body();
                    for (OrgsM b : tempData) {
                        if (b.getLogo() != null) {
                            b.setLogo_local(DownloadImage(b.getLogo()));
                        }
                        orgsDAO.insert(new OrgsModal("" + b.getId(), "" + b.getName(), "" + b.getLogo(), "" + b.getLogo_local(), "" + b.getShortDesc(), "" + b.getAddress(), "" + b.getCreatedAt(), "" + b.getUpdatedAt(), "" + b.getDeletedAt()));
                    }
                    Intent intent = new Intent(LoginActivity.this, TraineeCoursesActivity.class);
                    startActivity(intent);
                    finish();
                } else { }
            }

            @Override
            public void onFailure(Call<List<OrgsM>> call, Throwable t) {

            }
        });
    }
}