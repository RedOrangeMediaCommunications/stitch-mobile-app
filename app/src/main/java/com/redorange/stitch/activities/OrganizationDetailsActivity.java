package com.redorange.stitch.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterCourses;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.OrgsDAO;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class OrganizationDetailsActivity extends BaseActivity implements CustomAdapterCourses.ItemClickListener {
    CustomAdapterCourses customAdapterCourses;

    List<CoursesModal> courseListStored = new ArrayList<>();
    List<CoursesModal> courseList = new ArrayList<>();

    RecyclerView recyclerView2;
    String orgId, token;
    AppDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_organization_details);

        Intent intent = getIntent();
        orgId = intent.getExtras().getString("OrgID");

        database = Room.databaseBuilder(OrganizationDetailsActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        CoursesDAO coursesDAO = database.getCoursesDAO();
        OrgsDAO orgsDAO = database.getOrgsDAO();

        OrgsModal currentOrg = orgsDAO.getGetOrgsyID(orgId);

        CurrentUserDAO currentUSerDao = database.getCurrentUserDAO();
        List<CurrentUserModal> tempStored = currentUSerDao.getCurrentUser();
        token = "bearer " + tempStored.get(0).getToken();

        TextView tvCourseTitle = findViewById(R.id.textViewCourseTitle);
        tvCourseTitle.setText(currentOrg.getName());

        TextView tvAddress = findViewById(R.id.textViewAddress);
        tvAddress.setText(currentOrg.getAddress());

        TextView tvDescription = findViewById(R.id.textViewDiscription);
        tvDescription.setText(currentOrg.getShortDesc());
        ImageView imageViewBanner = findViewById(R.id.imageViewBanner);
        imageViewBanner.setImageURI(Uri.parse(currentOrg.getLogo_local()));

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.imageViewSidebar).setOnClickListener(v -> showSideBar(OrganizationDetailsActivity.this, tempStored, token));

        courseListStored = coursesDAO.getCoursesAll();
        for (CoursesModal c : courseListStored) {

            if (c.getOrgId().matches(orgId) || currentOrg.getName().matches(orgId)) {
                courseList.add(c);
            }
        }

        recyclerView2 = findViewById(R.id.recyclerviewCourseList);
        recyclerView2.setNestedScrollingEnabled(false);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        customAdapterCourses = new CustomAdapterCourses(this, courseList);
        customAdapterCourses.setClickListener(this);
        recyclerView2.setAdapter(customAdapterCourses);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.textViewJoin) {
            Boolean isFound=false;
            TextView textViewTemp = (TextView) view;
            String textT = "";
            textT = "" + textViewTemp.getText().toString();
            if(textT.matches(getApplicationContext().getResources().getString(R.string.continue2))){
                Toast.makeText(OrganizationDetailsActivity.this,"You Already Joined This Course",Toast.LENGTH_SHORT).show();

            }else {
                Intent intent = new Intent(OrganizationDetailsActivity.this, JoinCourseRequestActivity.class);
                intent.putExtra("courseId", "" + customAdapterCourses.getCourseID(position));
                intent.putExtra("token", "" + token);
                startActivity(intent);
            }
        }
    }
}