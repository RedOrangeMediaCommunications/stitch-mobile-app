package com.redorange.stitch.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.entities.LoginInfoM;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends BaseActivity {
    public String TAG = OtpActivity.class.getSimpleName();
    StitchApi api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_ottp);
        EditText otp = findViewById(R.id.editTextOTP);
        TextView validateButton = findViewById(R.id.textViewValidate);
        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        validateButton.setOnClickListener(v -> {
            Call<LoginInfoM> call = api.getOtp(otp.getText().toString());
            call.enqueue(new Callback<LoginInfoM>() {
                @Override
                public void onResponse(Call<LoginInfoM> call, Response<LoginInfoM> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(OtpActivity.this, "OTP is Validate. Please Login To Confirm SignUp", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(OtpActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } else {
                        Toast.makeText(OtpActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginInfoM> call, Throwable t) {
                    Toast.makeText(OtpActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }
}