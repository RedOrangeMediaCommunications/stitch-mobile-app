package com.redorange.stitch.activities;

import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.entities.CourseJoinRequestM;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class JoinCourseRequestActivity extends BaseActivity {
    public String TAG = JoinCourseRequestActivity.class.getSimpleName();
    public StitchApi api;
    public AppDatabase database;
    CurrentUserDAO currentUSerDao;
    List<CurrentUserModal> tempStored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_join_course_request);

        LinearLayout progressView = findViewById(R.id.progressView);
        Intent intent = getIntent();
        String courseId = intent.getExtras().getString("courseId");
        String token = intent.getExtras().getString("token");

        database = Room.databaseBuilder(JoinCourseRequestActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();

        Log.d(TAG, "onCreate: " + courseId);

        TextView joinButton = findViewById(R.id.send_message);

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.imageViewSidebar).setOnClickListener(v -> showSideBar(JoinCourseRequestActivity.this, tempStored, token));


        joinButton.setOnClickListener(v -> {
            joinButton.setOnClickListener(null);
            progressView.setVisibility(View.VISIBLE);
            api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);

            EditText editTextMessage = findViewById(R.id.editTextMessage);

            Call<CourseJoinRequestM> call = api.postRequestJoin("" + token, "trainee_enrollment", "" + courseId, "{\n" +
                    "\t\t\"message\": \" " + editTextMessage.getText().toString() + "\"\n" +
                    "\t}");
            call.enqueue(new Callback<CourseJoinRequestM>() {
                @Override
                public void onResponse(Call<CourseJoinRequestM> call, Response<CourseJoinRequestM> response) {
                    if (response.isSuccessful()) {
                        progressView.setVisibility(View.GONE);

                        Log.d(TAG, "onCreate: " + "DONE");
                        Toast.makeText(JoinCourseRequestActivity.this, "Request Sent", Toast.LENGTH_LONG).show();
                        Intent intent1 = new Intent(JoinCourseRequestActivity.this, TrainerCourses.class);
                        startActivity(intent1);
                        finish();
                    } else {
                        Log.d(TAG, "onCreate: " + "not Done");
                        progressView.setVisibility(View.GONE);
                        onBackPressed();
                    }
                }

                @Override
                public void onFailure(Call<CourseJoinRequestM> call, Throwable t) {
                    Log.d(TAG, "onCreate: " + "not Done" + t.getMessage());
                    Toast.makeText(JoinCourseRequestActivity.this, "Request Sent", Toast.LENGTH_LONG).show();
                    progressView.setVisibility(View.GONE);
                    onBackPressed();
                }
            });
        });
    }
}