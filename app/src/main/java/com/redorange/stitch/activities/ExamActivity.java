package com.redorange.stitch.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterQuiz;
import com.redorange.stitch.adapters.ScoreAdapter;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.ContentCoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.ScoreDao;
import com.redorange.stitch.entities.Answers;
import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.entities.Questions;
import com.redorange.stitch.entities.SubmissionsM;
import com.redorange.stitch.entities.SubmissionsReply;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.ProgressManagerWithExamModel;
import com.redorange.stitch.modal.ScoreModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class ExamActivity extends BaseActivity implements CustomAdapterQuiz.ItemClickListener, View.OnClickListener {
    public String TAG = ExamActivity.class.getSimpleName();

    CustomAdapterQuiz adapter;
    List<Questions> examsStored = new ArrayList<>();
    RecyclerView recyclerViewQuiz;
    String chapterID;
    AppDatabase database;
    int examID=0;
    String token;
    ContentCoursesDAO contentCoursesDAO;
    ScoreDao scoreDao;
    String courseID;
    TextView textView29;
    ImageView imageViewSidebar;
    CourseContentModel storedCourses;

    LinearLayoutManager linearLayoutManager;
    ScoreAdapter scoreAdapter;
    CurrentUserDAO currentUSerDao;
    List<CurrentUserModal> tempStored;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_exam);

        initViews();
        initListeners();

    }

    public void initViews(){
        database = Room.databaseBuilder(ExamActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        imageViewSidebar = findViewById (R.id.imageViewSidebar);
        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();

        Intent intent = getIntent();
        chapterID = intent.getExtras().getString("CHAPTERID");
        courseID = intent.getExtras().getString("COURSEID");

        textView29 = findViewById (R.id.textView29);

        contentCoursesDAO = database.getContentCourseDAO();
        scoreDao = database.getScoreDao();
        storedCourses = contentCoursesDAO.getGetCourseContentbyID(courseID);
        token = "bearer " + database.getCurrentUserDAO().getCurrentUser().get(0).getToken();
    }

    public void initListeners(){
        imageViewSidebar.setOnClickListener(v->{
            showSideBar(ExamActivity.this, tempStored, token);
        });

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.tvSubmitButton).setOnClickListener(v -> {
            showConfirmDialog();
        });

        if (storedCourses != null) {
            TextView textViewTitle = findViewById(R.id.textViewTitleChapter);
            textViewTitle.setText(storedCourses.getTitle());
            List<ContentChapters> contentChapters = storedCourses.getChapters();
            if (contentChapters != null) {
                for (ContentChapters a : contentChapters) {
                    if (a.getId().matches(chapterID)) {

                        if (null != a.getExams() && a.getExams().size() > 0 && null != a.getExams().get(0).getQuestions()) {
                            if (a.getExams().get(0).getQuestions().size() > 0) {
                                examID=Integer.parseInt(a.getExams().get(0).getId());
                                examsStored.addAll(a.getExams().get(0).getQuestions());
                            }
                        }
                    }
                }
            }
        }

        recyclerViewQuiz = findViewById(R.id.recyclerViewQuiz);
        recyclerViewQuiz.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewQuiz.setNestedScrollingEnabled(false);

        adapter = new CustomAdapterQuiz(this, examsStored);
        adapter.setClickListener(this);
        recyclerViewQuiz.setAdapter(adapter);

//        SubmissionsM submissionsM=database.getSubmissionsDao().getGetSubmissionsbyExamID(""+examID);
//        if(null!=submissionsM){
//            showPreviousResult();
//        }

        textView29.setOnClickListener(this);
    }

    @Override
    public void onItemClick(View view, int position) {}


    private void showResult(String resultScore) {
        SubmissionsM submissionsM=database.getSubmissionsDao().getGetSubmissionsbyExamID(""+examID);
        AlertDialog.Builder dialogBuilder;
        AlertDialog alertDialog;
        dialogBuilder = new AlertDialog.Builder(ExamActivity.this);
        View layoutView = getLayoutInflater().inflate(R.layout.dialog_score_exam, null);

        LinearLayout buttonConfirm = layoutView.findViewById(R.id.buttonConfirm);
        LinearLayout buttonCancel = layoutView.findViewById(R.id.buttonCancel);

        TextView textView18 = layoutView.findViewById(R.id.textView18);
        textView18.setText("Your Score: "+resultScore);

        TextView tCancel = layoutView.findViewById(R.id.tcancel);
        tCancel.setText("Ok");

        dialogBuilder.setView(layoutView);

        Gson gson = new Gson();
        Log.e(TAG,  gson.toJson(submissionsM.getAnswers()));

        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        buttonCancel.setOnClickListener(view -> {
            scoreDao.insert(new ScoreModal(courseID, chapterID, resultScore));
            alertDialog.dismiss();
            onBackPressed();
        });
    }

    private void showConfirmDialog() {
        List<List<String>> fullAnswer = new ArrayList<>();
        for (int positionQuestion = 0; positionQuestion < examsStored.size(); positionQuestion++) {
            View viewHolder = recyclerViewQuiz.getLayoutManager().findViewByPosition(positionQuestion);

            List<String> partialAnswers = new ArrayList<>();
            if (examsStored.get(positionQuestion).getAnswerType().matches("checkbox")) {
                if (examsStored.get(positionQuestion).getAnswers().size() == 1) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 2) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 3) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 4) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 5) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    CheckBox checkBox5 = viewHolder.findViewById(R.id.checkBox5);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                    if (checkBox5.isChecked()) {
                        partialAnswers.add("" + checkBox5.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 6) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    CheckBox checkBox5 = viewHolder.findViewById(R.id.checkBox5);
                    CheckBox checkBox6 = viewHolder.findViewById(R.id.checkBox6);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                    if (checkBox5.isChecked()) {
                        partialAnswers.add("" + checkBox5.getText());
                    }
                    if (checkBox6.isChecked()) {
                        partialAnswers.add("" + checkBox6.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 7) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    CheckBox checkBox5 = viewHolder.findViewById(R.id.checkBox5);
                    CheckBox checkBox6 = viewHolder.findViewById(R.id.checkBox6);
                    CheckBox checkBox7 = viewHolder.findViewById(R.id.checkBox7);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                    if (checkBox5.isChecked()) {
                        partialAnswers.add("" + checkBox5.getText());
                    }
                    if (checkBox6.isChecked()) {
                        partialAnswers.add("" + checkBox6.getText());
                    }
                    if (checkBox7.isChecked()) {
                        partialAnswers.add("" + checkBox7.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 8) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    CheckBox checkBox5 = viewHolder.findViewById(R.id.checkBox5);
                    CheckBox checkBox6 = viewHolder.findViewById(R.id.checkBox6);
                    CheckBox checkBox7 = viewHolder.findViewById(R.id.checkBox7);
                    CheckBox checkBox8 = viewHolder.findViewById(R.id.checkBox8);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                    if (checkBox5.isChecked()) {
                        partialAnswers.add("" + checkBox5.getText());
                    }
                    if (checkBox6.isChecked()) {
                        partialAnswers.add("" + checkBox6.getText());
                    }
                    if (checkBox7.isChecked()) {
                        partialAnswers.add("" + checkBox7.getText());
                    }
                    if (checkBox8.isChecked()) {
                        partialAnswers.add("" + checkBox8.getText());
                    }
                } else if (examsStored.get(positionQuestion).getAnswers().size() == 9) {
                    CheckBox checkBox1 = viewHolder.findViewById(R.id.checkBox);
                    CheckBox checkBox2 = viewHolder.findViewById(R.id.checkBox2);
                    CheckBox checkBox3 = viewHolder.findViewById(R.id.checkBox3);
                    CheckBox checkBox4 = viewHolder.findViewById(R.id.checkBox4);
                    CheckBox checkBox5 = viewHolder.findViewById(R.id.checkBox5);
                    CheckBox checkBox6 = viewHolder.findViewById(R.id.checkBox6);
                    CheckBox checkBox7 = viewHolder.findViewById(R.id.checkBox7);
                    CheckBox checkBox8 = viewHolder.findViewById(R.id.checkBox8);
                    CheckBox checkBox9 = viewHolder.findViewById(R.id.checkBox9);
                    if (checkBox1.isChecked()) {
                        partialAnswers.add("" + checkBox1.getText());
                    }
                    if (checkBox2.isChecked()) {
                        partialAnswers.add("" + checkBox2.getText());
                    }
                    if (checkBox3.isChecked()) {
                        partialAnswers.add("" + checkBox3.getText());
                    }
                    if (checkBox4.isChecked()) {
                        partialAnswers.add("" + checkBox4.getText());
                    }
                    if (checkBox5.isChecked()) {
                        partialAnswers.add("" + checkBox5.getText());
                    }
                    if (checkBox6.isChecked()) {
                        partialAnswers.add("" + checkBox6.getText());
                    }
                    if (checkBox7.isChecked()) {
                        partialAnswers.add("" + checkBox7.getText());
                    }
                    if (checkBox8.isChecked()) {
                        partialAnswers.add("" + checkBox8.getText());
                    }
                    if (checkBox9.isChecked()) {
                        partialAnswers.add("" + checkBox9.getText());
                    }
                }
                if(partialAnswers.size()==0){
                    partialAnswers.add("Not Answered");
                }
                fullAnswer.add(partialAnswers);

            } else {

                RadioGroup radioGroupTemp = viewHolder.findViewById(R.id.radioGroup);

                int genid = radioGroupTemp.getCheckedRadioButtonId();

                RadioButton radioButton = viewHolder.findViewById(genid);
                if (radioButton!=null) {
                    if(null != radioButton.getText())
                    partialAnswers.add(radioButton.getText().toString());


                }else {
                    partialAnswers.add("Not Answered");
                }
                fullAnswer.add(partialAnswers);
            }
        }

        AlertDialog.Builder dialogBuilder;
        AlertDialog alertDialog;
        dialogBuilder = new AlertDialog.Builder(ExamActivity.this);
        View layoutView = getLayoutInflater().inflate(R.layout.dialog_confirm_exam, null);

        LinearLayout buttonConfirm = layoutView.findViewById(R.id.buttonConfirm);
        LinearLayout buttonCancel = layoutView.findViewById(R.id.buttonCancel);
        TextView textViewAnswers = layoutView.findViewById(R.id.textViewAnswers);

        dialogBuilder.setView(layoutView);
        String yourResult="";
        int questionCount=1;

        List<Answers> answers=new ArrayList<>();
        for (List<String> a : fullAnswer) {
            yourResult=yourResult+"Question " + questionCount+" : "+examsStored.get(questionCount-1).getQuestion()+"\n";
            yourResult=yourResult+"Answer  :\n";
            answers.add(new Answers((questionCount-1),a));
            for (String b : a) {

                yourResult=yourResult+"•"+ b+"\n";
            }
            yourResult=yourResult+"\n\n";
            questionCount++;

        }
        SubmissionsM submissionsM=new SubmissionsM(examID,"0",""+chapterID,answers);
        Gson gson = new Gson();

        Log.e(TAG,  gson.toJson(answers));
        textViewAnswers.setText(""+yourResult);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        buttonCancel.setOnClickListener(view -> alertDialog.dismiss());
        buttonConfirm.setOnClickListener(v -> {
            database.getProgressWDao().insert(new ProgressManagerWithExamModel("" + chapterID, "True"));
            StitchApi api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
            database.getSubmissionsDao().insert(submissionsM);
            Call<SubmissionsReply> callSubmissions=api.postSubmissions(token,examID,gson.toJson(submissionsM.getAnswers()));
            callSubmissions.enqueue(new Callback<SubmissionsReply>() {
                @Override
                public void onResponse(Call<SubmissionsReply> call, Response<SubmissionsReply> response) {
                    if(response.isSuccessful()){
                        SubmissionsReply tempSub=response.body();
                        showResult(""+tempSub.getScore());
                    }else{
                        Toast.makeText(getApplicationContext(),"Could Not Submit Answers Saved."+response.message().toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SubmissionsReply> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Could Not Submit Answers Saved."+t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
            alertDialog.dismiss();
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textView29:
                Dialog alertDialog = new Dialog(ExamActivity.this);
                alertDialog.setContentView(R.layout.modal_score_layout);

                RecyclerView scoreRecycler = alertDialog.findViewById(R.id.scoreRecycler);
                AppCompatImageView close = alertDialog.findViewById(R.id.close);

                scoreAdapter = new ScoreAdapter(scoreDao.getScore(courseID, chapterID), getApplicationContext());
                linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                scoreRecycler.setAdapter(scoreAdapter);
                scoreRecycler.setLayoutManager(linearLayoutManager);
                scoreRecycler.setNestedScrollingEnabled(false);

                close.setOnClickListener(viewClose ->{ alertDialog.dismiss(); });
                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                alertDialog.show();
                break;
        }
    }
}