package com.redorange.stitch.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.Button;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.redorange.stitch.R;

import com.redorange.stitch.adapters.CustomAdapterComments;
import com.redorange.stitch.adapters.CustomAdapterContentManager;
import com.redorange.stitch.adapters.CustomAdapterDropdown;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.ContentCoursesDAO;
import com.redorange.stitch.downloadmanager.DownloadAdapter;
import com.redorange.stitch.downloadmanager.DownloadModel;
import com.redorange.stitch.downloadmanager.ItemClickListener;
import com.redorange.stitch.downloadmanager.PathUtil;
import com.redorange.stitch.entities.Comments;
import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.entities.CourseContentM;
import com.redorange.stitch.entities.Exams;
import com.redorange.stitch.entities.Files;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.modal.ProgressManagerNoExamModel;
import com.redorange.stitch.modal.ProgressManagerWithExamModel;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.StitchApplication;
import com.redorange.stitch.videoplayer.UniversalMediaController;
import com.redorange.stitch.videoplayer.UniversalVideoView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.constant.ApplicationConstants.PERMISSION_REQUEST_CODE;
import static com.redorange.stitch.constant.ApplicationConstants.REQUEST_CODE_VIDEO_FULLSCREEN;
import static com.redorange.stitch.constant.ApplicationConstants.SEEK_POSITION_KEY;
import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class ChapterDetailsActivity extends BaseActivity implements
        UniversalVideoView.VideoViewCallback, ItemClickListener, CustomAdapterContentManager.ItemClickListener,
        CustomAdapterDropdown.ItemClickListener, CustomAdapterComments.ItemClickListener {

    private static final String TAG = ChapterDetailsActivity.class.getSimpleName();

    CustomAdapterComments customAdapterComments;
    CustomAdapterDropdown customAdapterDropdown;
    CustomAdapterContentManager customAdapterContentManager;
    DownloadAdapter downloadAdapter;

    List<String> chapterList = new ArrayList<>();
    List<String> chapterIdList = new ArrayList<>();
    List<CurrentUserModal> tempStored;

    Button btnBottomSheet;
    LinearLayout linearBottomButton;
    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;
    LinearLayout buttonTakeQuiz;
    String token, trainerId = "0", instructions = "", chapterID, courseID;

    List<Exams> examsStored = new ArrayList<>();
    List<Comments> commentsStored = new ArrayList<>();
    List<Files> filesStored = new ArrayList<>();
    List<DownloadModel> downloadModels = new ArrayList<>();

    TextView textViewCourseTitle, textViewPost;

    View mBottomLayout;
    View mVideoLayout;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;

    private static String VIDEO_URL = "";
    RecyclerView recyclerViewDropDown;

    TextView textViewTitle;
    private int mSeekPosition, cachedHeight;
    Realm realm;

    ImageView imageViewPdfScale;
    PDFView pdfView;

    StitchApi api;
    AppDatabase database;
    ContentCoursesDAO contentCoursesDAO;
    EditText editTextTextPost;
    RecyclerView recyclerViewComments, recyclerViewDataList, recyclerViewContentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_chapter_details);

        Intent intent = getIntent();
        chapterID = intent.getExtras().getString("CHAPTERID");
        courseID = intent.getExtras().getString("COURSEID");
        trainerId = intent.getExtras().getString("TRAINERID");

        Log.d(TAG, "onCreate: " + chapterID);
        Log.d(TAG, "onCreate: " + courseID);

        database = Room.databaseBuilder(ChapterDetailsActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        contentCoursesDAO = database.getContentCourseDAO();
        tempStored = database.getCurrentUserDAO().getCurrentUser();
        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
        token = "bearer " + tempStored.get(0).getToken();

        editTextTextPost = findViewById(R.id.editTextTextPost);
        editTextTextPost.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if(s.toString().length()>0){
//                    sheetBehavior.setPeekHeight(70);
//                }else {
//                    sheetBehavior.setPeekHeight(70);
//                }

            }
        });
        textViewPost = findViewById(R.id.textViewPost);

        textViewPost.setOnClickListener(v -> {
            if (isNetworkAvailable()){
                if (editTextTextPost.getText().length() > 0) {
                    Call<ResponseBody> callPost = api.postComments("" + token, null,
                            "" + editTextTextPost.getText().toString(),
                            "" + chapterID, "Chapter");
                    callPost.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                editTextTextPost.setText("");
                                updateCourses();
                            } else {
                                Toast.makeText(getApplicationContext(),"Something Went Wrong", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                        }
                    });
                }
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        });

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());

        findViewById(R.id.messageTrainer).setOnClickListener(v -> {
            Intent intentMessage = new Intent(ChapterDetailsActivity.this, MessageActivity.class);
            intentMessage.putExtra("COURSEID", "" + courseID);
            intentMessage.putExtra("CHAPTERID", "" + chapterID);
            intentMessage.putExtra("TRAINERID", "" + trainerId);

            startActivity(intentMessage);
        });

        pdfView = findViewById(R.id.pdfView);

        mVideoLayout = findViewById(R.id.video_layout);
        mBottomLayout = findViewById(R.id.bottom_layout);
        mVideoView =  findViewById(R.id.videoView);
        mMediaController =  findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        setVideoAreaSize();
        mVideoView.setVideoViewCallback(this);
        mVideoView.start();

        setupRealm();

        btnBottomSheet = findViewById(R.id.buttonView);

        linearBottomButton = findViewById(R.id.linearBottomButton);
        layoutBottomSheet = findViewById(R.id.bottom_sheet);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        tempStored = database.getCurrentUserDAO().getCurrentUser();
        token = "bearer " + tempStored.get(0).getToken();

        sheetBehavior.setHideable(false);
        btnBottomSheet.setOnClickListener(v -> toggleBottomSheet());
        linearBottomButton.setOnClickListener(v -> toggleBottomSheet());

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        btnBottomSheet.setText("Close");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        btnBottomSheet.setText("Open");
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(ChapterDetailsActivity.this, tempStored, token));

        recyclerViewComments = findViewById(R.id.recyclerViewComments);
        recyclerViewComments.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        recyclerViewComments.setNestedScrollingEnabled(false);

        customAdapterComments = new CustomAdapterComments(this, commentsStored, tempStored.get(0).getId());
        customAdapterComments.setClickListener(this);
        recyclerViewComments.setAdapter(customAdapterComments);

        recyclerViewContentManager = findViewById(R.id.recyclerViewContent);
        recyclerViewContentManager.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewContentManager.setNestedScrollingEnabled(false);


        customAdapterContentManager = new CustomAdapterContentManager(this, filesStored);
        customAdapterContentManager.setClickListener(this);
        recyclerViewContentManager.setAdapter(customAdapterContentManager);
        customAdapterContentManager.notifyDataSetChanged();

        buttonTakeQuiz = findViewById(R.id.buttonTakeQuiz);
        buttonTakeQuiz.setOnClickListener(v -> {
            if(examsStored.size()==0){
                Toast.makeText(getApplicationContext(),"There are no Exams in this Chapter. Please View All Contents to Complete the Course.",Toast.LENGTH_SHORT).show();
            }else {
                Intent examIntent = new Intent(ChapterDetailsActivity.this, ExamActivity.class);

                examIntent.putExtra("COURSEID", "" + courseID);
                examIntent.putExtra("CHAPTERID", "" + chapterID);
                startActivity(examIntent);
            }

        });

        recyclerViewDropDown = findViewById(R.id.recyclerviewDropdown);
        recyclerViewDropDown.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewDropDown.setNestedScrollingEnabled(false);

        customAdapterDropdown = new CustomAdapterDropdown(this, chapterList, chapterIdList);
        customAdapterDropdown.setClickListener(this);
        recyclerViewDropDown.setAdapter(customAdapterDropdown);

        TextView textViewCourseTitle = findViewById(R.id.textViewCourseTitle);
        ImageView dropdown = findViewById(R.id.imageViewDropdown);
        textViewCourseTitle.setOnClickListener(v -> {
            NestedScrollView dropDownList = findViewById(R.id.dropdownRecycler);
            if (dropDownList.getVisibility() == View.GONE) {
                dropDownList.setAlpha(0);
                dropDownList.setVisibility(View.VISIBLE);
                dropDownList.animate().alpha(1);
                dropdown.animate().rotation(0);

            } else {
                dropdown.animate().rotation(180);
                dropDownList.setVisibility(View.GONE);
            }

        });

        findViewById(R.id.linearInstructions).setOnClickListener(v -> {
            TextView textViewInstructions = findViewById(R.id.textViewInstructions);
            if (textViewInstructions.getVisibility() == View.GONE) {
                textViewInstructions.setAlpha(0);
                textViewInstructions.setVisibility(View.VISIBLE);

                textViewInstructions.setVisibility(View.VISIBLE);
                textViewInstructions.animate().alpha(1);
                findViewById(R.id.dropdownInstructions).animate().rotation(0);
            } else {
                findViewById(R.id.dropdownInstructions).animate().rotation(180);
                textViewInstructions.setVisibility(View.GONE);
            }

        });

        findViewById(R.id.linearComments).setOnClickListener(v -> {

            if (recyclerViewComments.getVisibility() == View.GONE) {
                recyclerViewComments.setAlpha(0);
                findViewById(R.id.postInput).setAlpha(0);

                recyclerViewComments.setVisibility(View.VISIBLE);
                findViewById(R.id.postInput).setVisibility(View.VISIBLE);
                findViewById(R.id.postInput).animate().alpha(1);

                findViewById(R.id.dropDownComments).animate().rotation(0);
                recyclerViewComments.animate().alpha(1);

            } else {
                findViewById(R.id.dropDownComments).animate().rotation(180);
                findViewById(R.id.postInput).setVisibility(View.GONE);
                recyclerViewComments.setVisibility(View.GONE);
            }

        });
        reloadComments(contentCoursesDAO);
    }

    private void updateCourses() {
        Call<List<CourseContentM>> call = api.getCurrentContent(token);
        call.enqueue(new Callback<List<CourseContentM>>() {
            @Override
            public void onResponse(Call<List<CourseContentM>> call, Response<List<CourseContentM>> response) {
                if(response.isSuccessful()){
                    AppDatabase database = Room.databaseBuilder(ChapterDetailsActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                            .allowMainThreadQueries()
                            .build();

                    contentCoursesDAO = database.getContentCourseDAO();
                    List<CourseContentM> tempData = response.body();
                    for (int i =0 ; i <tempData.size(); i++){
                        contentCoursesDAO.updateChapters(tempData.get(i).getId(),tempData.get(i).getChapters());
                    }

//                    reloadComments(contentCoursesDAO);
                    Toast.makeText(getApplicationContext(), StitchApplication.getResourceString(R.string.post_comment), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ChapterDetailsActivity.this, TraineeCoursesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<List<CourseContentM>> call, Throwable t) {}
        });
    }

    private void reloadComments(ContentCoursesDAO contentCoursesDAO) {
        customAdapterComments.notifyDataSetChanged();
        CourseContentModel storedCourses = contentCoursesDAO.getGetCourseContentbyID(courseID);
        if (storedCourses != null) {

            Log.d(TAG, "onCreate: ChapterSize" +storedCourses.getChapters().size());

            textViewCourseTitle = findViewById(R.id.textViewCourseTitle);
            textViewTitle = findViewById(R.id.textViewTitle);
            textViewTitle.setText("Title: " + storedCourses.getTitle());

            List<ContentChapters> contentChapters = storedCourses.getChapters();
            if (contentChapters != null) {
                Collections.sort(contentChapters, (obj1, obj2) -> {
                    // ## Ascending order
                    return obj1.getId().compareToIgnoreCase(obj2.getId()); // To compare string values
                    // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                    // ## Descending order
                    // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                    // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
                });

                for (ContentChapters a : contentChapters) {
                    chapterList.add("" + a.getTitle());
                    chapterIdList.add("" + a.getId());
                    if (a.getId().matches(chapterID)) {
                        TextView textViewInstructions = findViewById(R.id.textViewInstructions);
                        textViewCourseTitle.setText("" + a.getTitle());
                        instructions = a.getInstructions();
                        textViewInstructions.setText(Html.fromHtml(instructions));
                        examsStored.addAll(a.getExams());
                        filesStored.addAll(a.getFiles());
                        commentsStored.addAll(a.getComments());
                    }
                }


                if(examsStored.size()==0 && filesStored.size()==0){
                    database.getProgressWDao().insert(new ProgressManagerWithExamModel("" + chapterID, "True"));
                  }
            }
            refreshDownloadCount();


        }
    }

    @SuppressLint("SetTextI18n")
    private void refreshDownloadCount() {
        int countDownload = getDownloadedCount(filesStored);
        TextView textViewTemp = findViewById(R.id.textViewDownloadCount);
        textViewTemp.setText("Downloaded (" + countDownload + "/" + filesStored.size() + ")");
    }

    private int getDownloadedCount(List<Files> filesStored) {
        int count = 0;
        for (Files file : filesStored) {
            String filename = URLUtil.guessFileName(file.getUrl(), null, null);
            String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/.stitch";

            File fileP = new File(downloadPath, filename);
            if (fileP.exists()) {
                count++;
            }
        }
        return count;
    }

    private void setupRealm() {
        realm = Realm.getDefaultInstance();
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        recyclerViewDataList = findViewById(R.id.recyclerViewContent_download_data_list);

        Realm realm = Realm.getDefaultInstance();

        final RealmResults<DownloadModel> downloadModelRealmResults = realm
                .where(DownloadModel.class)
                .findAll();

        List<DownloadModel> userDatabase = downloadModelRealmResults
                .where()
                .equalTo("status", "Completed")
                .findAll();

        if (userDatabase != null) {
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }
            for (DownloadModel a : userDatabase) {
                a.deleteFromRealm();
            }
            realm.commitTransaction();
        }

        List<DownloadModel> downloadModelsLocal = getAllDownloads();
        if (downloadModelsLocal != null) {
            if (downloadModelsLocal.size() > 0) {
                downloadModels.addAll(downloadModelsLocal);
                for (int i = 0; i < downloadModels.size(); i++) {
                    if (downloadModels.get(i).getStatus().equalsIgnoreCase("Pending") || downloadModels.get(i).getStatus().equalsIgnoreCase("Running") || downloadModels.get(i).getStatus().equalsIgnoreCase("Downloading")) {
                        DownloadStatusTask downloadStatusTask = new DownloadStatusTask(downloadModels.get(i));
                        runTask(downloadStatusTask, "" + downloadModels.get(i).getDownloadId());
                    }
                }
            }
        }
        downloadAdapter = new DownloadAdapter(ChapterDetailsActivity.this, downloadModels, ChapterDetailsActivity.this);
        recyclerViewDataList.setLayoutManager(new LinearLayoutManager(ChapterDetailsActivity.this));
        recyclerViewDataList.setAdapter(downloadAdapter);
    }

    private void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.homeLikeBtn) {
            Call<ResponseBody> call = api.postReaction(token, "like", "" + customAdapterComments.getCommentID(position), "Comment");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: Sucess");
                    } else {
                        Log.d(TAG, "onResponse: Failed " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG, "onResponse: Failed " + t.getMessage());

                }
            });
        }

        if (view.getId() == R.id.homeDisLikeBtn2) {
            Call<ResponseBody> call = api.postReaction(token, "dislike", "" + customAdapterComments.getCommentID(position), "Comment");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: Sucess");
                    } else {
                        Log.d(TAG, "onResponse: Failed " + response.message());

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG, "onResponse: Failed " + t.getMessage());

                }
            });
        }

        if (view.getId() == R.id.recyclerItemHolder) {
            TextView textViewButtonD = view.findViewById(R.id.textViewButtonDL);
            LinearLayout linearButton = view.findViewById(R.id.linearButton);

            if (textViewButtonD.getText().toString().matches("Download")) {
                customAdapterContentManager.getDownloadUri(position);
                downloadFile("" + customAdapterContentManager.getDownloadUri(position), position);
                textViewButtonD.setText("Downloading");
                linearButton.setBackgroundResource(R.drawable.roundbutton5);

            } else if (textViewButtonD.getText().toString().matches("Open")) {
                LinearLayout linearLayout = findViewById (R.id.linearLayout5);
                linearLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.defaultHolder).setVisibility(View.VISIBLE);
                findViewById(R.id.scrollview).scrollTo(0,0);

                if(examsStored.size()==0){
                    ProgressManagerNoExamModel temp = database.getProgresNDao().getByChapterFileID("" + chapterID, "" + customAdapterContentManager.getFileID(position));
                    if (null == temp) {
                        database.getProgresNDao().insert(new ProgressManagerNoExamModel("" + chapterID, "" + customAdapterContentManager.getFileID(position)));

                    }

                    int totalfiles = filesStored.size();
                    int totalViewed = database.getProgresNDao().getByChapter("" + chapterID).size();

                    Log.d("SANIPROGRESS", "" + totalViewed + "/" + totalfiles);
                    if (totalfiles == totalViewed) {
                        ProgressManagerWithExamModel temp2 = database.getProgressWDao().getProgressbyChapterID("" + chapterID);

                        if (null == temp2) {
                            database.getProgressWDao().insert(new ProgressManagerWithExamModel("" + chapterID, "True"));
                        }
                    }
                }

                String filename = URLUtil.guessFileName(customAdapterContentManager.getDownloadUri(position), null, null);
                String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/.stitch";

                File file = new File(downloadPath, filename);

                Log.d(TAG, "" + file.getAbsolutePath());

                hideAllViews();
                if (file.exists()) {
                    if (null != mVideoView && mVideoView.isPlaying()) {
                        mVideoView.pause();
                    }
                    Log.d(TAG, "" + customAdapterContentManager.getFileType(position));
                    if (customAdapterContentManager.getFileType(position).matches("doc") || customAdapterContentManager.getFileType(position).matches("docx")) {
                        findViewById(R.id.defaultHolder).setVisibility(View.VISIBLE);
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);

                            Uri uri = FileProvider.getUriForFile(ChapterDetailsActivity.this, "com.authority", file);
                            intent.setData(uri);

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            try {
                                startActivity(Intent.createChooser(intent, "Open Word document"));
                            } catch (Exception e) {
                                Toast.makeText(ChapterDetailsActivity.this, "Error: No app found to open Word document.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Throwable t) {
                            Toast.makeText(ChapterDetailsActivity.this, "Unable to open" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (customAdapterContentManager.getFileType(position).matches("ppt") || customAdapterContentManager.getFileType(position).matches("pptx")) {

                        findViewById(R.id.defaultHolder).setVisibility(View.VISIBLE);

                        try {

                            Intent intent = new Intent(Intent.ACTION_VIEW);

                            Uri uri = FileProvider.getUriForFile(ChapterDetailsActivity.this, "com.authority", file);
                            intent.setData(uri);

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            try {
                                startActivity(Intent.createChooser(intent, "Open Word document"));
                            } catch (Exception e) {
                                Toast.makeText(ChapterDetailsActivity.this, "Error: No app found to open Powerpoint document.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Throwable t) {
                            Toast.makeText(ChapterDetailsActivity.this, "Unable to open" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else if (customAdapterContentManager.getFileType(position).matches("mp4")) {
                        findViewById(R.id.linear).setVisibility(View.VISIBLE);
                        findViewById(R.id.videoHolder).setVisibility(View.VISIBLE);
                        if (mSeekPosition > 0) {
                            mVideoView.seekTo(mSeekPosition);
                        }
                        VIDEO_URL = file.getAbsolutePath();
                        mMediaController.setTitle("Video Player");
                        mVideoView.stopPlayback();
                        mVideoView.setVideoPath(file.getAbsolutePath());
                        mVideoView.start();


                    } else if (customAdapterContentManager.getFileType(position).matches("mp3")) {
                        VIDEO_URL = "";
                        mVideoView.setVideoPath("");
                        mVideoView.start();
                        mVideoView.stopPlayback();

                        findViewById(R.id.videoHolder).setVisibility(View.VISIBLE);
                        if (mSeekPosition > 0) {
                            mVideoView.seekTo(mSeekPosition);
                        }
                        VIDEO_URL = file.getAbsolutePath();
                        mMediaController.setTitle("Audio Player");
                        mVideoView.stopPlayback();
                        mVideoView.setVideoPath(file.getAbsolutePath());
                        mVideoView.start();


                    } else if (customAdapterContentManager.getFileType(position).matches("pdf")) {


                        findViewById(R.id.pdfHolder).setVisibility(View.VISIBLE);

                        imageViewPdfScale = findViewById(R.id.imageViewPdfScale);
                        imageViewPdfScale.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent pfIntent = new Intent(ChapterDetailsActivity.this, PdfFullscreenActivity.class);
                                pfIntent.putExtra("PDFURL", "" + file.getAbsolutePath());
                                startActivity(pfIntent);
                            }
                        });
                        pdfView.fromFile(file).showMinimap(true).enableSwipe(false).enableDoubletap(true).load();


                    } else if (customAdapterContentManager.getFileType(position).matches("jpg")) {
                        findViewById(R.id.imageHolder).setVisibility(View.VISIBLE);
                        ImageView imageViewHolder = findViewById(R.id.imageViewHolder);
                        ImageView imageViewImageScale = findViewById(R.id.imageViewImageScale);
                        imageViewImageScale.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent pfIntent = new Intent(ChapterDetailsActivity.this, ImageFullscreenActivity.class);
                                pfIntent.putExtra("IMAGEURL", "" + file.getAbsolutePath());
                                startActivity(pfIntent);
                            }
                        });
                        imageViewHolder.setImageURI(Uri.fromFile(file));


                    } else {
                        Toast.makeText(getApplicationContext(), "Can not Open", Toast.LENGTH_LONG).show();
                        findViewById(R.id.defaultHolder).setVisibility(View.VISIBLE);
                    }
                    toggleBottomSheet();
                }
                else {
                    findViewById(R.id.media_controller).setVisibility(View.GONE);
                }
            }

        }

        else if (view.getId() == R.id.textViewCourseTitle2) {
            Intent i = new Intent(ChapterDetailsActivity.this, ChapterDetailsActivity.class);
            i.putExtra("COURSEID", "" + courseID);
            i.putExtra("CHAPTERID", "" + customAdapterDropdown.getChapterID(position));
            VIDEO_URL = "";
            mVideoView.setVideoPath("");
            mVideoView.start();
            mVideoView.stopPlayback();

            startActivity(i);
            finish();


        }

    }


    private void hideAllViews() {
        findViewById(R.id.defaultHolder).setVisibility(View.GONE);
        findViewById(R.id.imageHolder).setVisibility(View.GONE);
        findViewById(R.id.pdfHolder).setVisibility(View.GONE);
    }

    private void downloadFile(String url, int position) {
        String filename = URLUtil.guessFileName(url, null, null);
        String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/.stitch";

        File file = new File(downloadPath, filename);

        DownloadManager.Request request = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            request = new DownloadManager.Request(Uri.parse(url))
                    .setTitle(filename)
                    .setDescription("Downloading")
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                    .setDestinationUri(Uri.fromFile(file))
                    .setRequiresCharging(false)
                    .setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true);
        } else {
            request = new DownloadManager.Request(Uri.parse(url))
                    .setTitle(filename)
                    .setDescription("Downloading")
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                    .setDestinationUri(Uri.fromFile(file))
                    .setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true);
        }

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        long downloadId = downloadManager.enqueue(request);

        Number current = realm.where(DownloadModel.class).max("id");
        int nextId;

        if (current == null) {
            nextId = 1;
        } else {
            nextId = current.intValue() + 1;
        }
        final DownloadModel downloadModel = new DownloadModel();
        downloadModel.setId(nextId);
        downloadModel.setStatus("Downloading");
        downloadModel.setTitle(filename);
        downloadModel.setFile_size("0");
        downloadModel.setProgress("0");
        downloadModel.setIs_paused(false);
        downloadModel.setDownloadId(downloadId);
        downloadModel.setFile_path("");
        downloadModel.setAdapter_position(position);

        downloadModels.add(downloadModel);
        downloadAdapter.notifyItemInserted(downloadModels.size() - 1);

        realm.executeTransaction(realm -> realm.copyToRealm(downloadModel));
        DownloadStatusTask downloadStatusTask = new DownloadStatusTask(downloadModel);
        runTask(downloadStatusTask, "" + downloadId);
    }

    @Override
    public void onCLickItem(String file_path) {
        Log.d("File Path : ", "" + file_path);
        openFile(file_path);
    }

    @Override
    public void onShareClick(DownloadModel downloadModel) { }

    public class DownloadStatusTask extends AsyncTask<String, String, String> {
        DownloadModel downloadModel;

        public DownloadStatusTask(DownloadModel downloadModel) {
            this.downloadModel = downloadModel;
        }

        @Override
        protected String doInBackground(String... strings) {
            downloadFileProcess(strings[0]);
            return null;
        }

        private void downloadFileProcess(String downloadId) {
            DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            boolean downloading = true;
            while (downloading) {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(Long.parseLong(downloadId));
                Cursor cursor = downloadManager.query(query);
                cursor.moveToFirst();

                int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                int total_size = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    downloading = false;
                }
                int progress = (int) ((bytes_downloaded * 100L) / total_size);
                String status = getStatusMessage(cursor);
                publishProgress(new String[]{String.valueOf(progress), String.valueOf(bytes_downloaded), status});
                cursor.close();
            }
        }

        @Override
        protected void onProgressUpdate(final String... values) {
            super.onProgressUpdate(values);
            realm.executeTransaction(realm -> {
                downloadModel.setFile_size(bytesIntoHumanReadable(Long.parseLong(values[1])));
                downloadModel.setProgress(values[0]);
                if (!downloadModel.getStatus().equalsIgnoreCase("PAUSE") && !downloadModel.getStatus().equalsIgnoreCase("RESUME")) {
                    downloadModel.setStatus(values[2]);
                }
                downloadAdapter.changeItem(downloadModel.getDownloadId());
            });
        }
    }

    private String getStatusMessage(Cursor cursor) {
        String msg = "-";
        switch (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Failed";
                break;
            case DownloadManager.STATUS_PAUSED:
                msg = "Paused";
                break;
            case DownloadManager.STATUS_RUNNING:
                msg = "Running";
                break;
            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Completed";
                break;
            case DownloadManager.STATUS_PENDING:
                msg = "Pending";
                break;
            default:
                msg = "Unknown";
                break;
        }
        return msg;
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            boolean comp = downloadAdapter.ChangeItemWithStatus("Completed", id);

            if (comp) {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(id);
                DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Cursor cursor = downloadManager.query(new DownloadManager.Query().setFilterById(id));
                cursor.moveToFirst();

                String downloaded_path = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                downloadAdapter.setChangeItemFilePath(downloaded_path, id);
                int locationPostion = downloadAdapter.getPositionStored(id);
                customAdapterContentManager.setUriLocal(downloaded_path, locationPostion);

                View v = recyclerViewContentManager.getLayoutManager().findViewByPosition(locationPostion);
                TextView textViewTemp = v.findViewById(R.id.textViewButtonDL);
                TextView textViewFilename = v.findViewById(R.id.textViewFilename);
                TextView textViewFileType = v.findViewById(R.id.textViewFileType);
                TextView textViewFileSize = v.findViewById(R.id.textViewFileSize);


                textViewFilename.setTextColor(getResources().getColor(R.color.white));
                textViewFileType.setTextColor(getResources().getColor(R.color.white));
                textViewFileSize.setTextColor(getResources().getColor(R.color.white));

                textViewTemp.setText("Open");

                LinearLayout linearButton = v.findViewById(R.id.linearButton);
                linearButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.roundbutton));

                Realm realm = Realm.getDefaultInstance();

                final RealmResults<DownloadModel> downloadModelRealmResults = realm
                        .where(DownloadModel.class)
                        .findAll();


                List<DownloadModel> userdatabase = downloadModelRealmResults
                        .where()
                        .equalTo("status", "Completed")

                        .findAll();

                if (userdatabase != null) {

                    if (!realm.isInTransaction()) {
                        realm.beginTransaction();
                    }

                    userdatabase.get(0).deleteFromRealm();
                    for (DownloadModel a : userdatabase) {
                        a.deleteFromRealm();
                    }

                    realm.commitTransaction();
                }
                downloadModels.clear();
                downloadModels.addAll(getAllDownloads());
                downloadAdapter.notifyDataSetChanged();

                refreshDownloadCount();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    public void runTask(DownloadStatusTask downloadStatusTask, String id) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                downloadStatusTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{id});
            } else {
                downloadStatusTask.execute(new String[]{id});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private RealmResults<DownloadModel> getAllDownloads() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DownloadModel.class).findAll();
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ChapterDetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(ChapterDetailsActivity.this, "Please Give Permission to Upload File", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(ChapterDetailsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(ChapterDetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ChapterDetailsActivity.this, "Permission Successfull", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChapterDetailsActivity.this, "Permission Failed", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void openFile(String fileurl) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!checkPermission()) {
                requestPermission();
                Toast.makeText(this, "Please Allow Permission to Open File", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        try {
            fileurl = PathUtil.getPath(ChapterDetailsActivity.this, Uri.parse(fileurl));

            File file = new File(fileurl);
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
            String type = mimeTypeMap.getMimeTypeFromExtension(ext);

            if (type == null) {
                type = "*/*";
            }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri contne = FileProvider.getUriForFile(ChapterDetailsActivity.this, "com.furthergrow.android_download_manager", file);
                intent.setDataAndType(contne, type);
            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
            }
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "Unable to Open File", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
        if (mVideoView != null && mVideoView.isPlaying()) {
            mSeekPosition = mVideoView.getCurrentPosition();
            Log.d(TAG, "onPause mSeekPosition=" + mSeekPosition);
            mVideoView.pause();
        }
    }


    private void setVideoAreaSize() {
        mVideoLayout.post((Runnable) () -> {
            int width = mVideoLayout.getWidth();
            cachedHeight = (int) (mVideoLayout.getHeight());
            ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
            videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            videoLayoutParams.height = cachedHeight;
            mVideoLayout.setLayoutParams(videoLayoutParams);
            mVideoView.setVideoPath(VIDEO_URL);
            mVideoView.requestFocus();
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        Log.d(TAG, "onRestoreInstanceState Position=" + mSeekPosition);
    }


    @Override
    public void onScaleChange(boolean isFullscreen) {

        Intent videoIntent = new Intent(ChapterDetailsActivity.this, VideoFullScreenActivity.class);
        videoIntent.putExtra("currenttime", mVideoView.getCurrentPosition());
        videoIntent.putExtra("Url", VIDEO_URL);
        startActivityForResult(videoIntent, REQUEST_CODE_VIDEO_FULLSCREEN);
    }


    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPause UniversalVideoView callback");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
//        Log.d(TAG, "onStart UniversalVideoView callback");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingStart UniversalVideoView callback");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingEnd UniversalVideoView callback");
    }

    @Override
    public void onBackPressed() {
        VIDEO_URL = "";
        mVideoView.setVideoPath("");
        mVideoView.start();
        mVideoView.stopPlayback();
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ChapterDetailsActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_VIDEO_FULLSCREEN) {
            if (data.hasExtra("currenttime")) {
                int result = data.getExtras().getInt("currenttime", 0);
                if (result > 0) {
                    if (null != mVideoView) {
                        mVideoView.start();
                        mVideoView.seekTo(result);
                    }
                }
            }
        }
    }
}