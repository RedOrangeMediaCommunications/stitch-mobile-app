package com.redorange.stitch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CoursesDAO;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.dao.OrgsDAO;
import com.redorange.stitch.entities.TrainerStats;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class TrainerCourses extends BaseActivity {

    List<CurrentUserModal> tempStored;

    String token;
    CurrentUserDAO currentUSerDao;
    Boolean isEnglish=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_trainer_courses);

        AppDatabase database = Room.databaseBuilder(TrainerCourses.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        CoursesDAO coursesDAO = database.getCoursesDAO();
        OrgsDAO orgsDAO = database.getOrgsDAO();

        currentUSerDao = database.getCurrentUserDAO();
        tempStored = currentUSerDao.getCurrentUser();
        token = "bearer " + tempStored.get(0).getToken();

        StitchApi api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);

        CircleImageView circleImageView=findViewById(R.id.imageViewTrainerM);
        if (tempStored.get(0).getPic_local() != null) {
            File imgFile = new File(tempStored.get(0).getPic_local());

            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                circleImageView.setImageBitmap(myBitmap);
            }
        }
        TextView textViewName=findViewById(R.id.textViewTrainerNameM);
        TextView textViewEditProfile=findViewById(R.id.textViewEditProfile);
        textViewEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TrainerCourses.this, TrainerOverviewActivity.class);
                startActivity(intent);

            }
        });
        textViewName.setText(""+tempStored.get(0).getName());


        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(TrainerCourses.this, tempStored, token));


        Call<TrainerStats> callStats=api.getTrainerStats(token,tempStored.get(0).getId());
        callStats.enqueue(new Callback<TrainerStats>() {
            @Override
            public void onResponse(Call<TrainerStats> call, Response<TrainerStats> response) {
                if(response.isSuccessful()){
                    TrainerStats trainerStats=response.body();
                    TextView textViewCourses=findViewById(R.id.textViewCourse);
                    TextView textviewChapters=findViewById(R.id.textviewChapters);
                    TextView textViewExams=findViewById(R.id.textViewExams);
                    TextView textViewSubmissions=findViewById(R.id.textViewSubmissions);

                     textViewCourses.setText(""+trainerStats.getCourses());
                     textviewChapters.setText(""+trainerStats.getChapters());
                     textViewExams.setText(""+trainerStats.getExams());
                     textViewSubmissions.setText(""+trainerStats.getSubmissions());
                }
            }

            @Override
            public void onFailure(Call<TrainerStats> call, Throwable t) {
            }
        });
    }

}