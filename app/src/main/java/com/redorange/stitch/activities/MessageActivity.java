package com.redorange.stitch.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.redorange.stitch.R;
import com.redorange.stitch.adapters.CustomAdapterMessages;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.ContentCoursesDAO;
import com.redorange.stitch.entities.Content;
import com.redorange.stitch.entities.MessageGet;
import com.redorange.stitch.entities.MessagesM;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.entities.UsersInConversation;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.Singleton;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class MessageActivity extends BaseActivity implements CustomAdapterMessages.ItemClickListener {

    public String TAG = MessageActivity.class.getSimpleName();

    CustomAdapterMessages adapter;
    List<MessagesM> messageList = new ArrayList<>();
    List<CurrentUserModal> tempStored;

    String courseID, TRAINERID, token,threadID;
    StitchApi api;
    boolean recordsFound=false;
    RecyclerView recyclerView;
    CircleImageView imageViewTrainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_message);

        Intent intent = getIntent();

        courseID = intent.getExtras().getString("COURSEID");
        TRAINERID = intent.getExtras().getString("TRAINERID");
        threadID = intent.getExtras().getString("threadID");

        Log.d(TAG, "onCreate: " + courseID);
        Log.d(TAG, "Trainer Id: " +TRAINERID);
        Log.d(TAG, "Thread Id: " +threadID);

        AppDatabase database = Room.databaseBuilder(MessageActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        ContentCoursesDAO contentCoursesDAO = database.getContentCourseDAO();

        CourseContentModel storedCourses = contentCoursesDAO.getGetCourseContentbyID(courseID);

        tempStored = database.getCurrentUserDAO().getCurrentUser();

        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);

        token = "bearer " + tempStored.get(0).getToken();


        recyclerView = findViewById(R.id.recyclerViewMessage);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MessageActivity.this);
        mLayoutManager.setReverseLayout(false);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new CustomAdapterMessages(this, messageList,tempStored.get(0).getId());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

//        Trainers trainerTemp = database.getCoursesDAO().getGetCoursesbyID(courseID).getTrainers().get(0);

        imageViewTrainer  = findViewById (R.id.imageViewTrainer);

        ((TextView) findViewById(R.id.textViewRecieversName)).setText("" + Singleton.getInstance().getSenderName());

        Glide.with(getApplicationContext())
                .load(Singleton.getInstance().getSenderImg())
                .placeholder(R.drawable.ic_baseline_check2_circle_24)
                .error(android.R.drawable.stat_notify_error)
                .into(imageViewTrainer);

//        if(null!=trainerTemp.getPic() && trainerTemp.getPic().length()>0){
//            String filename = trainerTemp.getPic().substring(trainerTemp.getPic().lastIndexOf("/") + 1);
//            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);
//
//            if (file.exists()) {
//                ((CircleImageView) findViewById(R.id.imageViewTrainer)).setImageURI(Uri.parse(file.getAbsolutePath()));
//
//            }
//        }


        EditText messageEditText = findViewById(R.id.editTextMessage);
        findViewById(R.id.textViewSendButton).setOnClickListener(v -> {
            Content tempContent= new Content(""+messageEditText.getText().toString());

            Gson gson = new Gson();
            Call<ResponseBody> callPostMessage = api.postMessageWithThread("" + token, "" + TRAINERID, ""+gson.toJson(tempContent),""+threadID);
            callPostMessage.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        loadMessages();
                        messageEditText.setText("");
                    }else { }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });

            recyclerView.scrollToPosition(messageList.size() - 1);

        });
        messageEditText.setOnEditorActionListener((v, actionId, event) -> {
                    // Identifier of the action. This will be either the identifier you supplied,
                    // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                    if (actionId == EditorInfo.IME_ACTION_SEARCH
                            || actionId == EditorInfo.IME_ACTION_DONE
                            || event.getAction() == KeyEvent.ACTION_DOWN
                            && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        if (messageEditText.getText().toString().length() > 0) {

                            Content tempContent= new Content(""+messageEditText.getText().toString());

                            if(recordsFound){
                                Gson gson = new Gson();
                                Call<ResponseBody> callPostMessage = api.postMessageWithThread("" + token, "" + TRAINERID, ""+gson.toJson(tempContent),""+threadID);
                                callPostMessage.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if(response.isSuccessful()){
                                            loadMessages();
                                            messageEditText.setText("");
                                        }else {

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });
                            }else {
                                Gson gson = new Gson();
                                Call<ResponseBody> callPostMessage = api.postMessageWithout("" + token, "" + TRAINERID, ""+gson.toJson(tempContent));
                                callPostMessage.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                            loadMessages();
                                            messageEditText.setText("");

                                        } else {
                                            Log.d(TAG, "onResponse: Failed :" + response.message());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        Log.d(TAG, "onResponse: Failed 2:" + t.getMessage());
                                    }
                                });
                            }
                            recyclerView.scrollToPosition(messageList.size() - 1);
                        }
                        return true;
                    }
                    // Return true if you have consumed the action, else false.
                    return false;
                });


        messageEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                recyclerView.scrollToPosition(messageList.size() - 1);
            }
        });

        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(MessageActivity.this, tempStored, token));

        loadMessages();
//        final Runnable tArea = () -> loadMessages(trainerTemp);
//        ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
//        timer.scheduleAtFixedRate(tArea, 15, 5, TimeUnit.SECONDS);

    }

    private void loadMessages() {
        Call <MessageGet> callMessage=api.getSingleThread(""+token,Integer.parseInt(threadID));
        callMessage.enqueue(new Callback<MessageGet>() {
            @Override
            public void onResponse(Call<MessageGet> call, Response<MessageGet> response) {
                if(response.isSuccessful()){
                    MessageGet tempServer=response.body();
                    messageList.clear();
                    for (MessagesM messages: tempServer.getMessages()){
                        messageList.add(messages);
                    }

                    adapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(messageList.size() - 1);
                }else{
//                    Toast.makeText(MessageActivity.this, "Not Sucessfull 2: "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MessageGet> call, Throwable t) {

//                Toast.makeText(MessageActivity.this, "Not Sucessfull 3: "+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}