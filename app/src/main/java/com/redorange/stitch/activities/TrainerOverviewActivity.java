package com.redorange.stitch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.dao.CurrentUserDAO;
import com.redorange.stitch.modal.CurrentUserModal;
import com.redorange.stitch.typeconverter.ImageFilePath;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.BaseActivity;
import com.redorange.stitch.utils.RetrofitService;
import com.redorange.stitch.utils.StitchApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.redorange.stitch.constant.ApplicationConstants.PICK_PHOTO_FOR_AVATAR;
import static com.redorange.stitch.utils.SidebarUtils.showSideBar;

public class TrainerOverviewActivity extends BaseActivity {
    public String TAG = TrainerOverviewActivity.class.getSimpleName();
    ImageView profilePic;
    StitchApi api;
    List<CurrentUserModal> tempStored;
    TextView textViewUpdate;
    String token;
    boolean pic_changed=false;
    boolean cv_changed=false;
    String imagePath="";
    EditText name, phone, email, address, shortBio;
    Button button2, uploadButton;
    CurrentUserDAO currentUSerDao;
    AppDatabase database;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_trainer_overview);

        name = findViewById(R.id.editTextfullName);
        phone = findViewById(R.id.editTextPhone);
        email = findViewById(R.id.editTextEmail);
        address = findViewById(R.id.editTextAddress);
        shortBio = findViewById(R.id.editTextTextBio1);
        button2=findViewById(R.id.button2);

        profilePic = findViewById(R.id.imageViewProfile);
        textViewUpdate = findViewById(R.id.textViewUpdate);

        database = Room.databaseBuilder(TrainerOverviewActivity.this, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        currentUSerDao = database.getCurrentUserDAO();

        List<CurrentUserModal> currentUserModalList = database.getCurrentUserDAO().getCurrentUser();

        tempStored = database.getCurrentUserDAO().getCurrentUser();

        token = "bearer " + tempStored.get(0).getToken();

        button2.setText(""+  tempStored.get(0).getCv());
        button2.setOnClickListener(v -> {

            Intent pfIntent = new Intent(TrainerOverviewActivity.this, CvFullscreenActivity.class);
            pfIntent.putExtra("PDFURL", "" +  tempStored.get(0).getCv());
            startActivity(pfIntent);
        });
        name.setText("" + currentUserModalList.get(0).getName());
        phone.setText("" + currentUserModalList.get(0).getPhone());
        email.setText("" + currentUserModalList.get(0).getEmail());
        address.setText("" + currentUserModalList.get(0).getAddress());
        shortBio.setText("" + currentUserModalList.get(0).getBio());
        File imgFile = new File(currentUserModalList.get(0).getPic_local());
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            profilePic.setImageBitmap(myBitmap);
        }
        uploadButton = findViewById(R.id.buttonUpload);

        TextView resetPassword = findViewById(R.id.textViewResetPassword);

        resetPassword.setOnClickListener(v -> {
            Intent intent = new Intent(TrainerOverviewActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
        });

        textViewUpdate.setOnClickListener(v -> {
            if(isNetworkAvailable()){
                updateProfile();
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

        uploadButton.setOnClickListener(v -> pickImage());
        LinearLayout buttonProfile = findViewById(R.id.buttonProfile);
        buttonProfile.setOnClickListener(v -> pickImage());
        findViewById(R.id.backButton).setOnClickListener(v -> onBackPressed());

        ImageView sidebarIcon = findViewById(R.id.imageViewSidebar);
        sidebarIcon.setOnClickListener(view -> showSideBar(TrainerOverviewActivity.this, tempStored, token));
    }

    private void updateProfile() {
        api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
        Call<ResponseBody> callTextChange=api.getUpdateUserText(""+token,tempStored.get(0).getId(),
                ""+name.getText().toString(),""+ shortBio.getText().toString(),
                ""+phone.getText().toString(),""+email.getText().toString(),
                ""+address.getText().toString());
        callTextChange.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse: "+response.message());

                    currentUSerDao.updateUser(currentUSerDao.getCurrentUser().get(0).getId(), name.getText().toString(),
                            shortBio.getText().toString(), address.getText().toString());

                    Toast.makeText(getApplicationContext(),StitchApplication.getResourceString(R.string.profile_updated_sucessfully), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(TrainerOverviewActivity.this, TraineeCoursesActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Log.d(TAG, "onResponse: "+response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onResponse:  Failure"+t.getMessage());
            }
        });

        if(pic_changed){
            File fileImageToSend = new File(imagePath);
            if(fileImageToSend.exists()){
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), fileImageToSend);
            }
        }
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                Uri uri = data.getData();
                 imagePath = ImageFilePath.getPath(TrainerOverviewActivity.this, uri);
                Log.i(TAG, "onActivityResult: file path : " + imagePath);

                InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);

                profilePic.setImageBitmap(bmp);
                pic_changed=true;
                currentUSerDao.updateUserAvater(currentUSerDao.getCurrentUser().get(0).getId(), imagePath);
                Toast.makeText(getApplicationContext(),StitchApplication.getResourceString(R.string.profile_updated_sucessfully), Toast.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(TrainerOverviewActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }
}