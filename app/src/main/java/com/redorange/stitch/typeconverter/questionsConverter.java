package com.redorange.stitch.typeconverter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redorange.stitch.entities.Chapters;
import com.redorange.stitch.entities.Questions;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class questionsConverter {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<Questions> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Questions>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<Questions> someObjects) {
        return gson.toJson(someObjects);
    }
}
