package com.redorange.stitch.typeconverter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CoursesModal2;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class coursesConverter2 {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<CoursesModal2> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<CoursesModal2>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<CoursesModal2> someObjects) {
        return gson.toJson(someObjects);
    }
}
