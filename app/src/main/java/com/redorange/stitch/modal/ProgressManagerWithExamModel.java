package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "progress_manager_with")
public class ProgressManagerWithExamModel {

    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String chapterId;

    String isCompleted;

    public ProgressManagerWithExamModel() {
    }

    public ProgressManagerWithExamModel(String chapterId, String isCompleted) {
        this.chapterId = chapterId;
        this.isCompleted = isCompleted;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(String isCompleted) {
        this.isCompleted = isCompleted;
    }
}
