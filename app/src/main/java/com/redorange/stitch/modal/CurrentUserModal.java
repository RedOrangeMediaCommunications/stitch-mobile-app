package com.redorange.stitch.modal;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "current_user")
public class CurrentUserModal {

    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String id;
    String name;
    String cv;
    String pic;
    String pic_local;
    String bio;
    String phone;
    String email;
    String address;
    String token;
    boolean isLoggedIN;
    String Role;

    public CurrentUserModal(String id, String name, String cv, String pic, String pic_local,
                            String bio, String phone, String email, String address, String token,
                            boolean isLoggedIN, String role) {
        this.id = id;
        this.name = name;
        this.cv = cv;
        this.pic = pic;
        this.pic_local = pic_local;
        this.bio = bio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.token = token;
        this.isLoggedIN = isLoggedIN;
        Role = role;
    }

    public CurrentUserModal(String name, String bio, String phone, String email, String address) {
        this.name = name;
        this.bio = bio;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    public CurrentUserModal() {
    }


    public int getPrimaryKey() {
        return primaryKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic_local() {
        return pic_local;
    }

    public void setPic_local(String pic_local) {
        this.pic_local = pic_local;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isLoggedIN() {
        return isLoggedIN;
    }

    public void setLoggedIN(boolean loggedIN) {
        isLoggedIN = loggedIN;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }
}
