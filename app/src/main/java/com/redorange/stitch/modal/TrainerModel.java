package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_trainers")
public class TrainerModel {
    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String courseId, trainerId, name, pic, bio, phone, email, address, ratings;

    public TrainerModel() {
    }

    public TrainerModel(int primaryKey, String courseId, String trainerId, String name,  String pic,
                        String bio, String phone, String email, String address, String ratings) {
        this.primaryKey = primaryKey;
        this.courseId = courseId;
        this.trainerId = trainerId;
        this.name = name;
        this.pic = pic;
        this.bio = bio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.ratings = ratings;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(String trainerId) {
        this.trainerId = trainerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }
}
