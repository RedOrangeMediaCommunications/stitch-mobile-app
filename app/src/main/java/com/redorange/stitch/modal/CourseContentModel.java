package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.typeconverter.contentChapterConverter;

import java.util.List;

@Entity(tableName = "courses_content")
public class CourseContentModel {

    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String id;

    String title;
    String banner;
    String shortDesc;
    String description;
    String objective;
    String rating;
    String orgId;
    String orgName;
    String orgLogo;
//    String evaluation;


    @TypeConverters(contentChapterConverter.class)
    List<ContentChapters> Chapters;


    public CourseContentModel(String id, String title, String banner, String shortDesc,
                              String description, String objective, String rating, String orgId,
                              String orgName, String orgLogo, List<ContentChapters> chapters) {
        this.id = id;
        this.title = title;
        this.banner = banner;
        this.shortDesc = shortDesc;
        this.description = description;
        this.objective = objective;
        this.rating = rating;
        this.orgId = orgId;
        this.orgName = orgName;
        this.orgLogo = orgLogo;
        Chapters = chapters;
    }

    public CourseContentModel() {
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgLogo() {
        return orgLogo;
    }

    public void setOrgLogo(String orgLogo) {
        this.orgLogo = orgLogo;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ContentChapters> getChapters() {
        return Chapters;
    }

    public void setChapters(List<ContentChapters> chapters) {
        Chapters = chapters;
    }


}
