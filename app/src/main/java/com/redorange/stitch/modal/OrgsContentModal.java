package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


public class OrgsContentModal {


    String id;
    String name;
    String logo;

    public OrgsContentModal(String id, String name, String logo) {
        this.id = id;
        this.name = name;
        this.logo = logo;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
