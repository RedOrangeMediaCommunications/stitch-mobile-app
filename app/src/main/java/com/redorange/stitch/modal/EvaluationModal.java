package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_evaluation")
public class EvaluationModal {
    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    public String id;
    public String evaluation;

    public EvaluationModal() {
    }

    public EvaluationModal(String id, String evaluation) {
        this.id = id;
        this.evaluation = evaluation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }
}
