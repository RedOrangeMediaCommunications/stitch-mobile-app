package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.typeconverter.contentChapterConverter;

import java.util.List;

@Entity(tableName = "progress_manager_no_exam")
public class ProgressManagerNoExamModel {

    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String chapterId;

    String fileId;



    public ProgressManagerNoExamModel() {
    }

    public ProgressManagerNoExamModel(String chapterId, String fileId) {
        this.chapterId = chapterId;
        this.fileId = fileId;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
