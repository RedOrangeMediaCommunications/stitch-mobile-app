package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_score")
public class ScoreModal {
    @PrimaryKey(autoGenerate = true)
    public int primaryKey;

    String id;
    String chapterId;
    String score;

    public ScoreModal() {
    }

    public ScoreModal(String id, String chapterId, String score) {
        this.id = id;
        this.score = score;
        this.chapterId = chapterId;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }
}
