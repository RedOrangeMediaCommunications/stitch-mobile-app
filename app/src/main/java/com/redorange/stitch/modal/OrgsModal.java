package com.redorange.stitch.modal;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "orgs")
public class OrgsModal {
    @PrimaryKey(autoGenerate = true)
    public int primaryKey;
    String id;
    String name;
    String logo;
    String logo_local;
    String shortDesc;
    String address;
    String createdAt;
    String updatedAt;
    String deletedAt;


    public OrgsModal(String id, String name, String logo, String logo_local, String shortDesc, String address, String createdAt, String updatedAt, String deletedAt) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.logo_local = logo_local;
        this.shortDesc = shortDesc;
        this.address = address;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public String getLogo_local() {
        return logo_local;
    }

    public void setLogo_local(String logo_local) {
        this.logo_local = logo_local;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
