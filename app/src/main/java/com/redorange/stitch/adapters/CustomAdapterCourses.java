package com.redorange.stitch.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.modal.CourseContentModel;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.CoursesModal2;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CustomAdapterCourses extends RecyclerView.Adapter<CustomAdapterCourses.ViewHolder>  implements Filterable {

    private List<CoursesModal> mData=new ArrayList<>();
    private List<CoursesModal> mDataFull=new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;

    // data is passed into the constructor
    public CustomAdapterCourses(Context context, List<CoursesModal> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mDataFull = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //String animal = mData.get(position);
        holder.textViewTitle.setText(""+mData.get(position).getTitle());
        holder.textViewShortDiscription.setText(""+mData.get(position).getShortDesc());
        String publishedAt=mData.get(position).getCreatedAt();
        try {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date = f.parse(publishedAt);
            String niceDateStr = (String) DateUtils.getRelativeTimeSpanString(date.getTime() , Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS);
            publishedAt=niceDateStr;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("SANICHECKTIME", "onBindViewHolder: "+e.getMessage());

        }

        holder.textViewPublishedOn.setText(""+context.getResources().getString(R.string.published)+" "+publishedAt);

        if(mData.get(position).getBannerLocal()!=null){
            File imgFile = new  File(mData.get(position).getBannerLocal());

            if(imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.banner.setImageBitmap(myBitmap);
            }
        }


        Boolean isFound=false;
        List<CoursesModal2> courseListMy = new ArrayList<>();
        AppDatabase database = Room.databaseBuilder(context, AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        List<CourseContentModel> tempStored =  database.getContentCourseDAO().getCourseContentAll();


        for(CourseContentModel coursesModal:tempStored){
            if(coursesModal.getId().matches(mData.get(position).getId())){
                isFound=true;
            }
        }
        if(isFound){
          holder.textViewJoin.setText(context.getResources().getString(R.string.continue2));
            holder.textViewJoinHolder.setBackgroundColor(context.getResources().getColor(R.color.color2));

        }



    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mData = mDataFull;
                } else {
                    List<CoursesModal> filteredList = new ArrayList<>();
                    for (CoursesModal row : mDataFull) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    mData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mData = (ArrayList<CoursesModal>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;

        TextView textViewPublishedOn;
        TextView textViewShortDiscription;
        TextView textViewJoin;
        LinearLayout linearAllcore;
        LinearLayout textViewJoinHolder;

        ImageView banner;

        ViewHolder(View itemView) {
            super(itemView);
            textViewTitle=itemView.findViewById(R.id.textViewTitle);
            textViewShortDiscription=itemView.findViewById(R.id.textViewShortDiscription);
            textViewPublishedOn=itemView.findViewById(R.id.textViewPublishedOn);
            banner=itemView.findViewById(R.id.imageView6);
            textViewJoin=itemView.findViewById(R.id.textViewJoin);

            linearAllcore=itemView.findViewById(R.id.linearAllcore);
            textViewJoinHolder=itemView.findViewById(R.id.textViewJoinHolder);



            linearAllcore.setOnClickListener(this);
            textViewJoin.setOnClickListener(this);



        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getCourseID(int id) {
        return mData.get(id).getId();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}