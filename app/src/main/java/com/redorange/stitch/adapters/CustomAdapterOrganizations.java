package com.redorange.stitch.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.modal.OrgsModal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CustomAdapterOrganizations extends RecyclerView.Adapter<CustomAdapterOrganizations.ViewHolder> implements Filterable {

    private List<OrgsModal> mData=new ArrayList<>();
    private List<OrgsModal> mDataFull=new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public CustomAdapterOrganizations(Context context, List<OrgsModal> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mDataFull = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.recycler_item_organization, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


         holder.myTextView.setText(mData.get(position).getName());

        if(mData.get(position).getLogo_local()!=null){
            File imgFile = new  File(mData.get(position).getLogo_local());

            if(imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.OrgImage.setImageBitmap(myBitmap);
            }
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public String getOrgId(int position) {

        return mData.get(position).getId();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mData = mDataFull;
                } else {
                    List<OrgsModal> filteredList = new ArrayList<>();
                    for (OrgsModal row : mDataFull) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mData = (ArrayList<OrgsModal>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView OrgImage;
        LinearLayout linearOrg;


        ViewHolder(View itemView) {
            super(itemView);
            myTextView=itemView.findViewById(R.id.textViewName);
            OrgImage=itemView.findViewById(R.id.imageViewOrgImage);
            linearOrg=itemView.findViewById(R.id.linearOrg);
            linearOrg.setOnClickListener(this);
//            myImage1 = itemView.findViewById(R.id.image1);
//
//
//            seeall = itemView.findViewById(R.id.seeall2);
//
//            myImage1.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id).getName();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}