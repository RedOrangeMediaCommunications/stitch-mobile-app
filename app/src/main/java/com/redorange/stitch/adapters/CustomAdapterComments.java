package com.redorange.stitch.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.redorange.stitch.R;
import com.redorange.stitch.entities.Comments;
import com.redorange.stitch.entities.Reactions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.util.TimeZone;

public class CustomAdapterComments extends RecyclerView.Adapter<CustomAdapterComments.ViewHolder> {

    private List<Comments> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    String id;
    private Context context;

    // data is passed into the constructor
    public CustomAdapterComments(Context context, List<Comments> data, String id) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.id = id;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_comments, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewComment.setText(mData.get(position).getContent());
        holder.textViewCommenter.setText(mData.get(position).getUser().getName());

        String publishedAt=mData.get(position).getCreatedAt();


        Log.d("CheckReactions", "onCreate: "+mData.get(position).getReactions().size());

        if(null!=mData.get(position).getReactions() || mData.size()>0){
            Log.d("CheckReactions", "onCreate2: "+mData.get(position).getReactions().size());
            int likeCount=0;
            int dislike=0;

            for (Reactions reaction: mData.get(position).getReactions()){
                String typeReaction=reaction.getType();
                if(typeReaction.matches("like")){
                    if(reaction.getUserId().matches(""+id)){
                        holder.checkBoxLike.setChecked(true);
                    }
                    likeCount++;
                }
                else  if(typeReaction.matches("dislike")){
                    if(reaction.getUserId().matches(""+id)){
                        holder.checkBoxDisLike.setChecked(true);
                    }
                    dislike++;
                }
            }

            holder.textViewDisLikeCount.setText(""+dislike);
            holder.textViewLikeCount.setText(""+likeCount);

        }



            try {
                SimpleDateFormat f = new SimpleDateFormat();
                f.setTimeZone(TimeZone.getTimeZone("GMT+06:00"));
                f.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");


//                DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");


                Date date = f.parse(publishedAt);

                String niceDateStr = (String) DateUtils.getRelativeTimeSpanString(date.getTime() , Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS);
                publishedAt=niceDateStr;
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.d("SANICHECKTIME", "onBindViewHolder: "+e.getMessage());

            }




        holder.textViewTime.setText(publishedAt);


        if(mData.get(position).getUser().getPic()!=null&&mData.get(position).getUser().getPic().length()>0){
            String filename =mData.get(position).getUser().getPic().substring(mData.get(position).getUser().getPic().lastIndexOf("/") + 1);
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);
            if (file.exists()) {
                holder.imageViewCommenter.setImageURI(Uri.parse(file.getAbsolutePath()));
            }
        }


    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewCommenter;
        TextView textViewTime;
        TextView textViewLikeCount;
        TextView textViewDisLikeCount;
        TextView textViewComment;
        MaterialCheckBox checkBoxLike;
        MaterialCheckBox checkBoxDisLike;
        boolean isChanged=false;


        ImageView imageViewCommenter;

        ViewHolder(View itemView) {
            super(itemView);
            textViewComment=itemView.findViewById(R.id.textViewComment);
            textViewCommenter=itemView.findViewById(R.id.textViewCommenter);
            textViewTime=itemView.findViewById(R.id.textViewTime);
            imageViewCommenter=itemView.findViewById(R.id.imageViewCommenter);

            textViewLikeCount=itemView.findViewById(R.id.textViewLikeCount);
            textViewDisLikeCount=itemView.findViewById(R.id.textViewDisLikeCount);
            checkBoxLike=itemView.findViewById(R.id.homeLikeBtn);
            checkBoxDisLike=itemView.findViewById(R.id.homeDisLikeBtn2);

            checkBoxLike.setOnClickListener(this);
            checkBoxDisLike.setOnClickListener(this);
            checkBoxLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if(isChecked){

                        checkBoxDisLike.setChecked(!isChecked);
                        textViewLikeCount.setText(""+(Integer.parseInt(textViewLikeCount.getText().toString())+1));

                    }else{
                        textViewLikeCount.setText(""+(Integer.parseInt(textViewLikeCount.getText().toString())-1));

                    }


                }
            });

            checkBoxDisLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    isChanged=true;

                    if (isChecked){
                        checkBoxLike.setChecked(!isChecked);
                        int temp=(Integer.valueOf(textViewDisLikeCount.getText().toString()))+1;

                        textViewDisLikeCount.setText(""+temp);

                    }else{
                        int temp=(Integer.valueOf(textViewDisLikeCount.getText().toString()))-1;

                        textViewDisLikeCount.setText(""+temp);
                    }
                }
            });





        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getCommentID(int id) {
        return mData.get(id).getId();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}