package com.redorange.stitch.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.StitchApi;
import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.utils.APIClient;
import com.redorange.stitch.utils.RetrofitService;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomAdapterTrainer extends RecyclerView.Adapter<CustomAdapterTrainer.ViewHolder> {

    private List<Trainers> mData;

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;
    String token;
    // data is passed into the constructor
    public CustomAdapterTrainer(Context context, List<Trainers> data,String token) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        this.token = token;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_traineer, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String filename = "";
        if(null!=mData.get(position).getPic()){
             filename = mData.get(position).getPic().substring(mData.get(position).getPic().lastIndexOf("/") + 1);
        }

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);

        if (file.exists() && null!=mData.get(position).getPic()&&mData.get(position).getPic().length()>0) {
            holder.imageViewTrainer.setImageURI(Uri.parse(file.getAbsolutePath()));
        }


        if(null!=mData.get(position).getRating()){

            holder.ratingBar.setRating(Float.parseFloat(mData.get(position).getRating()));
        }

        holder.linear.setVisibility(View.GONE);

        holder.textViewTrainerName.setText(""+mData.get(position).getName());
        holder.ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            StitchApi api = RetrofitService.createService(StitchApi.class, APIClient.BASE_URL, true);
            Call<ResponseBody> call=api.postTrainerRating(""+token,rating,""+mData.get(position).getId(),"User");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        setCurrentRating(rating);


                    }else {
                        Toast.makeText(context, ""+response.message().toString(), Toast.LENGTH_SHORT).show();
                    }
                }


                private void setCurrentRating(float rating) {
                    LayerDrawable drawable = (LayerDrawable)holder.ratingBar.getProgressDrawable();
                    if(context!=null) {

                        setRatingStarColor(drawable.getDrawable(2), ContextCompat.getColor(context, R.color.starcolor));
                        setRatingStarColor(drawable.getDrawable(1), ContextCompat.getColor(context, R.color.starcolor));
                        setRatingStarColor(drawable.getDrawable(0), ContextCompat.getColor(context, R.color.textColor));

                    }
                }
                private void setRatingStarColor(Drawable drawable, @ColorInt int color)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    {
                        DrawableCompat.setTint(drawable, color);
                    }
                    else
                    {
                        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTrainerName;
        TextView textViewTrainerProfile, tvMessage;
        CircleImageView imageViewTrainer;
        RatingBar ratingBar;
        LinearLayout linear;


        ViewHolder(View itemView) {
            super(itemView);


            textViewTrainerName=itemView.findViewById(R.id.textViewTrainerName);
            imageViewTrainer=itemView.findViewById(R.id.imageViewTrainer);
            textViewTrainerProfile=itemView.findViewById(R.id.textViewTrainerProfile);
            tvMessage = itemView.findViewById (R.id.tvMessage);
            ratingBar=itemView.findViewById(R.id.simpleRatingBar);
            linear = itemView.findViewById (R.id.linear);

            textViewTrainerProfile.setOnClickListener(this);
            tvMessage.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getTrainerID(int id) {
        return mData.get(id).getId();
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}