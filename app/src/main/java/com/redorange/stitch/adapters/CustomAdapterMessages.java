package com.redorange.stitch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.entities.MessagesM;

import java.util.List;

public class CustomAdapterMessages extends RecyclerView.Adapter<CustomAdapterMessages.ViewHolder> {

    private List<MessagesM> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    String myId;

    // data is passed into the constructor
    public CustomAdapterMessages(Context context, List<MessagesM> data,String myId) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.myId = myId;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        if (mData.get(viewType).getSenderId().matches(myId)){
            view = mInflater.inflate(R.layout.sending_messages_layout, parent, false);
        }else{
            view = mInflater.inflate(R.layout.receiver_messages_layout, parent, false);

        }

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewComment.setText(""+mData.get(position).getContent().getMessage());
        holder.textView11.setText(""+mData.get(position).getCreatedAt());

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewComment, textView11;

        ViewHolder(View itemView) {
            super(itemView);
            textViewComment=itemView.findViewById(R.id.textViewComment);
            textView11 = itemView.findViewById(R.id.textView11);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getSenderID(int id) {
        return mData.get(id).getSenderId();
    }
    public String getCreatedAt(int id) {
        return mData.get(id).getCreatedAt();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}