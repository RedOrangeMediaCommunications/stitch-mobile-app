package com.redorange.stitch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;

import java.util.List;

public class CustomAdapterDropdown extends RecyclerView.Adapter<CustomAdapterDropdown.ViewHolder> {

    private List<String> mData;
    private List<String> mData_chapterID;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public CustomAdapterDropdown(Context context, List<String> data, List<String> mData_chapterID) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mData_chapterID = mData_chapterID;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_dropdown, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewTitle.setText(""+mData.get(position));

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;


        ViewHolder(View itemView) {
            super(itemView);


            textViewTitle=itemView.findViewById(R.id.textViewCourseTitle2);
            textViewTitle.setOnClickListener(this);



        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getCourseID(int id) {
        return mData.get(id);
    }
    public String getChapterID(int id) {
        return mData_chapterID.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}