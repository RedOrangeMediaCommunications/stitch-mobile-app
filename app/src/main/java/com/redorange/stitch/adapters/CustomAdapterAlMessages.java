package com.redorange.stitch.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.redorange.stitch.R;
import com.redorange.stitch.entities.MessageGet;
import com.redorange.stitch.listeners.MessageThread;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomAdapterAlMessages extends RecyclerView.Adapter<CustomAdapterAlMessages.ViewHolder> {

    private List<MessageGet> mData;
    private Context context;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private MessageThread listener;

    String course_id;

    // data is passed into the constructor
    public CustomAdapterAlMessages(Context context, MessageThread listener, List<MessageGet> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        this.listener = listener;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("ImageURL_Sani", "onBindViewHolder: size "+mData.size());
        View view = mInflater.inflate(R.layout.recycler_item_allmessages_user, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //  String animal = mData.get(position);

        if(null!=mData && mData.size()>0){

            holder.buttonHolder.setOnClickListener(v->{
                listener.onClick(mData.get(position).getId(), position, mData.get(position).getUsers().get(1).getName(), mData.get(position).getUsers().get(1).getPic());
            });
            holder.textViewUserName.setText(mData.get(position).getName());


            String filename = "";
            if(null!=mData.get(position).getUsers().get(1).getPic()){
                filename = mData.get(position).getUsers().get(1).getPic().substring(mData.get(position).getUsers().get(1).getPic().lastIndexOf("/") + 1);
            }

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);

//            if (file.exists() && null!=mData.get(position).getUsers().get(1).getPic()&&mData.get(position).getUsers().get(1).getPic().length()>0) {
//              holder.imageViewTrainer.setImageDrawable();
//            }
            Glide.with(context)
                    .load(mData.get(position).getUsers().get(1).getPic())
                    .placeholder(R.drawable.ic_baseline_check2_circle_24)
                    .error(android.R.drawable.stat_notify_error)
                    .into(holder.imageViewTrainer);





            if(null!=mData.get(position).getMessages() && mData.get(position).getMessages().size()>0){
                String publishedAt=""+mData.get(position).getMessages().get(mData.get(position).getMessages().size()-1).getCreatedAt();
                try {
                    DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    Date date = f.parse(publishedAt);
                    String niceDateStr = (String) DateUtils.getRelativeTimeSpanString(date.getTime() , Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS);
                    publishedAt=niceDateStr;
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("SANICHECKTIME", "onBindViewHolder: "+e.getMessage());

                }
                holder.textViewUserDate.setText(publishedAt);
            }


        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public String getTrainerid(int position) {
        return mData.get(position).getUsers().get(1).getId();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewUserName;
        CircleImageView imageViewTrainer;
        TextView textViewUserDate;
        LinearLayout buttonHolder;



        ViewHolder(View itemView) {
            super(itemView);

            buttonHolder=itemView.findViewById(R.id.buttonHolder);
            textViewUserDate=itemView.findViewById(R.id.textViewDate);
            textViewUserName=itemView.findViewById(R.id.textViewUserName);
            imageViewTrainer=itemView.findViewById(R.id.imageViewTrainer);
//            buttonHolder.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}