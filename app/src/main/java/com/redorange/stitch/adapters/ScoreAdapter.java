package com.redorange.stitch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.modal.ScoreModal;

import java.util.List;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {
    public List<ScoreModal> scoreModals;
    public Context context;

    public ScoreAdapter(List<ScoreModal> scoreModals, Context context){
        this.scoreModals = scoreModals;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.modal_score_single_layout,parent,false);
        ScoreAdapter.ViewHolder viewHolder=new ScoreAdapter.ViewHolder(view);
        context = parent.getContext();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.scoreTv.setText("Score "+position+":");
        holder.scoreValue.setText(scoreModals.get(position).getScore());
    }

    @Override
    public int getItemCount() {
        return scoreModals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView scoreTv, scoreValue;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            scoreTv = itemView.findViewById(R.id.scoreTv);
            scoreValue = itemView.findViewById(R.id.scoreValue);
        }
    }
}
