package com.redorange.stitch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.entities.Chapters;
import com.redorange.stitch.entities.ContentChapters;

import java.util.List;

public class CustomAdapterChapters extends RecyclerView.Adapter<CustomAdapterChapters.ViewHolder> {

    private List<ContentChapters> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;
    String courseId;
    // data is passed into the constructor
    public CustomAdapterChapters(Context context, List<ContentChapters> data,String courseId) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        this.courseId = courseId;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_chapters, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewTitle.setText(""+mData.get(position).getTitle());
        AppDatabase database = Room.databaseBuilder(context, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

            if(null!=database.getProgressWDao().getProgressbyChapterID(mData.get(position).getId())){
                holder.doneImageView.setAlpha(1F);
            }

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;
        ConstraintLayout buttonChapter;
        ImageView doneImageView;
        LinearLayout buttonTakeQuiz;



        ViewHolder(View itemView) {
            super(itemView);

            textViewTitle=itemView.findViewById(R.id.textViewCourseTitle2);
            buttonChapter=itemView.findViewById(R.id.buttonChapter);
            doneImageView=itemView.findViewById(R.id.imageViewDone);
            buttonTakeQuiz=itemView.findViewById(R.id.buttonTakeQuiz);

            buttonChapter.setOnClickListener(this);
            buttonTakeQuiz.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getChapterID(int id) {
        return mData.get(id).getId();
    }
    public String getCourserID(int id) {
        return courseId;
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}