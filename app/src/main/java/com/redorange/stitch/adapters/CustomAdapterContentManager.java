package com.redorange.stitch.adapters;

import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.entities.Files;

import java.io.File;
import java.util.List;

public class CustomAdapterContentManager extends RecyclerView.Adapter<CustomAdapterContentManager.ViewHolder> {

    private List<Files> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    Context context;

    // data is passed into the constructor
    public CustomAdapterContentManager(Context context, List<Files> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_content_manager, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewFilename.setText(mData.get(position).getTitle());
        holder.textViewFileType.setText(mData.get(position).getExtension());
        holder.textViewFileSize.setText(mData.get(position).getDescription());


        String filename= URLUtil.guessFileName(mData.get(position).getUrl(),null,null);
        String downloadPath= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/.stitch";

        File file=new File(downloadPath,filename);
        if(file.exists()){

            mData.get(position).setUrl_local(file.getAbsolutePath() );

            holder.textViewFilename.setTextColor(context.getResources().getColor(R.color.white));
            holder.textViewFileType.setTextColor(context.getResources().getColor(R.color.white));
            holder.textViewFileSize.setTextColor(context.getResources().getColor(R.color.white));

            holder.textViewButtonDL.setText("Open");

            holder.linearLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.roundbutton));
        }

        if(mData.get(position).getExtension().matches("pdf")){
            holder.imageViewFileType.setImageResource(R.drawable.pdf);
        }else if(mData.get(position).getExtension().matches("jpg")){
            holder.imageViewFileType.setImageResource(R.drawable.noimage);
        }else if(mData.get(position).getExtension().matches("mp4")){
            holder.imageViewFileType.setImageResource(R.drawable.mp4);
        }else if(mData.get(position).getExtension().matches("mp3")){
            holder.imageViewFileType.setImageResource(R.drawable.mp3);
        }else if(mData.get(position).getExtension().matches("pptx")){
            holder.imageViewFileType.setImageResource(R.drawable.ppt);
        }else if(mData.get(position).getExtension().matches("doc")){
            holder.imageViewFileType.setImageResource(R.drawable.doc);
        }else if(mData.get(position).getExtension().matches("docx")){
            holder.imageViewFileType.setImageResource(R.drawable.doc);
        }




    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setUriLocal(String downloaded_path,int position) {
        mData.get(position).setUrl_local(downloaded_path);
    }



    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewButtonDL;
        TextView textViewFilename;
        TextView textViewFileType;
        TextView textViewFileSize;
        ImageView imageViewFileType;
        LinearLayout linearLayout;
        LinearLayout recyclerItemHolder;

        ViewHolder(View itemView) {
            super(itemView);
            textViewButtonDL=itemView.findViewById(R.id.textViewButtonDL);
            textViewFilename=itemView.findViewById(R.id.textViewFilename);
            textViewFileType=itemView.findViewById(R.id.textViewFileType);
            textViewFileSize=itemView.findViewById(R.id.textViewFileSize);
            imageViewFileType=itemView.findViewById(R.id.imageViewFileType);

            linearLayout=itemView.findViewById(R.id.linearButton);
            recyclerItemHolder=itemView.findViewById(R.id.recyclerItemHolder);
            recyclerItemHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getFileID(int id) {
        return mData.get(id).getId();
    }
    public String getDownloadUri(int id) {
        return mData.get(id).getUrl();
    }
    public String getFileType(int id) {
        return mData.get(id).getExtension();
    }
    public String getDownloadUriLocal(int id) {
        return mData.get(id).getUrl_local();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}