package com.redorange.stitch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.redorange.stitch.R;
import com.redorange.stitch.entities.Questions;

import java.util.List;

public class CustomAdapterQuiz extends RecyclerView.Adapter<CustomAdapterQuiz.ViewHolder> {

    private List<Questions> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    Context context;

    // data is passed into the constructor
    public CustomAdapterQuiz(Context context, List<Questions> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_quiz, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int place=position+1;
        holder.textViewQuestion.setText(place+". "+mData.get(position).getQuestion());

        if(mData.get(position).getAnswerType().matches("radio")){
            if ( mData.get(position).getAnswers() != null){
                if(mData.get(position).getAnswers().size()==0){
                    holder.radioButton1.setVisibility(View.GONE);
                    holder.radioButton2.setVisibility(View.GONE);
                    holder.radioButton3.setVisibility(View.GONE);
                    holder.radioButton4.setVisibility(View.GONE);
                    holder.radioButton5.setVisibility(View.GONE);
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==1){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setVisibility(View.GONE);
                    holder.radioButton3.setVisibility(View.GONE);
                    holder.radioButton4.setVisibility(View.GONE);
                    holder.radioButton5.setVisibility(View.GONE);
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==2){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setVisibility(View.GONE);
                    holder.radioButton4.setVisibility(View.GONE);
                    holder.radioButton5.setVisibility(View.GONE);
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==3){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setVisibility(View.GONE);
                    holder.radioButton5.setVisibility(View.GONE);
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);
                }
                else if (mData.get(position).getAnswers().size()==4){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setVisibility(View.GONE);
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==5){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.radioButton6.setVisibility(View.GONE);
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==6){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.radioButton6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.radioButton7.setVisibility(View.GONE);
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==7){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.radioButton6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.radioButton7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.radioButton8.setVisibility(View.GONE);
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==8){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.radioButton6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.radioButton7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.radioButton8.setText(""+mData.get(position).getAnswers().get(7));
                    holder.radioButton9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()>=9){
                    holder.radioButton1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.radioButton2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.radioButton3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.radioButton4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.radioButton5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.radioButton6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.radioButton7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.radioButton8.setText(""+mData.get(position).getAnswers().get(7));
                    holder.radioButton9.setText(""+mData.get(position).getAnswers().get(8));

                }
                holder.checkBoxGroup.setVisibility(View.GONE);
            } else {
                holder.checkBoxGroup.setVisibility(View.GONE);
                holder.radioGroup.setVisibility(View.GONE);
            }
        }
        else {
            if ( mData.get(position).getAnswers() != null){
                holder.checkBoxGroup.setVisibility(View.VISIBLE);
                if(mData.get(position).getAnswers().size()==0){
                    holder.checkBox1.setVisibility(View.GONE);
                    holder.checkBox2.setVisibility(View.GONE);
                    holder.checkBox3.setVisibility(View.GONE);
                    holder.checkBox4.setVisibility(View.GONE);
                    holder.checkBox5.setVisibility(View.GONE);
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==1){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setVisibility(View.GONE);
                    holder.checkBox3.setVisibility(View.GONE);
                    holder.checkBox4.setVisibility(View.GONE);
                    holder.checkBox5.setVisibility(View.GONE);
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);
                }
                else if (mData.get(position).getAnswers().size()==2){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setVisibility(View.GONE);
                    holder.checkBox4.setVisibility(View.GONE);
                    holder.checkBox5.setVisibility(View.GONE);
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);
                }
                else if (mData.get(position).getAnswers().size()==3){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setVisibility(View.GONE);
                    holder.checkBox5.setVisibility(View.GONE);
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);
                }
                else if (mData.get(position).getAnswers().size()==4){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setVisibility(View.GONE);
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==5){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.checkBox6.setVisibility(View.GONE);
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==6){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.checkBox6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.checkBox7.setVisibility(View.GONE);
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()==7){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.checkBox6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.checkBox7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.checkBox8.setVisibility(View.GONE);
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()>=8){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.checkBox6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.checkBox7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.checkBox8.setText(""+mData.get(position).getAnswers().get(7));
                    holder.checkBox9.setVisibility(View.GONE);

                }
                else if (mData.get(position).getAnswers().size()>=9){
                    holder.checkBox1.setText(""+mData.get(position).getAnswers().get(0));
                    holder.checkBox2.setText(""+mData.get(position).getAnswers().get(1));
                    holder.checkBox3.setText(""+mData.get(position).getAnswers().get(2));
                    holder.checkBox4.setText(""+mData.get(position).getAnswers().get(3));
                    holder.checkBox5.setText(""+mData.get(position).getAnswers().get(4));
                    holder.checkBox6.setText(""+mData.get(position).getAnswers().get(5));
                    holder.checkBox7.setText(""+mData.get(position).getAnswers().get(6));
                    holder.checkBox8.setText(""+mData.get(position).getAnswers().get(7));
                    holder.checkBox9.setText(""+mData.get(position).getAnswers().get(8));

                }
                holder.radioGroup.setVisibility(View.GONE);
            }
            else {
                holder.checkBoxGroup.setVisibility(View.GONE);
                holder.radioGroup.setVisibility(View.GONE);
            }

        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewQuestion;
        LinearLayout checkBoxGroup;
        RadioGroup radioGroup;
        RadioButton radioButton1;
        RadioButton radioButton2;
        RadioButton radioButton3;
        RadioButton radioButton4;
        RadioButton radioButton5;
        RadioButton radioButton6;
        RadioButton radioButton7;
        RadioButton radioButton8;
        RadioButton radioButton9;
        CheckBox checkBox1;
        CheckBox checkBox2;
        CheckBox checkBox3;
        CheckBox checkBox4;
        CheckBox checkBox5;
        CheckBox checkBox6;
        CheckBox checkBox7;
        CheckBox checkBox8;
        CheckBox checkBox9;

        ViewHolder(View itemView) {
            super(itemView);
            textViewQuestion=itemView.findViewById(R.id.textViewQuestion);
            checkBoxGroup=itemView.findViewById(R.id.checkBoxGroup);
            radioGroup=itemView.findViewById(R.id.radioGroup);
            radioButton1=itemView.findViewById(R.id.radioButton);
            radioButton2=itemView.findViewById(R.id.radioButton2);
            radioButton3=itemView.findViewById(R.id.radioButton3);
            radioButton4=itemView.findViewById(R.id.radioButton4);
            radioButton5=itemView.findViewById(R.id.radioButton5);
            radioButton6=itemView.findViewById(R.id.radioButton6);
            radioButton7=itemView.findViewById(R.id.radioButton7);
            radioButton8=itemView.findViewById(R.id.radioButton8);
            radioButton9=itemView.findViewById(R.id.radioButton9);
            checkBox1=itemView.findViewById(R.id.checkBox);
            checkBox2=itemView.findViewById(R.id.checkBox2);
            checkBox3=itemView.findViewById(R.id.checkBox3);
            checkBox4=itemView.findViewById(R.id.checkBox4);
            checkBox5=itemView.findViewById(R.id.checkBox5);
            checkBox6=itemView.findViewById(R.id.checkBox6);
            checkBox7=itemView.findViewById(R.id.checkBox7);
            checkBox8=itemView.findViewById(R.id.checkBox8);
            checkBox9=itemView.findViewById(R.id.checkBox9);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getQuestionA(int id) {
        return mData.get(id).getQuestion();
    }



    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}