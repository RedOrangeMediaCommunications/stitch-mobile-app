package com.redorange.stitch.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.redorange.stitch.R;
import com.redorange.stitch.apis.AppDatabase;
import com.redorange.stitch.entities.SentByUser;
import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.OrgsModal;

import java.util.List;

public class CustomAdapterRequests extends RecyclerView.Adapter<CustomAdapterRequests.ViewHolder> {

    private List<SentByUser> mData;
    private Context context;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public CustomAdapterRequests(Context context, List<SentByUser> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("ImageURL_Sani", "onBindViewHolder: size "+mData.size());
        View view = mInflater.inflate(R.layout.recycler_item_requests, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      //  String animal = mData.get(position);
        Log.d("ImageURL_Sani", "onBindViewHolder: "+mData.get(position).getId());
         holder.textViewCourse.setText(mData.get(position).getCourseId());

         holder.textViewInvite.setText("'"+mData.get(position).getContent().getMessage()+"'");
         //needsChange
         holder.textViewStatus.setText(mData.get(position).getStatus());

        AppDatabase database = Room.databaseBuilder(context, AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        OrgsModal temp=database.getOrgsDAO().getGetOrgsyID(mData.get(position).getOrgId());
        CoursesModal tempCourse=database.getCoursesDAO().getGetCoursesbyID(mData.get(position).getCourseId());





        if(tempCourse!=null){
            holder.textViewCourse.setText("" + tempCourse.getTitle());

//            String filename = temp.getLogo_local().substring(temp.getLogo_local().lastIndexOf("/") + 1);
//            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);
//            if (file.exists()) {
//                holder.imageViewOrgsLogo.setImageURI(Uri.parse(temp.getLogo_local()));
//
//            }
        }



    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewCourse;

        TextView textViewInvite;
        TextView textViewStatus;
        ImageView imageViewCancel;



        ViewHolder(View itemView) {
            super(itemView);

             textViewCourse=itemView.findViewById(R.id.textViewCourse);

             textViewInvite=itemView.findViewById(R.id.textViewInvitedBy);
             textViewStatus=itemView.findViewById(R.id.textViewStatus);
            imageViewCancel=itemView.findViewById(R.id.imageViewCancel);
            imageViewCancel.setOnClickListener(this);



        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id).getId();
    }

    public String getRequestId(int id) {
        return mData.get(id).getId();
    }
    public String getStatus(int id) {
        return mData.get(id).getStatus();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}