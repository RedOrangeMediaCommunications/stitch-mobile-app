package com.redorange.stitch.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.redorange.stitch.R;

import com.redorange.stitch.apis.AppDatabase;

import com.redorange.stitch.constant.DatabaseConstants;
import com.redorange.stitch.entities.ContentChapters;
import com.redorange.stitch.modal.CourseContentModel;


import java.io.File;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterMyCourses extends RecyclerView.Adapter<CustomAdapterMyCourses.ViewHolder> implements Filterable {

    private List<CourseContentModel> mData=new ArrayList<>();
    private List<CourseContentModel> mDataFull=new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public CustomAdapterMyCourses(Context context, List<CourseContentModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        this.mDataFull = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_my_courses, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //String animal = mData.get(position);
        AppDatabase database = Room.databaseBuilder(context, AppDatabase.class, DatabaseConstants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        int TotalChapters=0;
        int TotalCompleted=0;

        if(null!=mData.get(position).getChapters()){
            TotalChapters=mData.get(position).getChapters().size();
            for(ContentChapters chapters:mData.get(position).getChapters()){
                if(null!=database.getProgressWDao().getProgressbyChapterID(chapters.getId())){
                    TotalCompleted++;
                }
            }
        }

        holder.textViewTitle.setText("" + mData.get(position).getTitle());
        holder.textViewShortDiscription.setText("" + mData.get(position).getShortDesc());
//        holder.textViewPublishedOn.setText("Published on  " + mData.get(position).getCreatedAt());

        holder.fab.setImageResource(R.mipmap.ic_play);
        holder.textViewButton.setText(context.getResources().getString(R.string.continue2));
        holder.textViewChapterTally.setText(""+TotalCompleted+"/"+TotalChapters);
        holder.progressBar.setMax(TotalChapters);
        holder.progressBar.setProgress(TotalCompleted);
        int percentage = 0 ;
        if (TotalChapters!=0)
        percentage=(int) ((Double.parseDouble(""+TotalCompleted)/Double.parseDouble(""+TotalChapters))*100.00f);
        holder.textViewProgressWord.setText(""+percentage+context.getResources().getString(R.string.per_completed));

        if("100".matches(""+percentage)){
            holder.textViewButton.setText(""+context.getResources().getString(R.string.evaluation));
            holder.fab.setImageResource(R.mipmap.ic_completed);
        }

        if (mData.get(position).getOrgLogo() != null) {
            String filename = mData.get(position).getOrgLogo().substring(mData.get(position).getOrgLogo().lastIndexOf("/") + 1);
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);
            if (file.exists()) {
                holder.banner.setImageURI(Uri.parse(file.getAbsolutePath()));

            }

            holder.textviewOrgsName.setText("" + mData.get(position).getOrgName());

        }




        if (mData.get(position).getBanner() != null) {
            String filename = mData.get(position).getBanner().substring(mData.get(position).getBanner().lastIndexOf("/") + 1);

            File imgFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/.Stitch/" + filename);

            if (imgFile.exists()) {
                holder.banner.setImageURI(Uri.parse(imgFile.getAbsolutePath()));

            }
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mData = mDataFull;
                } else {
                    List<CourseContentModel> filteredList = new ArrayList<>();
                    for (CourseContentModel row : mDataFull) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getChapters().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    mData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mData = (ArrayList<CourseContentModel>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    public int getPositionbyCourseID(String courseID) {
        int counter=0;
        int position=0;
        for (CourseContentModel c :mData){
            if(c.getId().matches(""+courseID)){
                position=counter;
            }
            counter++;
        }
        return position;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;
        TextView textviewOrgsName;
        TextView textViewPublishedOn;
        TextView textViewShortDiscription;
        TextView textViewButton;
        TextView textViewButton2;
        TextView textViewProgressWord;
        TextView textViewChapterTally;
        ImageView fab;
        ImageView imageViewOrgsLogo;
        ProgressBar progressBar;
        ImageView banner;
        LinearLayout linearLayoutItemView;

        ViewHolder(View itemView) {
            super(itemView);
            linearLayoutItemView = itemView.findViewById(R.id.linearLayoutItemView);
            textviewOrgsName = itemView.findViewById(R.id.textviewOrgsName);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewChapterTally = itemView.findViewById(R.id.textViewChapterTally);
            textViewShortDiscription = itemView.findViewById(R.id.textViewShortDiscription);
            textViewPublishedOn = itemView.findViewById(R.id.textViewPublishedOn);
            banner = itemView.findViewById(R.id.imageViewMyCourseBanner);
            fab = itemView.findViewById(R.id.imageViewFab);
            imageViewOrgsLogo = itemView.findViewById(R.id.imageViewOrgsLogo);
            textViewButton = itemView.findViewById(R.id.textViewButton);
            progressBar = itemView.findViewById(R.id.progressBar);
            textViewButton2 = itemView.findViewById(R.id.textViewButtonDetails);
            textViewProgressWord = itemView.findViewById(R.id.textViewProgressWord);
            textViewButton2.setOnClickListener(this);
            textViewButton.setOnClickListener(this);
            linearLayoutItemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getCourseID(int id) {
        return mData.get(id).getId();
    }


    public List<ContentChapters> getChapters(int id) {
        return mData.get(id).getChapters();
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}