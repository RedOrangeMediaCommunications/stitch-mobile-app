package com.redorange.stitch.entities;

import androidx.room.TypeConverters;

import com.redorange.stitch.typeconverter.examsConverter;
import com.redorange.stitch.typeconverter.questionsConverter;

import java.io.Serializable;
import java.util.List;

public class Exams implements Serializable {
    String id;
    String courseId;
    String chapterId;
    String title;
    @TypeConverters(questionsConverter.class)
    List<Questions> questions;

    public Exams(String id, String courseId, String chapterId, String title, List<Questions> questions) {
        this.id = id;
        this.courseId = courseId;
        this.chapterId = chapterId;
        this.title = title;
        this.questions = questions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }
}
