package com.redorange.stitch.entities;

import java.util.List;

public class RequestSentByTraineeM {
    List<SentByUser> sentByUser;

    public RequestSentByTraineeM(List<SentByUser> sentToUser) {
        this.sentByUser = sentToUser;
    }

    public List<SentByUser> getSentToUser() {
        return sentByUser;
    }

    public void setSentToUser(List<SentByUser> sentToUser) {
        this.sentByUser = sentToUser;
    }
}
