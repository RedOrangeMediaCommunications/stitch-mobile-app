package com.redorange.stitch.entities;

public class SubmissionsReply {
    String score;

    public SubmissionsReply() {
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
