package com.redorange.stitch.entities;

public class Reactions {

    String id;
    String userId;
    String type;
    String entityId;
    String entityType;

    public Reactions(String id, String userId, String type, String entityId, String entityType) {
        this.id = id;
        this.userId = userId;
        this.type = type;
        this.entityId = entityId;
        this.entityType = entityType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }
}
