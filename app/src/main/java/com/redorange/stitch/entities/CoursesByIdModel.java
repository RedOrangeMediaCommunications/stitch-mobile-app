package com.redorange.stitch.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoursesByIdModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("courseId")
    @Expose
    private Integer courseId;
    @SerializedName("chapterId")
    @Expose
    private Object chapterId;
    @SerializedName("Exams")
    @Expose
    private List<ExamByIdModel> exams = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Object getChapterId() {
        return chapterId;
    }

    public void setChapterId(Object chapterId) {
        this.chapterId = chapterId;
    }

    public List<ExamByIdModel> getExams() {
        return exams;
    }

    public void setExams(List<ExamByIdModel> exams) {
        this.exams = exams;
    }

}


