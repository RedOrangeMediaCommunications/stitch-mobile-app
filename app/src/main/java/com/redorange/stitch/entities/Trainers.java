package com.redorange.stitch.entities;

public class Trainers {
    String id;
    String name;
    String password;
    String cv;
    String pic;
    String bio;
    String phone;
    String email;
    String address;
    String rating;

    public Trainers(String id, String name, String password, String cv, String pic, String bio, String phone, String email, String address, String rating) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.cv = cv;
        this.pic = pic;
        this.bio = bio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
