package com.redorange.stitch.entities;

import androidx.room.TypeConverters;

import com.redorange.stitch.modal.CoursesModal;
import com.redorange.stitch.modal.OrgsModal;
import com.redorange.stitch.typeconverter.coursesConverter;
import com.redorange.stitch.typeconverter.orgsConverter;

import java.util.List;

public class LoginInfoM {
    String id;
    String name;
    String cv;
    String pic;
    String bio;
    String phone;
    String email;
    String address;

    String createdAt;
    String updatedAt;
    String token;
    List<Roles> Roles;

    public LoginInfoM(String id, String name, String cv, String pic, String bio, String phone, String email, String address, String createdAt, String updatedAt, String token, List<Roles> roles) {
        this.id = id;
        this.name = name;
        this.cv = cv;
        this.pic = pic;
        this.bio = bio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.token = token;
        this.Roles = roles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<com.redorange.stitch.entities.Roles> getRoles() {
        return Roles;
    }

    public void setRoles(List<com.redorange.stitch.entities.Roles> roles) {
        Roles = roles;
    }
}
