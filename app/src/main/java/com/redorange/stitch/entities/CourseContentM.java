package com.redorange.stitch.entities;

import androidx.room.Entity;

import com.redorange.stitch.modal.OrgsContentModal;

import java.util.List;

public class CourseContentM {
    String id;

    String title;
    String banner;
    String shortDesc;
    String description;
    String objective;
    String rating;
    List<ContentChapters> Chapters;

    OrgsContentModal Org;

    public CourseContentM() {
    }


    public CourseContentM(String id, String title, String banner, String shortDesc, String description,
                          String objective, String rating, List<ContentChapters> chapters, OrgsContentModal org) {
        this.id = id;
        this.title = title;
        this.banner = banner;
        this.shortDesc = shortDesc;
        this.description = description;
        this.objective = objective;
        this.rating = rating;
        Chapters = chapters;
        Org = org;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public OrgsContentModal getOrg() {
        return Org;
    }

    public void setOrg(OrgsContentModal org) {
        Org = org;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ContentChapters> getChapters() {
        return Chapters;
    }

    public void setChapters(List<ContentChapters> chapters) {
        Chapters = chapters;
    }
}
