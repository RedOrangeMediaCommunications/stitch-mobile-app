package com.redorange.stitch.entities;

import java.io.Serializable;
import java.util.List;

public class Questions implements Serializable {
    List<String> answers;
    String question;
    String answerType;


    public Questions() {
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }


}
