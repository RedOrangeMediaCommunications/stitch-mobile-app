package com.redorange.stitch.entities.course;

import androidx.room.TypeConverters;

import com.redorange.stitch.entities.Trainers;
import com.redorange.stitch.typeconverter.chapterConverter;

import java.util.List;

public class CoursesM {
    String id;
    String orgId;
    String title;
    String banner;
    String bannerLocal;
    String shortDesc;
    String description;
    String objective;
    String passing_mark;
    String visitor_count;
    String is_featured;
    String published;
    String createdAt;
    String updatedAt;
    String deletedAt;



    List<Trainers> Trainers;
    String rating;


    public CoursesM() {
    }



    public List<Trainers> getTrainers() {
        return Trainers;
    }

    public void setTrainers(List<Trainers> trainers) {
        Trainers = trainers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getBannerLocal() {
        return bannerLocal;
    }

    public void setBannerLocal(String bannerLocal) {
        this.bannerLocal = bannerLocal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPassing_mark() {
        return passing_mark;
    }

    public void setPassing_mark(String passing_mark) {
        this.passing_mark = passing_mark;
    }

    public String getVisitor_count() {
        return visitor_count;
    }

    public void setVisitor_count(String visitor_count) {
        this.visitor_count = visitor_count;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }


}


