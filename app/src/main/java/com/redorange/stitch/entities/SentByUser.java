package com.redorange.stitch.entities;

public class SentByUser {
    String id;
    String orgId;
    String fromId;
    String handled_by;
    String courseId;
    String type;
    String status;
    RequestContent content;
    String createdAt;
    String updatedAt;
    String deletedAt;

    public SentByUser(String id, String orgId, String fromId, String handled_by, String courseId, String type, String status, RequestContent content, String createdAt, String updatedAt, String deletedAt) {
        this.id = id;
        this.orgId = orgId;
        this.fromId = fromId;
        this.handled_by = handled_by;
        this.courseId = courseId;
        this.type = type;
        this.status = status;
        this.content = content;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public RequestContent getContent() {
        return content;
    }

    public void setContent(RequestContent content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getHandled_by() {
        return handled_by;
    }

    public void setHandled_by(String handled_by) {
        this.handled_by = handled_by;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
