package com.redorange.stitch.entities.course;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class _0 {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("designation")
    @Expose
    private String designation;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getDesignation(){
        return designation;
    }

    public void setDesignation(String designation){
        this.designation=designation;
    }
}
