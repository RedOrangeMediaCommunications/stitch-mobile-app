package com.redorange.stitch.entities;

import androidx.room.TypeConverters;

import com.redorange.stitch.typeconverter.filesConverter;
import com.redorange.stitch.typeconverter.questionsConverter;
import com.redorange.stitch.typeconverter.reactionsConverter;
import com.redorange.stitch.typeconverter.userConverter;

import java.io.Serializable;
import java.util.List;

public class Comments implements Serializable {
    String id;
    String content;
    String createdAt;
    @TypeConverters(userConverter.class)
    User User;
    @TypeConverters(reactionsConverter.class)
    List<Reactions> Reactions;


    public Comments() {
    }

    public Comments(String id, String content, String createdAt, com.redorange.stitch.entities.User user, List<Reactions> reactions) {
        this.id = id;
        this.content = content;
        this.createdAt = createdAt;
        User = user;
        Reactions = reactions;
    }




    public List<Reactions> getReactions() {
        return Reactions;
    }

    public void setReactions(List<Reactions> reactions) {
        Reactions = reactions;
    }



    public com.redorange.stitch.entities.User getUser() {
        return User;
    }

    public void setUser(com.redorange.stitch.entities.User user) {
        User = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
