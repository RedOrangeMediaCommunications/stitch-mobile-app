package com.redorange.stitch.entities;

public class Files {
    String id;
    String title;
    String description;
    String url;
    String url_local="null";
    String type;
    String extension;



    public Files(String id, String title, String description, String url, String url_local, String type, String extension) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;
        this.url_local = url_local;
        this.type = type;
        this.extension = extension;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_local() {
        return url_local;
    }

    public void setUrl_local(String url_local) {
        this.url_local = url_local;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
