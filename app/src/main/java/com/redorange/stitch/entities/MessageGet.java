package com.redorange.stitch.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MessageGet implements Serializable {
    @SerializedName("id")
    String id;

    @SerializedName("name")
    String name;
    @SerializedName("Users")
    List<UsersInConversation> Users;

    @SerializedName("Messages")
    List<MessagesM> Messages;

    public MessageGet(String id, String name, List<UsersInConversation> users, List<MessagesM> messages) {
        this.id = id;
        this.name = name;
        Users = users;
        Messages = messages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UsersInConversation> getUsers() {
        return Users;
    }

    public void setUsers(List<UsersInConversation> users) {
        Users = users;
    }

    public List<MessagesM> getMessages() {
        return Messages;
    }

    public void setMessages(List<MessagesM> messages) {
        Messages = messages;
    }
}
