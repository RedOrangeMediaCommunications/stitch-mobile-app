package com.redorange.stitch.entities;

import com.google.gson.annotations.SerializedName;

public class MessagesM {
    String id;
    String senderId;
    String createdAt;
    Content content;


    public MessagesM(String id, String senderId, String createdAt, Content content) {
        this.id = id;
        this.senderId = senderId;
        this.createdAt = createdAt;
        this.content = content;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


}
