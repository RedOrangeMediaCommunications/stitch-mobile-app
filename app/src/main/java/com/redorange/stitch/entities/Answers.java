package com.redorange.stitch.entities;

import java.util.List;

public class Answers {
    int number;
    List<String> answer;


    public Answers(int number, List<String> answer) {
        this.number = number;
        this.answer = answer;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }
}
