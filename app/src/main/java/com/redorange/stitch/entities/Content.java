package com.redorange.stitch.entities;

import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("message")
    String message;



    public Content(String message) {
        this.message = message;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
