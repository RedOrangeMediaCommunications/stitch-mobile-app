package com.redorange.stitch.entities;

public class CourseJoinRequestM {
    String id;
    String type;
    String fromId;
    String courseId;
    String content;
    String handled_by;
    String updatedAt;
    String createdAt;
    String orgId;
    String status;
    String deletedAt;

    public CourseJoinRequestM(String id, String type, String fromId, String courseId, String content, String handled_by, String updatedAt, String createdAt, String orgId, String status, String deletedAt) {
        this.id = id;
        this.type = type;
        this.fromId = fromId;
        this.courseId = courseId;
        this.content = content;
        this.handled_by = handled_by;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.orgId = orgId;
        this.status = status;
        this.deletedAt = deletedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHandled_by() {
        return handled_by;
    }

    public void setHandled_by(String handled_by) {
        this.handled_by = handled_by;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
