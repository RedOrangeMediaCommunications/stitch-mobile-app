package com.redorange.stitch.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.redorange.stitch.typeconverter.answersConverter;


import java.util.List;
@Entity(tableName = "submissions")
public class SubmissionsM {
    @PrimaryKey(autoGenerate = true)
    public int primaryKey;
    int examId;
    String score;
    String chapterID;
    @TypeConverters(answersConverter.class)
    List<Answers> answers;

    public SubmissionsM() {
    }


    public SubmissionsM(int examId, String score, String chapterID, List<Answers> answers) {
        this.examId = examId;
        this.score = score;
        this.chapterID = chapterID;
        this.answers = answers;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public String getChapterID() {
        return chapterID;
    }

    public void setChapterID(String chapterID) {
        this.chapterID = chapterID;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }
}
