package com.redorange.stitch.entities;

import androidx.room.TypeConverters;

import com.redorange.stitch.typeconverter.commentsConverter;
import com.redorange.stitch.typeconverter.contentChapterConverter;
import com.redorange.stitch.typeconverter.examsConverter;
import com.redorange.stitch.typeconverter.filesConverter;

import java.util.List;

public class ContentChapters {
    String id;
    String title;
    String instructions;

    @TypeConverters(examsConverter.class)
    List<Exams> Exams;
    @TypeConverters(filesConverter.class)
    List<Files> Files;

    @TypeConverters(commentsConverter.class)
    List<Comments> Comments;

    public ContentChapters() {
    }

    public List<com.redorange.stitch.entities.Comments> getComments() {
        return Comments;
    }

    public void setComments(List<com.redorange.stitch.entities.Comments> comments) {
        Comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }


    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<com.redorange.stitch.entities.Exams> getExams() {
        return Exams;
    }

    public void setExams(List<com.redorange.stitch.entities.Exams> exams) {
        Exams = exams;
    }

    public List<com.redorange.stitch.entities.Files> getFiles() {
        return Files;
    }

    public void setFiles(List<com.redorange.stitch.entities.Files> files) {
        Files = files;
    }
}
