package com.redorange.stitch.entities.thread;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {

    @SerializedName("recipientId")
    @Expose
    private String recipientId;
    @SerializedName("threadId")
    @Expose
    private String threadId;
    @SerializedName("content")
    @Expose
    private ThreadContent content;

    public Info(String recipientId, String threadId, ThreadContent content) {
        this.recipientId = recipientId;
        this.threadId = threadId;
        this.content = content;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public ThreadContent getContent() {
        return content;
    }

    public void setContent(ThreadContent content) {
        this.content = content;
    }
}
